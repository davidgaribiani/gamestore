﻿using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game
{
    public class GameFilterVM
    {
        public string ContainsString { get; set; }

        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();
    }
}
