﻿using Newtonsoft.Json;

namespace GameStore.WebApi.Models.Game
{
    public class ImageVM
    {
        [JsonProperty("imageTitle")]
        public string ImageTitle { get; set; }

        [JsonProperty("imageDataUrl")]
        public string ImageDataUrl { get; set; }
    }
}
