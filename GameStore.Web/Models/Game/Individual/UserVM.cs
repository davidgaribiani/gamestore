﻿using Newtonsoft.Json;

namespace GameStore.WebApi.Models.Game.Individual
{
    public class UserVM
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }
    }
}
