﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game.Individual
{
    public class IndividualGameVM
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("genres")]
        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();

        [JsonProperty("comments")]
        public ICollection<CommentVM> Comments { get; set; } = new List<CommentVM>();

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public ImageVM Image { get; set; }
    }
}
