﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game.Individual
{
    public class CommentVM
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("parentCommentId", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid? ParentCommentId { get; set; }

        [JsonProperty("deletedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? DeletedAt { get; set; }

        [JsonProperty("timeSinceAdded")]
        public string TimeSinceAdded { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("commentAuthor")]
        public UserVM CommentAuthor { get; set; }

        [JsonProperty("childComments")]
        public ICollection<CommentVM> ChildComments { get; set; } = new List<CommentVM>();
    }
}
