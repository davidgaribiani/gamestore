﻿using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game
{
    public class GenreCheckBoxVM
    {
        public string Name { get; set; }

        public bool IsChecked { get; set; }

        public ICollection<GenreCheckBoxVM> ChildGenres { get; set; } = new List<GenreCheckBoxVM>();
    }
}
