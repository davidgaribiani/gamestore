﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game.Add
{
    public class AddGameGetVM
    {
        [JsonProperty("genres")]
        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();
    }
}