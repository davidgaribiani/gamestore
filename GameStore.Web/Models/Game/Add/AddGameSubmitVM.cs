﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.WebApi.Models.Game.Add
{
    public class AddGameSubmitVM
    {
        [JsonProperty("name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is required.")]
        [StringLength(50, ErrorMessage = "Length must be between 5 and 50 characters", MinimumLength = 5)]
        public string Name { get; set; }

        [JsonProperty("description")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required.")]
        [StringLength(600, ErrorMessage = "Length must be between 5 and 600 characters", MinimumLength = 5)]
        public string Description { get; set; }

        [JsonProperty("price")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Price must contain a positive number.")]
        public decimal Price { get; set; } = 1;

        [JsonProperty("genres")]
        [Required]
        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();
    }
}
