﻿using GameStore.WebApi.Models.Game.Card;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game
{
    public class HomeVM
    {
        [JsonProperty("currentPageNumber")]
        public int CurrentPageNumber { get; set; }

        [JsonProperty("numberOfPages")]
        public int NumberOfPages { get; set; }

        [JsonProperty("games", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ICollection<GameCardVM> Games { get; set; } = new List<GameCardVM>();
    }
}
