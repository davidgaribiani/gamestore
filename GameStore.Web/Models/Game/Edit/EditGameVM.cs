﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.WebApi.Models.Game.Edit
{
    public class EditGameVM
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("description")]
        [Required]
        public string Description { get; set; }

        [JsonProperty("price")]
        [Required]
        public decimal Price { get; set; }

        [JsonProperty("genres")]
        [Required]
        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();
    }
}
