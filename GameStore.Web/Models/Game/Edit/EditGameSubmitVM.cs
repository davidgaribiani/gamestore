﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.WebApi.Models.Game.Edit
{
    public class EditGameSubmitVM
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("genres")]
        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();
    }
}
