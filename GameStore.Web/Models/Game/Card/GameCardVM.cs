﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GameStore.WebApi.Models.Game.Card
{
    public class GameCardVM
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("genres")]
        public ICollection<GenreCheckBoxVM> Genres { get; set; } = new List<GenreCheckBoxVM>();

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public ImageVM Image { get; set; }
    }
}
