﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.WebApi.Models.Order
{

    public class OrderVM
    {
        [JsonProperty("orderedAt")]
        public string OrderedAt { get; set; }

        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("comments")]
        public string? Comments { get; set; }

        [JsonProperty("total")]
        public decimal Total => OrderDetails.Sum(x => x.Total);

        [JsonProperty("orderDetails")]
        public ICollection<OrderDetailVM> OrderDetails { get; set; } = new List<OrderDetailVM>();
    }
}
