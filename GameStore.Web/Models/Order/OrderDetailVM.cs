﻿using Newtonsoft.Json;

namespace GameStore.WebApi.Models.Order
{
    public class OrderDetailVM
    {
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("gameName")]
        public string GameName { get; set; }

        [JsonProperty("total")]
        public decimal Total => Quantity * Price;
    }
}
