﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GameStore.WebApi.Models.Order
{
    [Route("api/[controller]")]
    [ApiController]
    public class OnCompletingOrderVM
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name is required.")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name is required.")]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is required.")]
        [StringLength(50, ErrorMessage = "Email length must be between 5 and 50 characters", MinimumLength = 5)]
        [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone is required.")]
        [StringLength(50, ErrorMessage = "Phone number length must be between 5 and 50 characters", MinimumLength = 5)]
        public string Phone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "PaymentType is required.")]
        [MaxLength(50)]
        public string PaymentType { get; set; }

        [MaxLength(600)]
        public string? Comments { get; set; }
    }
}
