﻿using Newtonsoft.Json;

namespace GameStore.WebApi.Models.Admin
{
    public class UserIsManager
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("isManager")]
        public bool IsManager { get; set; }
    }
}
