﻿using Newtonsoft.Json;
using System;

namespace GameStore.PL.Models.Comment
{
    public class AddCommentVM
    {
        [JsonProperty("parentCommentId")]
        public Guid? ParentCommentId { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("gameId")]
        public Guid? GameId { get; set; }
    }
}
