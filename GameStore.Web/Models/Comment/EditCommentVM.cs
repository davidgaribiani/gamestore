﻿using Newtonsoft.Json;

namespace GameStore.WebApi.Models.Comment
{
    public class EditCommentVM
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }
    }
}
