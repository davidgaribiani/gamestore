﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GameStore.PL.Models.Auth
{
    public class TokenRequestViewModel
    {
        [Required]
        [JsonProperty("token")]
        public string Token { get; set; }

        [Required]
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
