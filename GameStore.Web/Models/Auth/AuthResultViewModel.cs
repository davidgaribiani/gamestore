﻿using Newtonsoft.Json;
using System;

namespace GameStore.PL.Models.Auth
{
    public class AuthResultViewModel
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("expiresAt")]
        public DateTime ExpiresAt { get; set; }
    }
}
