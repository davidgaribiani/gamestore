﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GameStore.PL.Models.Auth
{
    public class LoginViewModel
    {
        [JsonProperty("UserName")]
        [Required(ErrorMessage = "UserName is required")]
        public string UserName { get; set; }

        [JsonProperty("Password")]
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
