﻿using Newtonsoft.Json;

namespace GameStore.WebApi.Models
{
    public class ValidationErrorVM
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
