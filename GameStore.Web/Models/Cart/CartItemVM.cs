﻿using Newtonsoft.Json;
using System;

namespace GameStore.WebApi.Models.Cart
{
    public class CartItemVM
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("game")]
        public CartGameVM Game { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}
