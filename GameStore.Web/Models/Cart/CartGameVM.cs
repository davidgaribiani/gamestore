﻿using GameStore.WebApi.Models.Game;
using Newtonsoft.Json;

namespace GameStore.WebApi.Models.Cart
{
    public class CartGameVM
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public ImageVM Image { get; set; }
    }
}
