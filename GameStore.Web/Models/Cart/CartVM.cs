﻿using GameStore.WebApi.Models.Game.Individual;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GameStore.WebApi.Models.Cart
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartVM : ControllerBase
    {
        [JsonProperty("items")]
        public ICollection<CartItemVM> Items { get; set; }

        [JsonProperty("user")]
        public UserVM User { get; set; }

    }
}
