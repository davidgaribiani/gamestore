﻿using Newtonsoft.Json.Converters;

namespace GameStore.WebApi.CustomSerializers
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
