﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.PL.Models.Comment;
using GameStore.WebApi.Models.Cart;
using GameStore.WebApi.Models.Comment;
using GameStore.WebApi.Models.Game;
using GameStore.WebApi.Models.Game.Add;
using GameStore.WebApi.Models.Game.Card;
using GameStore.WebApi.Models.Game.Edit;
using GameStore.WebApi.Models.Game.Individual;
using GameStore.WebApi.Models.Order;
using System;

namespace GameStore.PL
{
    public class AutomapperProfilePL : Profile
    {
        public AutomapperProfilePL()
        {

            // Add Game
            CreateMap<GameDTO, AddGameGetVM>()
                .ReverseMap();
            CreateMap<GameDTO, AddGameSubmitVM>()
                .ReverseMap();

            // Edit Game
            CreateMap<GameDTO, EditGameVM>()
               .ReverseMap();
            CreateMap<GameDTO, EditGameSubmitVM>()
                .ReverseMap();

            // Game Individual
            CreateMap<GameDTO, IndividualGameVM>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<CommentDTO, CommentVM>()
                .ForMember(dest => dest.TimeSinceAdded, opt => opt.MapFrom(src => GetTimeLeftSince(src.PublicationDate)));
            CreateMap<CommentDTO, AddCommentVM>().ReverseMap();
            CreateMap<CommentDTO, EditCommentVM>().ReverseMap();

            // Game Card
            CreateMap<GameDTO, GameCardVM>().ReverseMap();

            // Game Common
            CreateMap<GenreDTO, GenreCheckBoxVM>().ReverseMap();
            CreateMap<ImageDTO, ImageVM>()
                .ForMember(dest => dest.ImageTitle, opt => opt.MapFrom(src => src.ImageTitle))
                .ForMember(dest => dest.ImageDataUrl, opt => opt.MapFrom(src => string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(src.ImageData))));
            CreateMap<ApplicationUserDTO, UserVM>();
            CreateMap<HomeDTO, HomeVM>().ReverseMap();

            // Cart
            CreateMap<CartDTO, CartVM>().ReverseMap();
            CreateMap<CartItemDTO, CartItemVM>().ReverseMap();
            CreateMap<GameDTO, CartGameVM>().ReverseMap();

            // Order
            CreateMap<OrderDTO, OnCompletingOrderVM>().ReverseMap();
            CreateMap<OrderDTO, OrderVM>()
                .ForMember(dest => dest.OrderedAt, opt => opt.MapFrom(src => src.OrderedAt.ToString()));
            CreateMap<OrderDetailDTO, OrderDetailVM>()
                .ForMember(dest => dest.GameName, opt => opt.MapFrom(src => src.Game.Name));
        }


        public string GetTimeLeftSince(DateTime dateTime)
        {
            var diff = DateTime.UtcNow.Subtract(dateTime);

            if (diff.TotalDays >= 1)
            {
                return $"{(int)diff.TotalDays} days ago";
            }
            else if (diff.TotalHours >= 1)
            {
                return $"{(int)diff.TotalHours} hours ago";
            }
            else if (diff.TotalMinutes >= 1)
            {
                return $"{(int)diff.TotalMinutes} minutes ago";
            }
            else
            {
                return "less than a minute ago";
            }
        }
    }
}
