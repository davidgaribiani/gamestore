﻿using FluentValidation;
using GameStore.WebApi.Models.Game;
using GameStore.WebApi.Models.Game.Add;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameStore.WebApi.Validation.ModelValidators
{
    public class AddGameSubmitVMValidator : AbstractValidator<AddGameSubmitVM>
    {
        public AddGameSubmitVMValidator()
        {
            RuleFor(model => model.Name)
                .NotEmpty()
                .WithMessage("Name is required.");

            RuleFor(model => model.Name)
                .Length(5, 50)
                .WithMessage("Name must contain between 5 and 50 characters.");

            RuleFor(model => model.Description)
                .NotEmpty()
                .WithMessage("Description is required");

            RuleFor(model => model.Description)
                .Length(5, 600)
                .WithMessage("Description must contain between 5 and 600 characters.");

            RuleFor(model => model.Price)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Price should be greater than or equal to 0.");

            //RuleFor(model => model.Genres)
            //    .Must(model => model.Any())
            //    .WithMessage("Something went wrong.");
        }
    }
}
