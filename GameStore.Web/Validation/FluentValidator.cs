﻿using FluentValidation;
using GameStore.PL.Models.Auth;
using GameStore.WebApi.Models.Game.Add;
using GameStore.WebApi.Models.Game.Edit;
using GameStore.WebApi.Validation.ModelValidators;

namespace GameStore.WebApi.Validation
{
    public class FluentValidator
    {
        public AbstractValidator<AddGameSubmitVM> AddGameSubmitVMValidator => new AddGameSubmitVMValidator();

        public AbstractValidator<EditGameSubmitVM> EditGameSubmitVMValidator = new EditGameSubmitVMValidator();
    }
}
