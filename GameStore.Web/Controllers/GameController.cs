﻿using AutoMapper;
using FluentValidation;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities;
using GameStore.WebApi.Extensions;
using GameStore.WebApi.Models.Game;
using GameStore.WebApi.Models.Game.Add;
using GameStore.WebApi.Models.Game.Card;
using GameStore.WebApi.Models.Game.Edit;
using GameStore.WebApi.Models.Game.Individual;
using GameStore.WebApi.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Web.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private readonly IGameService _gameService;
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;
        private readonly ILogger<GameController> _logger;
        private readonly FluentValidator _validator;

        public GameController(
            IGameService gameService,
            IGenreService genreService,
            IMapper mapper,
            ILogger<GameController> logger,
            FluentValidator validator)
        {
            _gameService = gameService;
            _genreService = genreService;
            _mapper = mapper;
            _logger = logger;
            _validator = validator;
        }

        #region Action Methods

        #region Add Game Action Methods

        [Authorize(Roles = $"{UserRoles.Administrator}, {UserRoles.Manager}")]
        [HttpGet("model", Name = "AddGameModel")]
        public async Task<IActionResult> AddGame()
        {
            try
            {
                var model = new AddGameGetVM();
                var genres = await _genreService.GetAllGenres();

                // Seed model with select options
                model.Genres = genres.Where(x => x.ParentGenre is null).Select(x => _mapper.Map<GenreCheckBoxVM>(x)).ToList();

                return StatusCode(200, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the game.");
                return StatusCode(500, "Something went wrong. Failed to retrieve");
            }
        }

        [Authorize(Roles = $"{UserRoles.Administrator}, {UserRoles.Manager}")]
        [HttpPost(Name = nameof(AddGame))]
        public async Task<IActionResult> AddGame([FromBody] AddGameSubmitVM model)
        {
            try
            {
                var validationResult = await _validator.AddGameSubmitVMValidator.ValidateAsync(model);

                if (!validationResult.IsValid)
                {
                    var validationErrors = new Dictionary<string, string>(); // key, errorMessage

                    foreach (var error in validationResult.Errors.DistinctBy(x => x.PropertyName))
                    {
                        validationErrors.Add(error.PropertyName.ToCamelCase(), error.ErrorMessage);
                    }

                    return StatusCode(400, validationErrors);
                }

                if (model.Genres.Any())
                {
                    model.Genres = FlattenGenreCheckBoxVMTree(model.Genres).Where(x => x.IsChecked == true).ToList();
                }

                await _gameService.AddGame(_mapper.Map<GameDTO>(model));
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while adding the game.");
                return StatusCode(500, "Something went wrong. Failed to add the game.");
            }
        }

        #endregion

        #region Edit Game Action Methods

        [Authorize(Roles = $"{UserRoles.Administrator}, {UserRoles.Manager}")]
        [HttpGet("model/{id}", Name = "EditGameModel")]
        public async Task<IActionResult> EditGame([FromRoute] Guid id)
        {
            try
            {
                var model = _mapper.Map<EditGameVM>(await _gameService.GetGame(id));
                var genres = await _genreService.GetAllGenres();
                var genreViewModels = new List<GenreCheckBoxVM>();

                genres
                    .Where(x => x.ParentGenre is null)
                    .Select(x => _mapper.Map<GenreCheckBoxVM>(x))
                    .ToList()
                    .ForEach(x => genreViewModels.Add(SelectGenreCheckBoxVMRecursively(x, FlattenGenreCheckBoxVMTree(model.Genres))));

                // Seed model with properties genres and platformTypes including selected ones.
                model.Genres = genreViewModels;

                return StatusCode(200, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the game model.");
                return StatusCode(500, "Something went wrong. Failed to retreive game model.");
            }
        }

        [Authorize(Roles = $"{UserRoles.Administrator}, {UserRoles.Manager}")]
        [HttpPut(Name = nameof(EditGame))]
        public async Task<IActionResult> EditGame([FromBody] EditGameSubmitVM model)
        {
            try
            {
                var validationResult = await _validator.EditGameSubmitVMValidator.ValidateAsync(model);

                if (!validationResult.IsValid)
                {
                    var validationErrors = new Dictionary<string, string>(); // key, errorMessage

                    foreach (var error in validationResult.Errors.DistinctBy(x => x.PropertyName))
                    {
                        validationErrors.Add(error.PropertyName.ToCamelCase(), error.ErrorMessage);

                        // TODO fix issue with same key 
                    }

                    return StatusCode(400, validationErrors);
                }

                model.Genres = FlattenGenreCheckBoxVMTree(model.Genres).Where(x => x.IsChecked == true).ToList();
                await _gameService.EditGame(_mapper.Map<GameDTO>(model));
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying edit the game.");
                return StatusCode(500, "Something went wrong. Game was not edited.");
            }
        }

        #endregion

        #region Delete Game Action Methods

        [Authorize(Roles = $"{UserRoles.Administrator}, {UserRoles.Manager}")]
        [HttpDelete("{id}", Name = nameof(DeleteGame))]
        public async Task<IActionResult> DeleteGame([FromRoute] Guid id)
        {
            try
            {
                await _gameService.DeleteGame(id);
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to delete the game.");
                return StatusCode(500, "Something went wrong. Failed to delete the game.");
            }

        }

        #endregion

        #region Get Game Action Methods

        [HttpGet("individual/{id}", Name = nameof(GetIndividualGameVM))]
        public async Task<IActionResult> GetIndividualGameVM([FromRoute] Guid id)
        {
            try
            {
                var dto = await _gameService.GetGame(id);

                if (dto is null)
                {
                    throw new NullReferenceException(nameof(dto)); // todo
                }

                dto.Comments = dto.Comments.Where(x => x.ParentCommentId == null  && x.DeletedAt == null).ToList();
                dto.Comments = GetRidOfDeletedComments(dto.Comments);

                var model = _mapper.Map<IndividualGameVM>(dto);

                return StatusCode(200, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the game details.");
                return StatusCode(500, "Something went wrong. Failed to retrieve game details.");
            }
        }


        [HttpPost("page/{pageNumber:min(1)}", Name = nameof(GetGames))]
        public async Task<IActionResult> GetGames([FromRoute] int pageNumber, [FromBody] GameFilterVM? filter)
        {
            try
            {
                var selectedGenres = FlattenGenreCheckBoxVMTree(filter.Genres)
                    .Where(x => x.IsChecked)
                    .Select(x => _mapper.Map<GenreDTO>(x)).ToList();

                var result = await _gameService.GetAllGamesFilteredWithPaging(pageNumber, 9, filter.ContainsString, selectedGenres);

                var response = _mapper.Map<HomeVM>(result);

                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the game details.");
                return StatusCode(500, "Something went wrong. Failed to retrieve games details.");
            }
        }

        #endregion



        #endregion

        #region Helper Methods 

        [NonAction]
        private ICollection<GenreCheckBoxVM> FlattenGenreCheckBoxVMTree(ICollection<GenreCheckBoxVM> genres)
        {
            var result = new List<GenreCheckBoxVM>();

            foreach (var genre in genres)
            {
                result.Add(genre);

                if (genre.ChildGenres != null && genre.ChildGenres.Count() >= 0)
                {
                    result.AddRange(FlattenGenreCheckBoxVMTree(genre.ChildGenres));
                }
            }

            return result;
        }

        [NonAction]
        private GenreCheckBoxVM SelectGenreCheckBoxVMRecursively(GenreCheckBoxVM genre, ICollection<GenreCheckBoxVM> selectedGenres)
        {
            if (selectedGenres.Any(x => x.Name == genre.Name)) genre.IsChecked = true;

            if (genre.ChildGenres != null && genre.ChildGenres.Count != 0)
            {
                genre.ChildGenres.AsQueryable().ForEachAsync(x => SelectGenreCheckBoxVMRecursively(x, selectedGenres));
            }

            return genre;
        }

        [NonAction]
        private ICollection<CommentDTO> GetRidOfDeletedComments(ICollection<CommentDTO> comments)
        {
            var result = new List<CommentDTO>();

            foreach(var comment in comments)
            {
                if (comment.DeletedAt == null)
                {
                    if (comment.ChildComments.Count() > 0)
                    {
                        var childComments = GetRidOfDeletedComments(comment.ChildComments);
                        comment.ChildComments = childComments;
                    }

                    result.Add(comment);
                }
            }

            return result;
        }

        #endregion

        #region NO LONGER IN USE

        [HttpGet("small/{id}", Name = nameof(GetGameCardVM))]
        public async Task<IActionResult> GetGameCardVM([FromRoute] Guid id)
        {
            try
            {
                var dto = await _gameService.GetGame(id);

                if (dto is null)
                {
                    throw new NullReferenceException(nameof(dto));
                }

                var model = _mapper.Map<GameCardVM>(dto);
                return StatusCode(200, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the game details.");
                return StatusCode(500, "Something went wrong. Failed to retreive game details.");
            }
        }
        #endregion
    }
}
