﻿using AutoMapper;
using GameStore.BLL.Exceptions;
using GameStore.BLL.Interfaces;
using GameStore.WebApi.Models.Cart;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GameStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;
        private readonly ILogger<CartController> _logger;

        public CartController(ICartService cartService, IMapper mapper, ILogger<CartController> logger)
        {
            _cartService = cartService;
            _mapper = mapper;
            _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetCart()
        {
            try
            {
                var result = await _cartService.GetCart();
                var response = _mapper.Map<CartVM>(result);
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying get the cart details.");
                return StatusCode(500, "Something went wrong. Failed to retreive the cart");
            }
        }


        [Authorize]
        [HttpPost("game/{id}", Name = nameof(AddItemToCart))]
        public async Task<IActionResult> AddItemToCart([FromRoute] Guid id)
        {
            try
            {
                await _cartService.AddItemToCart(id);
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured when trying to sign in.");
                return StatusCode(500, "Something went wrong. Cart item was not added to the cart.");
            }
        }

        [Authorize]
        [HttpPut("item/{id}/{quantity}", Name = nameof(UpdateItemQuantity))]
        public async Task<IActionResult> UpdateItemQuantity([FromRoute] Guid id, int quantity)
        {
            try
            {
                await _cartService.UpdateCartItemQuantity(id, quantity);
                return StatusCode(200);
            }
            catch (HttpResponseException ex)
            {
                _logger.LogWarning($"User with username : '{User.Identity.Name}' tried to access resources is not authorized to.");
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to update cart item");
                return StatusCode(500, "Something went wrong. Cart item was not updated.");
            }
        }

        [Authorize]
        [HttpDelete("item/{id}")]
        public async Task<IActionResult> RemoveItemFromCart(Guid id)
        {
            try
            {
                await _cartService.RemoveItemFromCart(id);
                return StatusCode(200);
            }
            catch (HttpResponseException ex)
            {
                _logger.LogWarning($"User with username : '{User.Identity.Name}' tried to access resources is not authorized to.");
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying remove item from the cart");
                return StatusCode(500, "Something went wrong. Item was not removed from the cart.");
            }
        }
    }
}
