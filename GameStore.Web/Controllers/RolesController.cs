﻿using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities.Identity;
using GameStore.WebApi.Models.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UserRoles.Administrator)]
    public class RolesController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly ILogger<RolesController> _logger;

        public RolesController(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, ILogger<RolesController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _logger = logger;
        }

        [HttpGet("user-manager", Name = nameof(GetUserManagers))]
        public async Task<IActionResult> GetUserManagers()
        {
            try
            {
                List<UserIsManager> response = new List<UserIsManager>();

                foreach (var user in _userManager.Users.ToList())
                {
                    var isManager = await _userManager.IsInRoleAsync(user, UserRoles.Manager);
                    response.Add(new UserIsManager { UserName = user.UserName, IsManager = isManager });
                }

                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying retrieve userIsManager list.");
                return StatusCode(500, "Something went wrong. Failed to retrieve UserIsManager models");
            }
        }

        [HttpPut("user/{username}/add/{role}", Name = nameof(AddUserToRole))]
        public async Task<IActionResult> AddUserToRole([FromRoute] string username, [FromRoute] string role)
        {
            try
            {
                var roleExists = await _roleManager.RoleExistsAsync(role);

                if (roleExists)
                {
                    var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == username);

                    if (user != null)
                    {
                        await _userManager.AddToRoleAsync(user, role);
                    }
                    else
                    {
                        throw new NullReferenceException($"User with username : '{username}' does not exist.");
                    }
                }
                else
                {
                    throw new Exception($"Entered role : '{role}' does not exist.");
                }

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to add user to the role.");
                return StatusCode(500, "Something went wrong. Failed to add user to role.");
            }
        }

        [HttpPut("user/{username}/remove/{role}", Name = nameof(RemoveUserFromRole))]
        public async Task<IActionResult> RemoveUserFromRole([FromRoute] string username, [FromRoute] string role)
        {
            try
            {
                var roleExists = await _roleManager.RoleExistsAsync(role);

                if (roleExists)
                {
                    var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == username);

                    if (user != null)
                    {
                        await _userManager.RemoveFromRoleAsync(user, role);
                    }
                    else
                    {
                        throw new NullReferenceException($"User with username : '{username}' does not exist.");
                    }


                }
                else
                {
                    throw new Exception($"Entered role : '{role}' does not exist.");
                }

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to remove user from role.");
                return StatusCode(500, "Something went wrong. Failed to remove user from role.");
            }
        }
    }
}
