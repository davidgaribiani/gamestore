﻿using GameStore.DAL.Data;
using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.Token;
using GameStore.PL.Models.Auth;
using GameStore.WebApi.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Web.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly GameStoreDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly ILogger<AuthenticationController> _logger;

        public AuthenticationController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            GameStoreDbContext context,
            IConfiguration configuration,
            TokenValidationParameters tokenValidationParameters,
            ILogger<AuthenticationController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _configuration = configuration;
            _tokenValidationParameters = tokenValidationParameters;
            _logger = logger;
        }

        #region Action Methods

        [HttpPost("login", Name = nameof(Login))]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var validationErrors = new Dictionary<string, string>(); // key, errorMessage

                    foreach (var modelStateKey in ModelState.Keys)
                    {
                        var value = ModelState[modelStateKey];
                        validationErrors.Add(modelStateKey.ToCamelCase(), value.Errors.FirstOrDefault().ErrorMessage);
                    }

                    return StatusCode(400, validationErrors);
                }

                var userExists = await _userManager.FindByNameAsync(loginViewModel.UserName);

                if (userExists != null && await _userManager.CheckPasswordAsync(userExists, loginViewModel.Password))
                {
                    var tokenValue = await GenerateJWTTokenAsync(userExists, null);

                    return StatusCode(200, tokenValue);

                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to sign in.");
                return StatusCode(500, "Something went wrong. Failed to login.");
            }
        }

        [HttpPost("register", Name = nameof(Register))]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel registerViewModel)
        {
            try
            {
                var validationErrors = new Dictionary<string, string>(); // key, errorMessage

                if (!ModelState.IsValid)
                {
                    foreach (var modelStateKey in ModelState.Keys)
                    {
                        var value = ModelState[modelStateKey];
                        validationErrors.Add(modelStateKey.ToCamelCase(), value.Errors.FirstOrDefault().ErrorMessage);
                    }

                    return StatusCode(400, validationErrors);
                }

                bool userWithEmailExists = _userManager.Users.Any(user => user.Email == registerViewModel.Email);
                bool userWithUserNameExists = _userManager.Users.Any(user => user.UserName == registerViewModel.UserName);

                if (userWithEmailExists || userWithUserNameExists)
                {
                    if (userWithEmailExists) validationErrors.Add(nameof(registerViewModel.Email).ToCamelCase(), "User with entered email does already exist.");
                    if (userWithUserNameExists) validationErrors.Add(nameof(registerViewModel.UserName).ToCamelCase(), "User with entered username does already exist.");

                    return StatusCode(400, validationErrors);
                }

                ApplicationUser newUser = new ApplicationUser()
                {
                    FirstName = registerViewModel.FirstName,
                    LastName = registerViewModel.LastName,
                    Email = registerViewModel.Email,
                    UserName = registerViewModel.UserName,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                var result = await _userManager.CreateAsync(newUser, registerViewModel.Password);

                if (result.Succeeded)
                {
                    // Add default role : 'Customer' to the user
                    await _userManager.AddToRoleAsync(newUser, UserRoles.Customer);
                    return StatusCode(200);
                }

                return StatusCode(500, "User could not be created.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to sign up.");
                return StatusCode(500, "Something went wrong. User could not be created.");
            }
        }

        [HttpPost("refresh-token", Name = nameof(RefreshToken))]
        public async Task<IActionResult> RefreshToken(TokenRequestViewModel tokenRequestViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var errors = new List<string>();

                    foreach (var error in ModelState.Values.SelectMany(modelState => modelState.Errors))
                    {
                        errors.Add(error.ErrorMessage);
                    }

                    return StatusCode(400, errors);
                }

                var result = await VerifyAndGenerateTokenAsync(tokenRequestViewModel);

                return StatusCode(200, result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to refresh the token.");
                return StatusCode(500);
            }
        }

        #endregion

        #region Token-Handling Helper Methods

        [NonAction]
        private async Task<AuthResultViewModel> VerifyAndGenerateTokenAsync(TokenRequestViewModel tokenRequestViewModel)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var storedToken = await _context.RefreshTokens.FirstOrDefaultAsync(rt => rt.Token ==
                tokenRequestViewModel.RefreshToken);
            var dbUser = await _userManager.FindByIdAsync(storedToken.UserId.ToString());

            try
            {
                var tokenCheckResult = jwtTokenHandler.ValidateToken(tokenRequestViewModel.Token, _tokenValidationParameters,
                    out var validatedToken);

                return await GenerateJWTTokenAsync(dbUser, storedToken);
            }
            catch (SecurityTokenExpiredException)
            {
                if (storedToken.DateExpires >= DateTime.UtcNow)
                {
                    return await GenerateJWTTokenAsync(dbUser, storedToken);
                }
                else
                {
                    return await GenerateJWTTokenAsync(dbUser, null);
                }
            }
        }

        [NonAction]
        private async Task<AuthResultViewModel> GenerateJWTTokenAsync(ApplicationUser user, RefreshToken refreshToken)
        {
            var authClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // Add User Role Claims

            var userRoles = await _userManager.GetRolesAsync(user);

            foreach (var role in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }

            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:Issuer"],
                    audience: _configuration["JWT:Audience"],
                    expires: DateTime.UtcNow.AddHours(24), // TODO AddMinutes(2)
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            if (refreshToken != null)
            {
                var response = new AuthResultViewModel()
                {
                    Token = jwtToken,
                    RefreshToken = refreshToken.Token,
                    ExpiresAt = token.ValidTo
                };

                return response;
            }
            else
            {
                var newRefreshToken = new RefreshToken()
                {
                    JwtId = token.Id,
                    IsRevoked = false,
                    UserId = user.Id,
                    DateAdded = DateTime.UtcNow,
                    DateExpires = DateTime.UtcNow.AddMonths(6),
                    Token = Guid.NewGuid().ToString() + "-" + Guid.NewGuid().ToString()
                };

                await _context.RefreshTokens.AddAsync(newRefreshToken);
                await _context.SaveChangesAsync();

                var response = new AuthResultViewModel()
                {
                    Token = jwtToken,
                    RefreshToken = newRefreshToken.Token,
                    ExpiresAt = token.ValidTo
                };


                // Add Jwt Token To Db
                await _context.JwtTokens.AddAsync(new UserToken()
                {
                    ValidFrom = token.ValidFrom,
                    ValidTo = token.ValidTo,
                    Value = jwtToken,
                    UserId = user.Id,
                    RefreshToken = response.RefreshToken
                });

                await _context.SaveChangesAsync();

                return response;
            }
        }

        #endregion
    }
}
