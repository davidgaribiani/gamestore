﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using GameStore.BLL.Interfaces;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Logging;
using GameStore.WebApi.Models.Game;

namespace GameStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;
        private readonly ILogger<GenresController> _logger;

        public GenresController(IGenreService genreService, IMapper mapper, ILogger<GenresController> logger)
        {
            _genreService = genreService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet(Name = nameof(GetGenres))]
        public async Task<IActionResult> GetGenres()
        {
            try
            {
                var dtos = await _genreService.GetAllGenres();
                var response = dtos.Select(x => _mapper.Map<GenreCheckBoxVM>(x)).ToList();
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the genres");
                return StatusCode(500, "Something went wrong. Failed to retrieve genres.");
            }

        }
    }
}
