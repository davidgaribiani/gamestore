﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Exceptions;
using GameStore.BLL.Interfaces;
using GameStore.PL.Models.Comment;
using GameStore.WebApi.Models.Comment;
using GameStore.WebApi.Models.Game.Individual;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.PL.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;
        private readonly ILogger<CommentController> _logger;

        public CommentController(ICommentService commentService, IMapper mapper, ILogger<CommentController> logger)
        {
            _commentService = commentService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("{id}", Name = nameof(GetComment))]
        public async Task<IActionResult> GetComment(Guid id)
        {
            try
            {
                var dto = await _commentService.GetCommentById(id);

                if (dto is null)
                {
                    throw new ArgumentNullException(nameof(dto));
                }

                var response = _mapper.Map<CommentVM>(dto);

                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the comment");
                return StatusCode(500, "Something went wrong. Failed to retrieve comment.");
            }
        }

        [HttpGet("game/{id}", Name = nameof(GetAllCommentsTree))]
        public async Task<IActionResult> GetAllCommentsTree([FromRoute] Guid id)
        {
            try
            {
                var comments = await _commentService.GetAllComments(id);
                comments = comments.Where(x => x.ParentCommentId == null);
                comments = GetRidOfDeletedComments(comments.ToList());
                var response = comments.Select(x => _mapper.Map<CommentVM>(x));
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the comments");
                return StatusCode(500, "Something went wrong. Failed to retrieve comments");
            }
        }

        [Authorize]
        [HttpPost(Name = nameof(AddComment))]
        public async Task<IActionResult> AddComment([FromBody] AddCommentVM model)
        {
            try
            {
                var dto = _mapper.Map<CommentDTO>(model);
                await _commentService.AddComment(dto);
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to add the comment.");
                return StatusCode(500, "Something went wrong. Failed to add the comment.");
            }
        }

        [Authorize]
        [HttpPut(Name = nameof(EditComment))]
        public async Task<IActionResult> EditComment([FromBody] EditCommentVM model)
        {
            try
            {
                var dto = _mapper.Map<CommentDTO>(model);
                await _commentService.EditComment(dto);
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to edit the comment.");
                return StatusCode(500, "Something went wrong. Failed to edit comment");
            }
        }

        [Authorize]
        [HttpPut("delete/{id}", Name = nameof(DeleteCommentSoft))]
        public async Task<IActionResult> DeleteCommentSoft([FromRoute] Guid id)
        {
            try
            {
                // get comment 
                await _commentService.DeleteCommentSoft(id);
                return StatusCode(200);
            }
            catch (HttpResponseException ex)
            {
                _logger.LogWarning($"User with username : '{User.Identity.Name}' tried to access resources is not authorized to.");
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying delete the comment");
                return StatusCode(500, "Something went wrong. Failed to delete comment");
            }
        }

        [Authorize]
        [HttpPut("recover/{id}", Name = nameof(RecoverCommentSoft))]
        public async Task<IActionResult> RecoverCommentSoft([FromRoute] Guid id)
        {
            try
            {
                await _commentService.RecoverCommentSoft(id);
                return StatusCode(200);
            }
            catch (HttpResponseException ex)
            {
                _logger.LogWarning($"User with username : '{User.Identity.Name}' tried to access resources is not authorized to.");
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to recover the comment.");
                return StatusCode(500, "Something went wrong. Failed to recover comment");
            }
        }




        #region Helper Methods

        [NonAction]
        private ICollection<CommentDTO> GetRidOfDeletedComments(ICollection<CommentDTO> comments)
        {
            var result = new List<CommentDTO>();

            foreach (var comment in comments)
            {
                if (comment.DeletedAt == null)
                {
                    if (comment.ChildComments != null && comment.ChildComments.Count() > 0)
                    {
                        var childComments = GetRidOfDeletedComments(comment.ChildComments);
                        comment.ChildComments = childComments;
                    }

                    result.Add(comment);
                }
            }

            return result;
        }

        #endregion
    }
}
