﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Data.Helpers;
using GameStore.WebApi.Models.Game;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.WebApi.Controllers
{
    [ApiController, Route("api/[controller]")] // 
    public class ImageController : ControllerBase
    {
        private readonly IImageService _imageService;
        private readonly IMapper _mapper;
        private readonly ILogger<ImageController> _logger;

        public ImageController(IImageService imageService, IMapper mapper, ILogger<ImageController> logger)
        {
            _imageService = imageService;
            _mapper = mapper;
            _logger = logger;
        }

        #region Action Methods

        // TODO Add Bearer header to the front request
        [Authorize(Roles = $"{UserRoles.Administrator}, {UserRoles.Manager}")]
        [HttpPost("game/{id}", Name = nameof(UploadImage)), DisableRequestSizeLimit]
        [ProducesResponseType(typeof(string), 200)]
        public async Task<IActionResult> UploadImage([FromRoute] Guid id)
        {
            try
            {
                var file = Request.Form.Files.FirstOrDefault();

                // TODO add check if file is null or empty, act accordingly.

                var imageDTO = new ImageDTO();
                var ms = new MemoryStream();

                file.CopyTo(ms);

                imageDTO.ImageTitle = file.FileName;
                imageDTO.ImageData = ms.ToArray();
                imageDTO.GameId = id;

                ms.Close();
                ms.Dispose();

                await _imageService.UpdateGameImage(imageDTO);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the image");
                return StatusCode(500, "Something went wrong. Failed to retreive image.");
            }
        }

        [HttpGet("game/{id}", Name = nameof(RetrieveImage)), DisableRequestSizeLimit]
        public async Task<IActionResult> RetrieveImage([FromRoute] Guid id)
        {
            try
            {
                var imageDTO = await _imageService.GetGameImage(id);

                // TODO check if imageDTO is null or not, act accordingly

                var imageVM = _mapper.Map<ImageVM>(imageDTO);

                return Ok(imageVM);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the game details.");
                return BadRequest("Image was not retreived. Something went wrong.");
            }
        }

        #endregion

    }
}
