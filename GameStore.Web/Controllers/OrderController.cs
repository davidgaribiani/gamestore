﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.WebApi.Extensions;
using GameStore.WebApi.Models.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(IOrderService orderService, IMapper mapper, ILogger<OrdersController> logger)
        {
            _orderService = orderService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PlaceOrder([FromBody] OnCompletingOrderVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var validationErrors = new Dictionary<string, string>(); // key, errorMessage

                    foreach (var modelStateKey in ModelState.Keys)
                    {
                        var value = ModelState[modelStateKey];
                        validationErrors.Add(modelStateKey.ToCamelCase(), value.Errors.FirstOrDefault().ErrorMessage);
                    }

                    return StatusCode(400, validationErrors);
                }

                var dto = _mapper.Map<OrderDTO>(viewModel);
                await _orderService.PlaceOrder(dto);
                return StatusCode(200);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to place the order.");
                return StatusCode(500, "Something went wrong. Failed to place an order.");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders()
        {
            try
            {
                var dtos = await _orderService.GetMyOrders();
                var response = dtos.Select(dto => _mapper.Map<OrderVM>(dto));
                return StatusCode(200, response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured while trying to retrieve the orders");
                return StatusCode(500, "Something went wrong. Failed to retrieve orders.");
            }
        }
    }
}
