using GameStore.DAL.Extensions;
using GameStore.PL.Extensions;
using GameStore.WebApi.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace GameStore.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure DAL
            services.ConfigureDAL(Configuration);

            // Configurae BLL
            services.ConfigureBLL();

            // Configure WebAPI 
            services.ConfigureWebAPI();

            services.AddScoped<FluentValidator>();

            // Configure Automapper 
            services.ConfigureAutomapper();

            // Configure Swagger
            services.ConfigureSwagger();

            // Configure Logging
            services.ConfigureLogging(Configuration);

            services.AddSpaStaticFiles(options =>
            {
                options.RootPath = "wwwroot/dist/client";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();

                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API-3000");
                    c.RoutePrefix = string.Empty;
                });
            }
            else
            {
                // app.UseExceptionHandler("")
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseCors(opt =>
            {
                opt
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed(origin => true) // allow any origin
                    .AllowCredentials(); // allow credentials
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            var spaStaticFileOptions = new StaticFileOptions
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(System.IO.Path.Combine(env.ContentRootPath, "wwwroot/dist/client"))
            };

            app.UseSpa(spa =>
            {
                //  spa.Options.SourcePath = "client";
                spa.Options.DefaultPageStaticFileOptions = spaStaticFileOptions;
            });

            // Seed Database
            app.EnsureSeeded().Wait();
        }
    }
}
