﻿using AutoMapper;
using GameStore.BLL;
using GameStore.BLL.Interfaces;
using GameStore.BLL.Services;
using GameStore.DAL;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;
using System;
using System.Text;

namespace GameStore.PL.Extensions
{
    public static class DependencyInjectionExtension
    {
        public static void ConfigureDAL(this IServiceCollection services, IConfiguration configuration)
        {
            // Configure DbContext with SQL
            var connectionString = configuration.GetConnectionString("GameStore");
            
            services.AddDbContext<GameStoreDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
                //options.LogTo(Console.WriteLine, LogLevel.Information);
                //options.ConfigureWarnings(warnings => warnings.Ignore(CoreEventId.RowLimitingOperationWithoutOrderByWarning));
            }, ServiceLifetime.Scoped);

            // Inect Unit Of Work
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.ConfigureIdentity(configuration);
        }

        public static void ConfigureIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            // Configure Authentication
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JWT:Secret"])), // TODO Make sure encrypted

                ValidateIssuer = true,
                ValidIssuer = configuration["JWT:Issuer"],

                ValidateAudience = true,
                ValidAudience = configuration["JWT:Audience"],

                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddSingleton(tokenValidationParameters);

            // Add Identity
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<GameStoreDbContext>()
                .AddDefaultTokenProviders();

            // Add Authentication
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                // Add JWT Bearer
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = tokenValidationParameters;
                });
        }

        public static void ConfigureBLL(this IServiceCollection services)
        {
            services.AddScoped<IGameService, GameService>();
            services.AddScoped<IGenreService, GenreService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<IOrderService, OrderService>();
        }

        public static void ConfigureWebAPI(this IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            // Disable automatic ModelState validation
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public static void ConfigureAutomapper(this IServiceCollection services)
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutomapperProfileDAL>();
                cfg.AddProfile<AutomapperProfileBLL>();
                cfg.AddProfile<AutomapperProfilePL>();
            });

            IMapper mapper = mapperConfiguration.CreateMapper();

            services.AddSingleton(mapper);
        }

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                // c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My Api", Version = "v1" });
                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            });
        }

        public static void ConfigureLogging(this IServiceCollection services, IConfiguration configuration)
        {
            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.FromLogContext()
                .CreateLogger();

            //var logger = new LoggerConfiguration()
            //    .MinimumLevel.Debug()
            //    .WriteTo.File(path: "debug.log", rollingInterval: RollingInterval.Day)
            //    .WriteTo.File(path: "info.log", restrictedToMinimumLevel: LogEventLevel.Information, rollingInterval: RollingInterval.Day)
            //    .WriteTo.File(path: "error.log", restrictedToMinimumLevel: LogEventLevel.Error, rollingInterval: RollingInterval.Day)
            //    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Warning)
            //    .CreateLogger();

            //var logger = new LoggerConfiguration()
            //        .MinimumLevel.Debug()
            //        .WriteTo.Console()
            //        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Information).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyyMMdd")}\Info.log"))
            //        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Debug).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyyMMdd")}\Debug.log"))
            //        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Warning).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyyMMdd")}\Warning.log"))
            //        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Error).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyyMMdd")}\Error.log"))
            //        .WriteTo.Logger(l => l.Filter.ByIncludingOnly(e => e.Level == LogEventLevel.Fatal).WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyyMMdd")}\Fatal.log"))
            //        .WriteTo.File($@"Logs\{DateTime.Now.ToString("yyyyMMdd")}\Verbose.log")
            //        .CreateLogger();

            services.AddLogging(opt =>
            {
                opt.ClearProviders();
                opt.AddSerilog(logger);
            });
        }
    }
}
