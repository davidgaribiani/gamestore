﻿using GameStore.DAL.Data;
using Microsoft.AspNetCore.Builder;
using System.Threading.Tasks;

namespace GameStore.DAL.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static async Task EnsureSeeded(this IApplicationBuilder applicationBuilder)
        {
            await SeedData.Seed(applicationBuilder);
        }
    }
}
