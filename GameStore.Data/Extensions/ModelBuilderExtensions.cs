﻿using GameStore.DAL.Data.FluentApiConfigurations;
using Microsoft.EntityFrameworkCore;

namespace GameStore.DAL.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void ApplyFluentApiConfigurations(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CommentConfiguration());
            modelBuilder.ApplyConfiguration(new GameConfiguration());
            modelBuilder.ApplyConfiguration(new GenreConfiguration());
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderDetailConfiguration());
            modelBuilder.ApplyConfiguration(new ImageConfiguration());
            modelBuilder.ApplyConfiguration(new CartConfiguration());
        }
    }
}
