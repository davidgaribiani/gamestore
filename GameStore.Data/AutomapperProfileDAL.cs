﻿using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.Orders;
using GameStore.DAL.Entities.ShoppingCart;

namespace GameStore.DAL
{
    public class AutomapperProfileDAL : Profile
    {
        public AutomapperProfileDAL()
        {
            CreateMap<Game, Game>().ReverseMap();
            CreateMap<Order, Order>().ReverseMap();
            CreateMap<OrderDetail, OrderDetail>().ReverseMap();
            CreateMap<Comment, Comment>().ReverseMap();
            CreateMap<Genre, Genre>().ReverseMap();
            CreateMap<Image, Image>().ReverseMap();
            CreateMap<ApplicationUser, ApplicationUser>().ReverseMap();
            CreateMap<Cart, Cart>().ReverseMap();
            CreateMap<CartItem, CartItem>().ReverseMap();
            CreateMap<Order, Order>().ReverseMap();
            CreateMap<OrderDetail, OrderDetail>().ReverseMap();
        }
    }
}
