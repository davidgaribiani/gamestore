﻿using GameStore.DAL.Data;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly GameStoreDbContext _context;

        public UserRepository(GameStoreDbContext context)
        {
            _context = context;
        }

        public async Task<ApplicationUser> GetUserByUserId(Guid userId)
        {
            var user = await _context.Users
                .Include(x => x.Cart).ThenInclude(x => x.Items).ThenInclude(x => x.Game)
                .Include(x => x.Orders).ThenInclude(x => x.OrderDetails).ThenInclude(x => x.Game)
                .FirstOrDefaultAsync(x => x.Id == userId);
            return user;
        }

        public async Task<bool> UserExists(Guid userId)
        {
            var userExists = await _context.Users.AnyAsync(x => x.Id == userId);
            return userExists;
        }
    }
}
