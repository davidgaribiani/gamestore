﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.ShoppingCart;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Repositories
{
    public class CartRepository : GenericRepository<Cart>, ICartRepository
    {
        private readonly GameStoreDbContext _context;

        public CartRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override IQueryable<Cart> IncludeMultiple(IQueryable<Cart> source)
        {
            return source.Include(x => x.Items).ThenInclude(x => x.Game);
        }

        public async Task<Cart> GetCartByUserId(Guid id)
        {
            var result = await _context.Carts
                .Include(x => x.Items).ThenInclude(x => x.Game).ThenInclude(x => x.Image)
                .FirstOrDefaultAsync(x => x.UserId == id);
            return result;
        }
    }
}
