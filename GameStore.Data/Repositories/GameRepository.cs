﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace GameStore.DAL.Repositories
{
    public class GameRepository : GenericRepository<Game>, IGameRepository
    {
        private readonly GameStoreDbContext _context;
        private readonly IMapper _mapper;

        public GameRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public override IQueryable<Game> IncludeMultiple(IQueryable<Game> source)
        {
            return source
                .Include(x => x.Genres)
                .Include(x => x.Comments).ThenInclude(x => x.CommentAuthor)
                .Include(x => x.Image);
        }

        public IQueryable<Game> GetAllAsQuertyable()
        {
            return _context.Games.AsQueryable();
        }
    }
}
