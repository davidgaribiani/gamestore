﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Repositories
{
    public abstract class GenericRepository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly GameStoreDbContext _context;
        private readonly IMapper _mapper;

        public GenericRepository(GameStoreDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public virtual async Task AddAsync(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            await _context.Set<T>().AddAsync(entity);
        }

        public virtual async Task RemoveByIdAsync(Guid id)
        {
            var entityToRemove = await _context.Set<T>().FindAsync(id);

            if (entityToRemove is null) // TODO Should it be checked if does exist? If doesn't should It throw an exception?
            {
                throw new ArgumentNullException($"Entity with such 'Id' does not exist");
            }

            _context.Set<T>().Remove(entityToRemove);
        }

        public virtual void Update(T entity)
        {
            _context.Set<T>().Update(entity);
        }

        public virtual IQueryable<T> IncludeMultiple(IQueryable<T> source)
        {
            return source;
        }

        #region Methods returning T

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await _context.Set<T>()
                .ToListAsync();
            return result;
        }

        public virtual async Task<IEnumerable<T>> GetAllWithDetailsAsync()
        {
            var result = await IncludeMultiple(_context.Set<T>())
                .ToListAsync();
            return result;
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            var result = await _context.Set<T>()
                .FindAsync(id);
            return result;
        }

        public virtual async Task<T> GetByIdWithDetailsAsync(Guid id)
        {
            var result = await IncludeMultiple(_context.Set<T>())
                .FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        #endregion

        #region CURRENTLY NOT IN USE

        public virtual async Task<TModel> GetByIdAsync<TModel>(Guid id) where TModel : IModel
        {
            var query = await _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            var result = _mapper.Map<TModel>(query);
            return result;
        }

        public virtual async Task<TModel> GetByIdWithDetailsAsync<TModel>(Guid id) where TModel : IModel
        {
            var result = await _context.Set<T>()
                .ProjectTo<TModel>(GetConfiguration<TModel>())
                .FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync<TModel>() where TModel : IModel
        {
            var query = await _context.Set<T>().ToListAsync();
            var result = query.Select(x => _mapper.Map<TModel>(x)).ToList();
            return result;
        }

        public virtual async Task<IEnumerable<TModel>> GetAllWithDetailsAsync<TModel>() where TModel : IModel
        {
            var result = await _context.Set<T>().ProjectTo<TModel>(GetConfiguration<TModel>()).ToListAsync();
            return result;

        }

        #region Helper Methods

        protected virtual MapperConfiguration GetConfiguration<TModel>() where TModel : IModel
        {
            var projectctions = new MapperConfigurationExpression();
            projectctions.CreateProjection<T, TModel>();
            var config = new MapperConfiguration(projectctions);
            return config;
        }

        #endregion

        #endregion
    }
}
