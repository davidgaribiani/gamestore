﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Orders;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GameStore.DAL.Repositories
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        private readonly GameStoreDbContext _context;

        public OrderDetailRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public override IQueryable<OrderDetail> IncludeMultiple(IQueryable<OrderDetail> source)
        {
            return source
                .Include(x => x.Order)
                .Include(x => x.Game);
        }
    }
}
