﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Repositories
{
    public class GenreRepository : GenericRepository<Genre>, IGenreRepository
    {
        private readonly GameStoreDbContext _context;

        public GenreRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override Task AddAsync(Genre entity)
        {
            if (_context.Genres.Any(g => g.Name == entity.Name))
            {
                throw new ArgumentException($"Genre with the 'Name' : '{entity.Name}' does already exist.");
            }

            return base.AddAsync(entity);
        }

        public async Task<Genre> GetByName(string name)
        {
            var genre = await _context.Genres.FirstOrDefaultAsync(g => g.Name == name);
            return genre;
        }

        public override IQueryable<Genre> IncludeMultiple(IQueryable<Genre> source)
        {
            return source
                .Include(x => x.ChildGenres)
                .Include(x => x.ParentGenre)
                .Include(x => x.Games);
        }
    }
}
