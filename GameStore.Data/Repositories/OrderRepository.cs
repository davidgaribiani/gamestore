﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Orders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private readonly GameStoreDbContext _context;

        public OrderRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override IQueryable<Order> IncludeMultiple(IQueryable<Order> source)
        {
            return source
                .Include(x => x.OrderDetails)
                .Include(x => x.Customer);
        }

        public IEnumerable<Order> GetOrdersByCustomerId(Guid customerId)
        {
            var orders = _context.Orders
                .Include(x => x.OrderDetails).ThenInclude(x => x.Game)
                .Include(x => x.Customer)
                .Where(x => x.CustomerId == customerId);

            return orders;
        }
    }
}
