﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities.ShoppingCart;
using GameStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GameStore.DAL.Repositories
{
    public class CartItemRepository : GenericRepository<CartItem>, ICartItemRepository
    {
        public CartItemRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public override IQueryable<CartItem> IncludeMultiple(IQueryable<CartItem> source)
        {
            return source.Include(x => x.Cart).ThenInclude(x => x.User);
        }
    }
}
