﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GameStore.DAL.Repositories
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        private readonly GameStoreDbContext _context;

        public CommentRepository(GameStoreDbContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override IQueryable<Comment> IncludeMultiple(IQueryable<Comment> source)
        {
            return source
                .Include(x => x.CommentAuthor)
                .Include(x => x.ChildComments).ThenInclude(x => x.CommentAuthor);
        }
    }
}
