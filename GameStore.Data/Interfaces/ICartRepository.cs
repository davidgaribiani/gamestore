﻿using GameStore.DAL.Entities.ShoppingCart;
using System;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public interface ICartRepository : IRepository<Cart>
    {
        public Task<Cart> GetCartByUserId(Guid id);
    }
}
