﻿namespace GameStore.DAL.Entities
{
    public interface ICommentRepository : IRepository<Comment>
    {
    }
}
