﻿using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public interface IGenreRepository : IRepository<Genre>
    {
        public Task<Genre> GetByName(string name);
    }
}
