﻿using GameStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public interface IUnitOfWork
    {
        public IGameRepository GameRepository { get; }
        public ICommentRepository CommentRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }
        public IGenreRepository GenreRepository { get; }
        public ICartRepository CartRepository { get; }
        public ICartItemRepository CartItemRepository { get; }
        public IUserRepository UserRepository { get; }

        public EntityEntry<T> ChangeTracking<T>(T entity) where T : class, IEntity;
        public Task SaveChangesAsync();
        public Task DisposeAsync();
    }
}
