﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public interface IRepository<T> where T : class, IEntity
    {
        public Task AddAsync(T entity);

        public Task RemoveByIdAsync(Guid id);

        public void Update(T entity);

        public Task<IEnumerable<T>> GetAllAsync();
        public Task<IEnumerable<TModel>> GetAllAsync<TModel>() where TModel : IModel;

        public Task<IEnumerable<T>> GetAllWithDetailsAsync();
        public Task<IEnumerable<TModel>> GetAllWithDetailsAsync<TModel>() where TModel : IModel;

        public Task<T> GetByIdAsync(Guid id);
        public Task<TModel> GetByIdAsync<TModel>(Guid id) where TModel : IModel;

        public Task<T> GetByIdWithDetailsAsync(Guid id);
        public Task<TModel> GetByIdWithDetailsAsync<TModel>(Guid id) where TModel : IModel;

        public IQueryable<T> IncludeMultiple(IQueryable<T> source);
    }
}
