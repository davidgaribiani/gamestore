﻿using GameStore.DAL.Entities.Orders;

namespace GameStore.DAL.Entities
{
    public interface IOrderDetailRepository : IRepository<OrderDetail>
    {
    }
}
