﻿using System;

namespace GameStore.DAL.Entities
{
    public interface IEntity
    {
        public Guid Id { get; set; }
    }
}