﻿using GameStore.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL.Interfaces
{
    public interface IUserRepository
    {
        public Task<ApplicationUser> GetUserByUserId(Guid userId);
    }
}
