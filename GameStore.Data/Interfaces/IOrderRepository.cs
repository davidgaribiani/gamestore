﻿using GameStore.DAL.Entities.Orders;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public interface IOrderRepository : IRepository<Order>
    {
        public IEnumerable<Order> GetOrdersByCustomerId(Guid customerId);
    }
}
