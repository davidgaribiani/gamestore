﻿using System;

namespace GameStore.DAL.Entities
{
    public interface IModel
    {
        public Guid Id { get; set; }
    }
}
