﻿using GameStore.DAL.Entities;
using GameStore.DAL.Entities.ShoppingCart;


namespace GameStore.DAL.Interfaces
{
    public interface ICartItemRepository : IRepository<CartItem>
    {
    }
}
