﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Entities
{
    public interface IGameRepository : IRepository<Game>
    {
        public IQueryable<Game> GetAllAsQuertyable();
    }
}
