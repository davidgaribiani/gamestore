﻿using GameStore.DAL.Entities.Orders;

namespace GameStore.DAL.Payment
{
    public interface IPaymentService
    {
        public void SetPaymentStrategy(IPaymentStrategy paymentStrategy);

        public void Pay(Order order, IPaymentStrategy paymentStrategy);
    }
}
