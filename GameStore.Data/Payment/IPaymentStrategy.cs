﻿namespace GameStore.DAL.Payment
{
    public interface IPaymentStrategy
    {
        public void Pay(decimal amount);
    }
}
