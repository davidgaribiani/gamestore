﻿using GameStore.DAL.Entities.Orders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.DAL.Data.FluentApiConfigurations
{
    public class OrderDetailConfiguration : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder
                .HasKey(od => od.Id);

            //builder
            //    .HasOne(od => od.Game)
            //    .WithMany(g => g.OrderDetails)
            //    .HasForeignKey(od => od.GameId)
            //    .IsRequired();

            builder
                .HasOne(od => od.Order)
                .WithMany(o => o.OrderDetails)
                .HasForeignKey(od => od.OrderId)
                .IsRequired();

            builder
                .Property(od => od.Quantity)
                .HasColumnType("int")
                .IsRequired();

            builder
                .Property(od => od.Price)
                .HasColumnType("money")
                .IsRequired();
        }
    }
}
