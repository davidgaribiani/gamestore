﻿using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.DAL.Data.FluentApiConfigurations
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasKey(c => c.Id);

            builder
                .Property(c => c.Body)
                .HasMaxLength(600)
                .IsRequired();

            builder
                .Property(c => c.PublicationDate)
                .HasColumnType("datetime2")
                .IsRequired();

            builder
                .HasOne(c => c.ParentComment)
                .WithMany(c => c.ChildComments)
                .HasForeignKey(c => c.ParentCommentId)
                .IsRequired(false);

            builder
                .HasOne(c => c.Game)
                .WithMany(g => g.Comments)
                .HasForeignKey(c => c.GameId)
                .IsRequired();

            builder
                .HasOne(c => c.CommentAuthor)
                .WithMany()
                .HasForeignKey(x => x.CommentAuthorId)
                .IsRequired();
        }
    }
}
