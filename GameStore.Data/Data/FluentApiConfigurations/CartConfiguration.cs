﻿using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.ShoppingCart;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.DAL.Data.FluentApiConfigurations
{
    public class CartConfiguration : IEntityTypeConfiguration<Cart>
    {
        public void Configure(EntityTypeBuilder<Cart> builder)
        {
            builder
                .HasOne<ApplicationUser>(x => x.User)
                .WithOne(x => x.Cart)
                .HasForeignKey<Cart>(x => x.UserId)
                .IsRequired(false);

            builder
                .HasMany<CartItem>(x => x.Items)
                .WithOne(x => x.Cart)
                .HasForeignKey(x => x.CartId);
        }
    }
}
