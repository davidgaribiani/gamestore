﻿using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.DAL.Data.FluentApiConfigurations
{
    internal class ImageConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            // TODO ImageConfiguration : FluentAPI

            builder.HasKey(x => x.Id);

            builder.HasOne(g => g.Game)
                .WithOne(g => g.Image)
                .HasForeignKey<Game>(g => g.ImageId)
                .IsRequired(false);

            builder.Property(x => x.ImageTitle);
            builder.Property(x => x.ImageData);
        }
    }
}
