﻿using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.DAL.Data.FluentApiConfigurations
{
    public class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder
                .HasKey(g => g.Id);

            builder
                .Property(g => g.Name)
                .HasMaxLength(50)
                .IsRequired(true);

            builder
                .Property(g => g.Description)
                .HasMaxLength(450)
                .IsRequired(false);

            builder
                .Property(g => g.DateOfAddition)
                .HasColumnType("datetime2")
                .IsRequired(true);

            builder
                .Property(g => g.Price)
                .HasColumnType("money")
                .IsRequired(true);
        }
    }
}
