﻿using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.DAL.Data.FluentApiConfigurations
{
    internal class GenreConfiguration : IEntityTypeConfiguration<Genre>
    {
        public void Configure(EntityTypeBuilder<Genre> builder)
        {
            builder
                .HasKey(g => g.Id);

            builder
                .HasIndex(g => g.Name)
                .IsUnique();

            builder
                .Property(g => g.Name)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(g => g.ParentGenreId)
                .IsRequired(false);

            builder
                .Property(g => g.Description)
                .HasMaxLength(600)
                .IsRequired(false);

            builder
                .HasOne(cg => cg.ParentGenre)
                .WithMany(pg => pg.ChildGenres)
                .HasForeignKey(cg => cg.ParentGenreId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
