﻿using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.DAL.Repositories;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading.Tasks;

namespace GameStore.DAL.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GameStoreDbContext _context;
        private readonly IMapper _mapper;

        private IGameRepository _gameRepository;
        private IGenreRepository _genreRepository;
        private ICommentRepository _commentRepository;
        private IOrderRepository _orderRepository;
        private IOrderDetailRepository _orderDetailRepository;
        private ICartRepository _cartRepository;
        private ICartItemRepository _cartItemRepository;
        private IUserRepository _userRepository;

        public UnitOfWork(GameStoreDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IGameRepository GameRepository => _gameRepository ??= new GameRepository(_context, _mapper);
        public ICommentRepository CommentRepository => _commentRepository ??= new CommentRepository(_context, _mapper);
        public IOrderRepository OrderRepository => _orderRepository ??= new OrderRepository(_context, _mapper);
        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository ??= new OrderDetailRepository(_context, _mapper);
        public IGenreRepository GenreRepository => _genreRepository ??= new GenreRepository(_context, _mapper);
        public ICartRepository CartRepository => _cartRepository ??= new CartRepository(_context, _mapper);
        public ICartItemRepository CartItemRepository => _cartItemRepository ??= new CartItemRepository(_context, _mapper);
        public IUserRepository UserRepository => _userRepository ??= new UserRepository(_context);

        public EntityEntry<T> ChangeTracking<T>(T entity) where T : class, IEntity
        {
            return _context.Entry(entity);
        }
        public async Task SaveChangesAsync() => await _context.SaveChangesAsync();
        public async Task DisposeAsync() => await _context.DisposeAsync();
    }
}
