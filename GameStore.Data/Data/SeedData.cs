﻿using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.DAL.Data
{
    public class SeedData
    {
        public static async Task Seed(IApplicationBuilder applicationBuilder)
        {
            await SeedRolesToDb(applicationBuilder);
            await SeedUsersToDb(applicationBuilder);
            await SeedGenresToDb(applicationBuilder);
            await SeedGamesToDb(applicationBuilder);
            await SeedFirstGameWithComments(applicationBuilder);
        }

        private static async Task SeedRolesToDb(IApplicationBuilder applicationBuilder)
        {
            using var serviceScope = applicationBuilder.ApplicationServices.CreateScope();

            var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

            if (!await roleManager.RoleExistsAsync(UserRoles.Manager))
                await roleManager.CreateAsync(new ApplicationRole(UserRoles.Manager));

            if (!await roleManager.RoleExistsAsync(UserRoles.Customer))
                await roleManager.CreateAsync(new ApplicationRole(UserRoles.Customer));

            if (!await roleManager.RoleExistsAsync(UserRoles.Administrator))
                await roleManager.CreateAsync(new ApplicationRole(UserRoles.Administrator));
        }

        private static async Task SeedUsersToDb(IApplicationBuilder applicationBuilder)
        {
            using var serviceScope = applicationBuilder.ApplicationServices.CreateScope();

            var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

            var customer = new ApplicationUser()
            {
                FirstName = "Customer",
                LastName = "Customer",
                UserName = "Customer",
                Email = "customer@customer.com",
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var manager = new ApplicationUser()
            {
                FirstName = "Manager",
                LastName = "Manager",
                UserName = "Manager",
                Email = "manager@manager.com",
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var administrator = new ApplicationUser()
            {
                FirstName = "Admin",
                LastName = "Admin",
                UserName = "Admin",
                Email = "admin@admin.com",
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!userManager.Users.Any(x => x.UserName == customer.UserName || x.Email == customer.Email))
            {
                var customerRegistered = await userManager.CreateAsync(customer, "Password@123");

                if (customerRegistered.Succeeded && await roleManager.RoleExistsAsync(UserRoles.Customer))
                {
                    await userManager.AddToRoleAsync(customer, UserRoles.Customer);
                }
            }

            if (!userManager.Users.Any(x => x.UserName == manager.UserName || x.Email == manager.Email))
            {
                var managerRegistered = await userManager.CreateAsync(manager, "Password@123");

                if (managerRegistered.Succeeded && await roleManager.RoleExistsAsync(UserRoles.Manager))
                {
                    await userManager.AddToRoleAsync(manager, UserRoles.Customer);
                    await userManager.AddToRoleAsync(manager, UserRoles.Manager);
                }
            }

            if (!userManager.Users.Any(x => x.UserName == administrator.UserName || x.Email == administrator.Email))
            {
                var adminRegistered = await userManager.CreateAsync(administrator, "Password@123");

                if (adminRegistered.Succeeded && await roleManager.RoleExistsAsync(UserRoles.Administrator))
                {
                    await userManager.AddToRoleAsync(administrator, UserRoles.Customer);
                    await userManager.AddToRoleAsync(administrator, UserRoles.Administrator);
                }
            }
        }

        private static async Task SeedGenresToDb(IApplicationBuilder applicationBuilder)
        {
            using var serviceScope = applicationBuilder.ApplicationServices.CreateScope();

            var context = serviceScope.ServiceProvider.GetRequiredService<GameStoreDbContext>();

            if (!context.Genres.Any())
            {
                await context.AddAsync(new Genre()
                {
                    Name = Genres.Strategy,
                    ChildGenres = new List<Genre>()
                        {
                            new Genre() { Name = Genres.Rally },
                            new Genre() { Name = Genres.Arcade },
                            new Genre() { Name = Genres.Formula },
                            new Genre() { Name = Genres.OffRoad }
                        }
                });

                await context.AddAsync(new Genre() { Name = Genres.RPG });
                await context.AddAsync(new Genre() { Name = Genres.Sports });
                await context.AddAsync(new Genre() { Name = Genres.Races });

                await context.AddAsync(new Genre()
                {
                    Name = Genres.Action,
                    ChildGenres = new List<Genre>()
                        {
                            new Genre() { Name = Genres.FPS },
                            new Genre() { Name = Genres.TPS },
                            new Genre() { Name = Genres.Misc }
                        }
                });

                await context.AddAsync(new Genre() { Name = Genres.Adventure });
                await context.AddAsync(new Genre() { Name = Genres.PuzzleAndSkill });
                await context.AddAsync(new Genre() { Name = Genres.Other });

                await context.SaveChangesAsync();
            }
        }

        private static async Task SeedGamesToDb(IApplicationBuilder applicationBuilder)
        {
            using var serviceScope = applicationBuilder.ApplicationServices.CreateScope();

            var context = serviceScope.ServiceProvider.GetRequiredService<GameStoreDbContext>();

            if (!context.Games.Any())
            {
                var fpsGenre = await context.Genres.FirstOrDefaultAsync(g => g.Name == Genres.FPS);
                var racesGenre = await context.Genres.FirstOrDefaultAsync(g => g.Name == Genres.Races);
                var sportGenre = await context.Genres.FirstOrDefaultAsync(g => g.Name == Genres.Sports);
                var otherGenre = await context.Genres.FirstOrDefaultAsync(g => g.Name == Genres.Other);

                var game1 = new Game()
                {
                    Name = "Valorant",
                    Description = "Valorant is a free-to-play first-person hero shooter developed and published by Riot Games, for Microsoft Windows.",
                    Genres = new List<Genre>() { fpsGenre },
                    Price = 9.99M
                };

                var game2 = new Game()
                {
                    Name = "World of Warcraft: Wrath of the Lich King",
                    Description = "World of Warcraft: Wrath of the Lich King is the second expansion set for the massively multiplayer online role-playing game World of Warcraft, following The Burning Crusade.",
                    Genres = new List<Genre>() { otherGenre }, // MMORPG
                    Price = 24.99M,
                };

                var game3 = new Game()
                {
                    Name = "Gran Turismo 7",
                    Description = "Gran Turismo 7 is a 2022 sim racing video game developed by Polyphony Digital and published by Sony Interactive Entertainment.",
                    Genres = new List<Genre>() { racesGenre, otherGenre },
                    Price = 19.99M,
                };

                var game4 = new Game()
                {
                    Name = "Tekken 7",
                    Description = "Tekken 7 is a fighting game developed and published by Bandai Namco Entertainment. It is the seventh main and ninth overall installment in the Tekken series.",
                    Genres = new List<Genre>() { sportGenre, otherGenre }, // Fighting
                    Price = 19.99M
                };

                await context.Games.AddRangeAsync(game1, game2, game3, game4);
                await context.SaveChangesAsync();
            }
        }

        private static async Task SeedFirstGameWithComments(IApplicationBuilder applicationBuilder)
        {
            using var serviceScope = applicationBuilder.ApplicationServices.CreateScope();

            var context = serviceScope.ServiceProvider.GetRequiredService<GameStoreDbContext>();

            if (context.Games.Any())
            {
                var firstGame = await context.Games.FirstOrDefaultAsync();

                if (firstGame is null) return;

                if (!context.Comments.Any())
                {
                    // track first and last user in Db
                    var firstCommentAuthor = context.Users.Take(1).FirstOrDefault();
                    var secondCommentAuthor = context.Users.Skip(1).Take(1).FirstOrDefault();

                    // seed game with comments
                    firstGame.Comments = new List<Comment>()
                        {
                            new Comment()
                            {
                                CommentAuthor = firstCommentAuthor,
                                Body = "I enjoy playing the game with my homies after tought working days.",
                                PublicationDate = DateTime.UtcNow,
                                Game = firstGame,
                                ChildComments = new List<Comment>()
                                {
                                    new Comment()
                                    {
                                        CommentAuthor = secondCommentAuthor,
                                        Body = "Oh, thank you so much. You've provided such an interesting information. I will definitely try this game, thanks to you.",
                                        PublicationDate = DateTime.UtcNow,
                                        Game = firstGame
                                    }
                                }
                            },

                            new Comment()
                            {
                                CommentAuthor = firstCommentAuthor,
                                Body = "Sheeeeeeesh",
                                PublicationDate = DateTime.UtcNow,
                                Game = firstGame
                            }
                        };

                    await context.SaveChangesAsync();
                }
            }
        }
    }
}


