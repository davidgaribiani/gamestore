﻿namespace GameStore.DAL.Data.Helpers
{
    public static class UserRoles
    {
        public const string Manager = "Manager";
        public const string Customer = "Customer";
        public const string Administrator = "Administrator";
    }
}
