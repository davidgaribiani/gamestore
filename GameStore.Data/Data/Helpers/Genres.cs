﻿namespace GameStore.DAL.Data.Helpers
{
    public static class Genres
    {
        public const string Strategy = "Strategy";
        public const string Rally = "Rally";
        public const string Arcade = "Arcade";
        public const string Formula = "Formula";
        public const string OffRoad = "Off-Road";
        public const string RPG = "RPG";
        public const string Sports = "Sports";
        public const string Races = "Races";
        public const string Action = "Action";
        public const string FPS = "FPS";
        public const string TPS = "TPS";
        public const string Misc = "Misc.";
        public const string Adventure = "Adventure";
        public const string PuzzleAndSkill = "Puzzle & Skill";
        public const string Other = "Other";
    }
}
