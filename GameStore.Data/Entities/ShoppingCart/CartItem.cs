﻿using System;

namespace GameStore.DAL.Entities.ShoppingCart
{
    public class CartItem : IEntity
    {
        public Guid Id { get; set; }

        public Guid GameId { get; set; }
        public Game Game { get; set; }

        public Guid CartId { get; set; }
        public Cart Cart { get; set; }

        public int Quantity { get; set; }
    }
}
