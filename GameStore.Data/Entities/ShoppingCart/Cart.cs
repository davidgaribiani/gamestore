﻿using GameStore.DAL.Entities.Identity;
using System;
using System.Collections.Generic;

namespace GameStore.DAL.Entities.ShoppingCart
{
    public class Cart : IEntity
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }

        public ICollection<CartItem> Items { get; set; } = new List<CartItem>();
    }
}
