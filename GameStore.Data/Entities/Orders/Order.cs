﻿using GameStore.DAL.Entities.Identity;
using System;
using System.Collections.Generic;

namespace GameStore.DAL.Entities.Orders
{
    public class Order : IEntity
    {
        public Guid Id { get; set; }

        public DateTime OrderedAt { get; init; } = DateTime.UtcNow;

        public Guid CustomerId { get; set; }
        public ApplicationUser Customer { get; set; }

        // Details asked on completing order
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PaymentType { get; set; }
        public string? Comments { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; } = new List<OrderDetail>();
    }
}
