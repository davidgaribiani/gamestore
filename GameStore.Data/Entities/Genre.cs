﻿using System;
using System.Collections.Generic;

namespace GameStore.DAL.Entities
{
    public class Genre : IEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? ParentGenreId { get; set; }
        public Genre ParentGenre { get; set; }

        public ICollection<Genre> ChildGenres { get; set; } = new List<Genre>();
        public ICollection<Game> Games { get; set; } = new List<Game>();
    }
}
