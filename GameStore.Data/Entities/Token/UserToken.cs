﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.DAL.Entities.Token
{
    public class UserToken : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string Value { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidTo { get; set; }

        public bool IsRevoked { get; set; }

        public string RefreshToken { get; set; }
    }
}
