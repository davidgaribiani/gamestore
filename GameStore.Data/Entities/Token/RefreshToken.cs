﻿using GameStore.DAL.Entities.Identity;
using System;

namespace GameStore.DAL.Entities.Token
{
    public class RefreshToken : IEntity
    {
        public Guid Id { get; set; }

        public string Token { get; set; }

        public string JwtId { get; set; }

        public bool IsRevoked { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime DateExpires { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
