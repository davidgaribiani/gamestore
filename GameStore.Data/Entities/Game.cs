﻿using System;
using System.Collections.Generic;

namespace GameStore.DAL.Entities
{
    public class Game : IEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateOfAddition { get; set; } = DateTime.UtcNow;

        public DateTime? UpdatedAt { get; set; }

        public decimal Price { get; set; }

        public Guid? ImageId { get; set; }
        public Image Image { get; set; }

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
        public ICollection<Genre> Genres { get; set; } = new List<Genre>();
    }
}
