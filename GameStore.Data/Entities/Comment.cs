﻿using GameStore.DAL.Entities.Identity;
using System;
using System.Collections.Generic;

namespace GameStore.DAL.Entities
{
    public class Comment : IEntity
    {
        public Guid Id { get; set; }

        public string Body { get; set; }

        public DateTime PublicationDate { get; set; } = DateTime.UtcNow;

        public DateTime? DeletedAt { get; set; } // TODO Nullable datetime, DeletedAt, EF Core Temporal Tables

        public Guid GameId { get; set; }
        public Game Game { get; set; }

        public Guid CommentAuthorId { get; set; }
        public ApplicationUser CommentAuthor { get; set; }

        public Guid? ParentCommentId { get; set; }
        public Comment ParentComment { get; set; }

        public ICollection<Comment> ChildComments { get; set; } = new List<Comment>();
    }
}
