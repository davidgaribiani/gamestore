﻿using GameStore.DAL.Entities.Orders;
using GameStore.DAL.Entities.ShoppingCart;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.DAL.Entities.Identity
{
    public enum Gender { NotDefined = 0, Female = 1, Male = 2, Other = 3, }

    public class ApplicationUser : IdentityUser<Guid>
    {
        public ApplicationUser()
        {

        }

        public ApplicationUser(string userName) : base(userName)
        {

        }

        [Required(AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public DateTime BirthDate { get; set; }

        public ICollection<Order> Orders { get; set; } = new List<Order>();

        public Guid? CartId { get; set; }
        public Cart Cart { get; set; }
    }
}
