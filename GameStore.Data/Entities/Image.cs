﻿using System;

namespace GameStore.DAL.Entities
{
    public class Image : IEntity
    {
        public Guid Id { get; set; }

        public string ImageTitle { get; set; }

        public byte[] ImageData { get; set; }

        public Guid GameId { get; set; }
        public Game Game { get; set; }
    }
}
