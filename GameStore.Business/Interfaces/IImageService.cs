﻿using GameStore.BLL.DTOs;

namespace GameStore.BLL.Interfaces
{
    public interface IImageService
    {
        public Task UpdateGameImage(ImageDTO imageDTO);

        public Task<ImageDTO> GetGameImage(Guid gameId);
    }
}
