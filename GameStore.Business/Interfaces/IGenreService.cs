﻿using GameStore.BLL.DTOs;

namespace GameStore.BLL.Interfaces
{
    public interface IGenreService
    {
        public Task<IEnumerable<GenreDTO>> GetAllGenres(bool withDetails = true);
    }
}
