﻿using GameStore.BLL.DTOs;
using System.Security.Claims;

namespace GameStore.BLL.Interfaces
{
    public interface ICommentService
    {
        public Task<IEnumerable<CommentDTO>> GetAllComments(Guid gameId, bool withDetails = true);

        public Task<CommentDTO> GetCommentById(Guid commentId, bool withDetails = true);

        public Task AddComment(CommentDTO comment);

        public Task EditComment(CommentDTO commentDTO);

        public Task DeleteCommentSoft(Guid commentId);

        public Task RecoverCommentSoft(Guid commentId);
    }
}
