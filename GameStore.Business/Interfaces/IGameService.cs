﻿using GameStore.BLL.DTOs;

namespace GameStore.BLL.Interfaces
{
    public interface IGameService
    {
        public Task AddGame(GameDTO gameDTO);

        public Task EditGame(GameDTO gameDTO);

        public Task DeleteGame(Guid id);

        public Task<GameDTO> GetGame(Guid id, bool withDetails = true);

        public Task<HomeDTO> GetAllGamesFilteredWithPaging(int pageNumber, int numberOfItemsPerPage, string containsString, ICollection<GenreDTO> selectedGenres);
    }
}
