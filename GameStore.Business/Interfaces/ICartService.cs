﻿using GameStore.BLL.DTOs;
using System.Security.Claims;

namespace GameStore.BLL.Interfaces
{
    public interface ICartService
    {
        public Task<CartDTO> GetCart();

        public Task AddItemToCart(Guid gameId);

        public Task UpdateCartItemQuantity(Guid cartItemId, int newQuantity);

        public Task RemoveItemFromCart(Guid id);
    }
}
