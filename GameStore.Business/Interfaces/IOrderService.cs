﻿using GameStore.BLL.DTOs;
using System.Security.Claims;

namespace GameStore.BLL.Interfaces
{
    public interface IOrderService
    {
        public Task PlaceOrder(OrderDTO dto);

        public Task<IEnumerable<OrderDTO>> GetMyOrders();
    }
}
