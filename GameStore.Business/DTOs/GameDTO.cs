﻿using GameStore.DAL.Entities;

#pragma warning disable CS8618

namespace GameStore.BLL.DTOs
{
    /// <summary>
    /// Data Transfer Object type for type <see cref="Game"/>
    /// </summary>
    public class GameDTO : IModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateOfAddition { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public decimal Price { get; set; }

        public Guid? ImageId { get; set; }
        public ImageDTO Image { get; set; }

        public ICollection<CommentDTO> Comments { get; set; } = new List<CommentDTO>();
        public ICollection<GenreDTO> Genres { get; set; } = new List<GenreDTO>();
    }
}

