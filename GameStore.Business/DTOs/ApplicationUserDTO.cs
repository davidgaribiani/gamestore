﻿using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace GameStore.BLL.DTOs
{
    /// <summary>
    /// Data Transfer Object type for type <see cref="ApplicationUser"/>
    /// </summary>
    public class ApplicationUserDTO : IModel
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
