﻿using GameStore.DAL.Entities;

namespace GameStore.BLL.DTOs
{
    public class CartDTO : IModel
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUserDTO User { get; set; }

        public ICollection<CartItemDTO> Items { get; set; } = new List<CartItemDTO>();
    }
}
