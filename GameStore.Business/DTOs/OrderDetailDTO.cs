﻿using GameStore.DAL.Entities;

namespace GameStore.BLL.DTOs
{
    public class OrderDetailDTO : IModel
    {
        public Guid Id { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public Guid GameId { get; set; }
        public GameDTO Game { get; set; }

        public Guid OrderId { get; set; }
        public OrderDTO Order { get; set; }
    }
}
