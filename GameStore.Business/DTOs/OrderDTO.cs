﻿using GameStore.DAL.Entities;

namespace GameStore.BLL.DTOs
{
    public class OrderDTO : IModel
    {
        public Guid Id { get; set; }

        public DateTime OrderedAt { get; init; } = DateTime.UtcNow;

        public Guid CustomerId { get; set; }
        public ApplicationUserDTO Customer { get; set; }

        // Details asked on completing order
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PaymentType { get; set; }
        public string? Comments { get; set; }

        public ICollection<OrderDetailDTO> OrderDetails { get; set; } = new List<OrderDetailDTO>();
    }
}
