﻿using GameStore.DAL.Entities;

#pragma warning disable CS8618

namespace GameStore.BLL.DTOs
{
    /// <summary>
    /// Data Transfer Object type for type <see cref="Genre"/>
    /// </summary>
    public class GenreDTO : IModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? ParentGenreId { get; set; }
        public GenreDTO ParentGenre { get; set; }

        public ICollection<GenreDTO> ChildGenres { get; set; } = new List<GenreDTO>();
        public ICollection<GameDTO> Games { get; set; } = new List<GameDTO>();
    }
}
