﻿namespace GameStore.BLL.DTOs
{
    public class HomeDTO
    {
        public int CurrentPageNumber { get; set; }

        public int NumberOfPages { get; set; }

        public ICollection<GameDTO> Games { get; set; } = new List<GameDTO>();
    }
}
