﻿using GameStore.DAL.Entities;

#pragma warning disable CS8618

namespace GameStore.BLL.DTOs
{
    /// <summary>
    /// Data Transfer Object type for type <see cref="Comment"/>
    /// </summary>
    public class CommentDTO : IModel
    {
        public Guid Id { get; set; }

        public string Body { get; set; }

        public DateTime PublicationDate { get; set; }

        public DateTime? DeletedAt { get; set; }

        public Guid GameId { get; set; }
        public GameDTO Game { get; set; }

        public Guid CommentAuthorId { get; set; }
        public ApplicationUserDTO CommentAuthor { get; set; }

        public Guid? ParentCommentId { get; set; }
        public CommentDTO ParentComment { get; set; }

        public ICollection<CommentDTO> ChildComments { get; set; } = new List<CommentDTO>();
    }
}
