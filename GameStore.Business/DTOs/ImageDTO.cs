﻿using GameStore.DAL.Entities;

namespace GameStore.BLL.DTOs
{
    /// <summary>
    /// Data Transfer Object type for type <see cref="Image"/>
    /// </summary>
    public class ImageDTO : IModel
    {
        public Guid Id { get; set; }

        public string ImageTitle { get; set; }

        public byte[] ImageData { get; set; }

        public Guid GameId { get; set; }
    }
}
