﻿using GameStore.DAL.Entities;

namespace GameStore.BLL.DTOs
{
    public class CartItemDTO : IModel
    {
        public Guid Id { get; set; }

        public Guid GameId { get; set; }
        public GameDTO Game { get; set; }

        public Guid CartId { get; set; }
        public CartDTO Cart { get; set; }

        public int Quantity { get; set; }
    }
}
