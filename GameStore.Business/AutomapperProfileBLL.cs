﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.Orders;
using GameStore.DAL.Entities.ShoppingCart;

namespace GameStore.BLL
{
    public class AutomapperProfileBLL : Profile
    {
        public AutomapperProfileBLL()
        {
            // Map
            CreateMap<ApplicationUser, ApplicationUserDTO>().ReverseMap();
            CreateMap<Comment, CommentDTO>().ReverseMap();
            CreateMap<Game, GameDTO>().ReverseMap();
            CreateMap<Genre, GenreDTO>().ReverseMap();
            CreateMap<Image, ImageDTO>().ReverseMap();
            CreateMap<Cart, CartDTO>().ReverseMap();
            CreateMap<CartItem, CartItemDTO>().ReverseMap();
            CreateMap<Order, OrderDTO>().ReverseMap();
            CreateMap<OrderDetail, OrderDetailDTO>().ReverseMap();

            // Projection
            //CreateProjection<ApplicationUser, ApplicationUserDTO>();
            //CreateProjection<Comment, CommentDTO>();
            //CreateProjection<Game, GameDTO>();
            //CreateProjection<Genre, GenreDTO>();
            //CreateProjection<Image, ImageDTO>();
            //CreateProjection<Cart, CartDTO>();
            //CreateProjection<CartItem, CartItemDTO>();
            //CreateProjection<Order, OrderDTO>();
            //CreateProjection<OrderDetail, OrderDetailDTO>();
        }
    }
}
