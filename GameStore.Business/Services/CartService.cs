﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Exceptions;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.ShoppingCart;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace GameStore.BLL.Services
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartService(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<CartDTO> GetCart()
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));
            var cart = await _unitOfWork.CartRepository.GetCartByUserId(userId);
            var result = _mapper.Map<CartDTO>(cart);
            return result;
        }

        public async Task AddItemToCart(Guid gameId)
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            var game = await _unitOfWork.GameRepository.GetByIdAsync(gameId) ?? throw new ArgumentNullException($"Game with Id : {gameId} does not exist");

            var cart = await _unitOfWork.CartRepository.GetCartByUserId(userId);

            if (cart is null)
            {
                var newCart = new Cart()
                {
                    Id = Guid.NewGuid(),
                    UserId = userId,
                    Items = new List<CartItem>() {
                        new CartItem()
                        {
                            GameId = gameId,
                            Quantity = 1
                        }
                    }
                };

                await _unitOfWork.CartRepository.AddAsync(newCart);
                await _unitOfWork.SaveChangesAsync();
            }
            else if (!cart.Items.Any(x => x.GameId == gameId))
            {
                cart.Items.Add(new CartItem()
                {
                    CartId = cart.Id,
                    GameId = gameId,
                    Quantity = 1
                });

                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                cart.Items.First(x => x.GameId == gameId).Quantity++;
                await _unitOfWork.SaveChangesAsync();
            }
        }

        public async Task UpdateCartItemQuantity(Guid cartItemId, int newQuantity)
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            var cartItem = await _unitOfWork.CartItemRepository.GetByIdWithDetailsAsync(cartItemId);

            if (cartItem is null)
            {
                throw new ArgumentNullException(nameof(cartItem));
            }
            else if (cartItem.Cart.UserId != userId)
            {
                throw new HttpResponseException(401); // Unauthorized
            }
            else if (newQuantity <= 0)
            {
                throw new ArgumentOutOfRangeException($"New quantity can not be negative but was '{newQuantity}'");
            }

            cartItem.Quantity = newQuantity;
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveItemFromCart(Guid id)
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            var cartItem = await _unitOfWork.CartItemRepository.GetByIdWithDetailsAsync(id);

            if (cartItem is null)
            {
                throw new ArgumentNullException(nameof(cartItem));
            }

            if (cartItem.Cart.UserId == userId)
            {
                await _unitOfWork.CartItemRepository.RemoveByIdAsync(id);
                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                throw new HttpResponseException(401); // Unauthorized
            }
        }
    }
}
