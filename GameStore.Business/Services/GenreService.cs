﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Entities;

namespace GameStore.BLL.Services
{
    /// <summary>
    /// Provides methods of storing, retreiving and removing objects of type <see cref="Genre"/> from the database.
    /// </summary>
    public class GenreService : IGenreService
    {
        public readonly IUnitOfWork _unitOfWork;
        public readonly IMapper _mapper;

        public GenreService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GenreDTO>> GetAllGenres(bool withDetails = true)
        {
            var genres = withDetails ?
                await _unitOfWork.GenreRepository.GetAllWithDetailsAsync() : await _unitOfWork.GenreRepository.GetAllAsync();
            var result = genres.Where(x => x.ParentGenre is null).Select(g => _mapper.Map<GenreDTO>(g));// TODO remove Parent genre is null
            return result;
        }
    }
}
