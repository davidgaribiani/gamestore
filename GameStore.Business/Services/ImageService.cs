﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Entities;

namespace GameStore.BLL.Services
{
    /// <summary>
    /// Provides methods of uploading/updating and retreiveing object of the type <see cref="Image"/> from the database.
    /// </summary>
    public class ImageService : IImageService
    {
        public readonly IUnitOfWork _unitOfWork;
        public readonly IMapper _mapper;

        public ImageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task UpdateGameImage(ImageDTO imageDTO)
        {
            var game = await _unitOfWork.GameRepository.GetByIdWithDetailsAsync(imageDTO.GameId);

            if (game is null)
            {
                throw new ArgumentNullException(nameof(game));
            }

            var image = _mapper.Map<Image>(imageDTO);

            game.Image = image;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<ImageDTO> GetGameImage(Guid gameId)
        {
            var game = await _unitOfWork.GameRepository.GetByIdWithDetailsAsync(gameId);

            if (game is null)
            {
                throw new ArgumentException($"Game with 'Id' : {gameId} does not exist.");
            }

            var imageDTO = _mapper.Map<ImageDTO>(game.Image);

            return imageDTO;
        }
    }
}
