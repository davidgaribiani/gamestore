﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Data;

namespace GameStore.BLL.Services
{
    /// <summary>
    /// Provides method for storing, retrieving and removing games of type <see cref="Game"/> from the database.
    /// </summary>
    public class GameService : IGameService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GameService(
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddGame(GameDTO gameDTO)
        {
            if (gameDTO == null)
            {
                throw new ArgumentNullException();
            }

            if (string.IsNullOrEmpty(gameDTO.Name))
            {
                throw new ArgumentNullException("Name can not be empty.");
            }

            if (string.IsNullOrEmpty(gameDTO.Description))
            {
                throw new ArgumentNullException("Description can not be empty");
            }


            var entityToAdd = _mapper.Map<Game>(gameDTO);

            var genres = new List<Genre>();

            entityToAdd.Genres.ToList().ForEach(g => genres.Add(_unitOfWork.GenreRepository.GetByName(g.Name).Result));
            entityToAdd.Genres = genres;

            await _unitOfWork.GameRepository.AddAsync(entityToAdd);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task EditGame(GameDTO gameDTO)
        {
            if (gameDTO == null)
            {
                throw new ArgumentNullException();
            }

            if (string.IsNullOrEmpty(gameDTO.Name))
            {
                throw new ArgumentNullException("Name can not be empty.");
            }

            if (string.IsNullOrEmpty(gameDTO.Description))
            {
                throw new ArgumentNullException("Description can not be empty");
            }

            var game = await _unitOfWork.GameRepository.GetByIdWithDetailsAsync(gameDTO.Id);

            if (game is null)
            {
                throw new ArgumentNullException($"Game with entered Id : {gameDTO.Id} does not exist.");
            }

            var genres = new List<Genre>();

            gameDTO.Genres.ToList().ForEach(g => genres.Add(_unitOfWork.GenreRepository.GetByName(g.Name).Result));

            game.Name = gameDTO.Name;
            game.Description = gameDTO.Description;
            game.Genres = genres;
            game.Price = gameDTO.Price;
            game.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteGame(Guid id)
        {
            await _unitOfWork.GameRepository.RemoveByIdAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }


        public async Task<GameDTO> GetGame(Guid id, bool withDetails = true)
        {
            var game = withDetails ? await _unitOfWork.GameRepository.GetByIdWithDetailsAsync(id) : await _unitOfWork.GameRepository.GetByIdAsync(id);
            
            if (game is null)
            {
                throw new ArgumentNullException($"Game with Id : {id} was not found.");
            }

            var result = _mapper.Map<GameDTO>(game);
            return result;
        }

        public async Task<HomeDTO> GetAllGamesFilteredWithPaging(int pageNumber, int numberOfItemsPerPage, string containsString, ICollection<GenreDTO> selectedGenres)
        {
            var result = new HomeDTO(); 

            var games = _unitOfWork.GameRepository.GetAllAsQuertyable();

            if (!string.IsNullOrEmpty(containsString))
            {
                games = games.Where(x => x.Name.ToLower().Contains(containsString.ToLower()));
            }

            if (selectedGenres.Count() > 0)
            {
                games = games.Where(g => g.Genres.Any(x => selectedGenres.Select(sg => sg.Name).Contains(x.Name)));
            }

            result.CurrentPageNumber = pageNumber;
            result.NumberOfPages = games.Count() % numberOfItemsPerPage == 0 ? (games.Count() / numberOfItemsPerPage) : (games.Count() / numberOfItemsPerPage) + 1;
            result.NumberOfPages = result.NumberOfPages == 0 ? 1 : result.NumberOfPages;

            games = games
                .Include(x => x.Genres)
                .Include(x => x.Image);

            var queryResult = await games.Skip((pageNumber - 1) * numberOfItemsPerPage).Take(numberOfItemsPerPage).ToListAsync();

            result.Games = queryResult.Select(x => _mapper.Map<GameDTO>(x)).ToList();

            return result;
        }

    }
}
