﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Exceptions;
using GameStore.BLL.Extensions;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace GameStore.BLL.Services
{
    /// <summary>
    /// Provides method for storing, retrieving and removing <see cref="Game"/> comments of type <see cref="Comment"/> from the database.
    /// </summary>
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CommentService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<CommentDTO>> GetAllComments(Guid gameId, bool withDetails = true)
        {
            var comments = withDetails ? await _unitOfWork.CommentRepository.GetAllWithDetailsAsync() : await _unitOfWork.CommentRepository.GetAllAsync();
            comments = comments.Where(c => c.GameId == gameId && c.ParentComment == null && c.DeletedAt == null);
            var result = comments.Select(c => _mapper.Map<CommentDTO>(c));
            return result;
        }

        public async Task AddComment(CommentDTO commentDTO)
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            if (commentDTO.ParentCommentId != null && commentDTO.ParentCommentId != Guid.Empty)
            {
                var parentComment = await _unitOfWork.CommentRepository.GetByIdWithDetailsAsync(commentDTO.ParentCommentId.ToGuid());

                parentComment.ChildComments.Add(new Comment()
                {
                    CommentAuthor = await _unitOfWork.UserRepository.GetUserByUserId(userId),
                    Game = await _unitOfWork.GameRepository.GetByIdAsync(parentComment.GameId),
                    ParentCommentId = parentComment.Id,
                    Body = commentDTO.Body
                });

                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                var entityToAdd = new Comment()
                {
                    CommentAuthor = await _unitOfWork.UserRepository.GetUserByUserId(userId),
                    Game = await _unitOfWork.GameRepository.GetByIdAsync(commentDTO.GameId),
                    Body = commentDTO.Body
                };

                await _unitOfWork.CommentRepository.AddAsync(entityToAdd);

                await _unitOfWork.SaveChangesAsync();
            }
        }

        public async Task EditComment(CommentDTO commentDTO)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(commentDTO.Id);

            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            if (comment is null)
            {
                throw new NullReferenceException(nameof(comment));
            }

            if (comment.CommentAuthorId == userId || userClaims.IsInRole(UserRoles.Manager) || userClaims.IsInRole(UserRoles.Administrator))
            {
                comment.Body = commentDTO.Body;
                // TODO updatedAt & by  can be added to db and updated here
                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                throw new HttpResponseException(401);
            }
        }

        public async Task DeleteCommentSoft(Guid commentId)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(commentId);

            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            if (comment is null)
            {
                throw new NullReferenceException(nameof(comment));
            }

            if (comment.CommentAuthorId == userId || userClaims.IsInRole(UserRoles.Manager) || userClaims.IsInRole(UserRoles.Administrator))
            {
                comment.DeletedAt = DateTime.Now;
                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                throw new HttpResponseException(401);
            }
        }

        public async Task RecoverCommentSoft(Guid commentId)
        {
            var comment = await _unitOfWork.CommentRepository.GetByIdAsync(commentId);

            if (comment is null)
            {
                throw new NullReferenceException(nameof(comment));
            }

            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            if (comment.CommentAuthorId == userId || userClaims.IsInRole(UserRoles.Manager) || userClaims.IsInRole(UserRoles.Administrator))
            {
                comment.DeletedAt = (DateTime?)null;
                // _unitOfWork.ChangeTracking<Comment>(comment).Property(x => x.DeletedAt).IsModified = true;
                await _unitOfWork.SaveChangesAsync();
            }
            else
            {
                throw new HttpResponseException(401);
            }
        }


        #region CURRENTLY NOT IN USE

        public async Task<CommentDTO> GetCommentById(Guid commentId, bool withDetails = true)
        {
            var comment = withDetails ?
                await _unitOfWork.CommentRepository.GetByIdWithDetailsAsync(commentId) : await _unitOfWork.CommentRepository.GetByIdWithDetailsAsync(commentId);
            var result = _mapper.Map<CommentDTO>(comment);
            return result;
        }

        #endregion

    }
}
