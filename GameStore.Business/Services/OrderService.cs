﻿using AutoMapper;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.Orders;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace GameStore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task PlaceOrder(OrderDTO dto)
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));

            var cart = await _unitOfWork.CartRepository.GetCartByUserId(userId);

            if (cart is null || cart.Items.Count() == 0)
            {
                throw new ArgumentNullException("Cart is empty. Nothing to order.");
            }

            var newOrder = _mapper.Map<Order>(dto);

            newOrder.CustomerId = userId;

            cart.Items.ToList().ForEach(item => newOrder.OrderDetails.Add(new OrderDetail()
            {
                GameId = item.GameId,
                Price = item.Game.Price,
                Quantity = item.Quantity
            }));

            await _unitOfWork.OrderRepository.AddAsync(newOrder);
            cart.Items = null;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<OrderDTO>> GetMyOrders()
        {
            var userClaims = _httpContextAccessor.HttpContext.User;
            var userId = Guid.Parse(userClaims.FindFirstValue(ClaimTypes.NameIdentifier));
            //var user = await _unitOfWork.UserRepository.GetUserByUserId(userId);
            //var orders = user.Orders.ToList();
            var orders = _unitOfWork.OrderRepository.GetOrdersByCustomerId(userId);
            var result = orders.Select(order => _mapper.Map<OrderDTO>(order));
            return result;
        }
    }
}
