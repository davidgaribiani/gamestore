﻿using AutoMapper;
using GameStore.BLL;
using GameStore.DAL;
using GameStore.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests
{
    internal class Configuration
    {
        public static IMapper GetAutomapperConfiguration()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutomapperProfileDAL>();
                cfg.AddProfile<AutomapperProfileBLL>();
                cfg.AddProfile<AutomapperProfilePL>();
            });

            IMapper mapper = mapperConfiguration.CreateMapper();

            return mapper;
        }
    }
}
