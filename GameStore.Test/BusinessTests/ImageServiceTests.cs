﻿using GameStore.DAL.Entities.ShoppingCart;
using GameStore.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using GameStore.BLL.DTOs;
using GameStore.BLL.Services;
using Moq;
using System.Security.Claims;

namespace GameStore.Tests.BusinessTests
{
    public class ImageServiceTests
    {
        [Fact]
        public async Task ImageService_UpdateGameImage_WorksCorrectly()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => new Game { Id = id });

            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var dto = new ImageDTO()
            {
                GameId = Guid.NewGuid()
            };

            // Act
            await imageService.UpdateGameImage(dto);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetByIdWithDetailsAsync(It.Is<Guid>(x => x == dto.GameId)),Times.Once);
            mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task ImageService_PlaceOrder_ThrowsIfGameWithIdDoesNotExist()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => null);

            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var dto = new ImageDTO()
            {
                GameId = Guid.NewGuid()
            };

            // Act
            Func<Task> act = async () => await imageService.UpdateGameImage(dto);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task ImageService_GetGameImage_WorksCorrectly()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => expectedGame);

            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var expected = new ImageDTO() {
                Id = Guid.Parse(UnitTestHelper.firstImageIdStr),
                GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                ImageTitle = "title.jpg",
                ImageData = new byte[5] { 255, 254, 253, 252, 251 }
            };

            var firstGameId = Guid.Parse(UnitTestHelper.firstGameIdStr);

            // Act
            var actual = await imageService.GetGameImage(firstGameId);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetByIdWithDetailsAsync(It.Is<Guid>(x => x == firstGameId)), Times.Once);
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task ImageService_GetGameImage_ThrowsWhenGameDoesNotExist()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => null);

            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            Func<Task> act = async () => await imageService.GetGameImage(Guid.NewGuid());

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }


        private Game expectedGame = new Game()
        {
            Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
            ImageId = Guid.Parse(UnitTestHelper.firstImageIdStr),
            Image = new Image()
            {
                Id = Guid.Parse(UnitTestHelper.firstImageIdStr),
                GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                ImageTitle = "title.jpg",
                ImageData = new byte[5] { 255, 254, 253, 252, 251 }
            }
        };

    }
}
