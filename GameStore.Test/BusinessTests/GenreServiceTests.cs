﻿using FluentAssertions;
using GameStore.BLL.Services;
using GameStore.DAL.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests.BusinessTests
{
    public class GenreServiceTests
    {
        [Fact]
        public async Task GenreService_GetAllGenres_ReturnsValuesWithoutDetails()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.GetAllAsync()).ReturnsAsync(UnitTestHelper.ExpectedGenres);
            
            var expected = UnitTestHelper.ExpectedGenresModels;
            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            
            // Act
            var actual = await genreService.GetAllGenres(false);

            // Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task GenreService_GetAllGenres_ReturnsValuesWithDetails()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.GetAllWithDetailsAsync()).ReturnsAsync(UnitTestHelper.ExpectedGenres);

            var expected = UnitTestHelper.ExpectedGenresModels;
            var genreService = new GenreService(mockUnitOfWork.Object, Configuration.GetAutomapperConfiguration());

            // Act
            var actual = await genreService.GetAllGenres(true);

            // Assert
            actual.Should().BeEquivalentTo(expected);
        }
    }
}
