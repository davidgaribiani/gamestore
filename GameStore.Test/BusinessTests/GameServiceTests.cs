﻿using Castle.Core.Resource;
using FluentAssertions;
using GameStore.BLL.DTOs;
using GameStore.BLL.Services;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace GameStore.Tests.BusinessTests
{
    public class GameServiceTests
    {
        #region AddGame

        [Fact]
        public async Task GameService_AddGame_AddsModel()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(m => m.GameRepository.AddAsync(It.IsAny<Game>()));
            mockUnitOfWork.Setup(m => m.GenreRepository.GetByName(It.IsAny<string>())).ReturnsAsync(() => new Genre());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var game = GameModels.First();

            // Act
            await gameService.AddGame(game);

            // Assert
            mockUnitOfWork.Verify(x => x.GameRepository.AddAsync(It.Is<Game>(x =>
                x.Id == game.Id && x.Name == game.Name && x.Description == game.Description
                && x.DateOfAddition == game.DateOfAddition)), Times.Once);
            mockUnitOfWork.Verify(x => x.GenreRepository.GetByName(It.IsAny<string>()), Times.Exactly(game.Genres.Count()));
            mockUnitOfWork.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task GameService_AddGame_ThrowsExceptionWhenEmptyName()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.AddAsync(It.IsAny<Game>()));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var game = GameModels.First();
            game.Name = string.Empty;

            // Act
            Func<Task> act = async () => await gameService.AddGame(game);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task GameService_AddGame_ThrowsExceptionWhenEmptyDescription()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.AddAsync(It.IsAny<Game>()));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var game = GameModels.First();
            game.Description = string.Empty;

            // Act
            Func<Task> act = async () => await gameService.AddGame(game);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task GameService_AddGame_ThrowsExceptionWhenNull()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.AddAsync(It.IsAny<Game>()));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            Func<Task> act = async () => await gameService.AddGame(null);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        #endregion

        #region EditGame

        [Fact]
        public async Task GameService_EditGame_UpdatesGameDetails()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => new Game());
            mockUnitOfWork.Setup(m => m.GenreRepository.GetByName(It.IsAny<string>())).ReturnsAsync(() => new Genre());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var game = GameModels.First();

            // Act
            await gameService.EditGame(game);

            // Assert
            mockUnitOfWork.Verify(x => x.GameRepository.GetByIdWithDetailsAsync(game.Id), Times.Once());
            mockUnitOfWork.Verify(x => x.GenreRepository.GetByName(It.IsAny<string>()), Times.Exactly(game.Genres.Count()));
            mockUnitOfWork.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task GameService_EditGame_ThrowsWhenNameIsEmpty()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => new Game());
            mockUnitOfWork.Setup(m => m.GenreRepository.GetByName(It.IsAny<string>())).ReturnsAsync(() => new Genre());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var game = GameModels.First();
            game.Name = String.Empty;

            // Act
            Func<Task> act = async () => await gameService.EditGame(game);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task GameService_EditGame_ThrowsWhenDescriptionIsEmpty()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => new Game());
            mockUnitOfWork.Setup(m => m.GenreRepository.GetByName(It.IsAny<string>())).ReturnsAsync(() => new Genre());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var game = GameModels.First();
            game.Description = String.Empty;

            // Act
            Func<Task> act = async () => await gameService.EditGame(game);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task GameService_EditGame_ThrowsWhenGameWithIdDoesNotExist()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync(() => null);
            mockUnitOfWork.Setup(m => m.GenreRepository.GetByName(It.IsAny<string>())).ReturnsAsync(() => new Genre());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var game = GameModels.First();

            // Act
            Func<Task> act = async () => await gameService.EditGame(game);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        #endregion

        #region DeleteGame

        [Fact]
        public async Task GameService_DeleteGame_DeletesGameIfDoesExist()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(m => m.GameRepository.RemoveByIdAsync(It.IsAny<Guid>()));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            await gameService.DeleteGame(Guid.NewGuid());

            // Assert
            mockUnitOfWork.Verify(x => x.GameRepository.RemoveByIdAsync(It.IsAny<Guid>()), Times.Once());
            mockUnitOfWork.Verify(x => x.SaveChangesAsync(), Times.Once);
        }


        #endregion

        #region GetGame

        [Fact]
        public async Task GameService_GetGame_ReturnsModelWithDetails()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == id));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var expected = UnitTestHelper.ExpectedGamesModels.FirstOrDefault(x => x.Id == Guid.Parse(UnitTestHelper.firstGameIdStr));

            // Act
            var actual = await gameService.GetGame(Guid.Parse(UnitTestHelper.firstGameIdStr), true);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>()), Times.Once);
            actual.Should().BeEquivalentTo(UnitTestHelper.ExpectedGamesModels.FirstOrDefault(x => x.Id == Guid.Parse(UnitTestHelper.firstGameIdStr)));
        }

        [Fact]
        public async Task GameService_GetGame_WithDetails_ThrowsIfGameWithIdDoesNotExists()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == id));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var expected = UnitTestHelper.ExpectedGamesModels.FirstOrDefault(x => x.Id == Guid.Parse(UnitTestHelper.firstGameIdStr));

            // Act
            Func<Task> act = async () => await gameService.GetGame(Guid.NewGuid());

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }


        [Fact]
        public async Task GameService_GetGame_ReturnsModelWithoutDetails()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == id));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            var actual = await gameService.GetGame(Guid.Parse(UnitTestHelper.firstGameIdStr), false);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
            actual.Should().BeEquivalentTo(UnitTestHelper.ExpectedGamesModels.FirstOrDefault(x => x.Id == Guid.Parse(UnitTestHelper.firstGameIdStr)));
        }

        [Fact]
        public async Task GameService_GetGame_WithoutDetails_ThrowsIfGameWithIdDoesNotExists()
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == id));

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            Func<Task> act = async () => await gameService.GetGame(Guid.NewGuid(), false);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }


        #endregion

        #region GetAllGamesFilteredWithPaging

        [InlineData("gam", 2)]
        [InlineData("firs", 1)]
        [InlineData("sec", 1)]
        [InlineData("non-existing", 0)]
        [Theory]
        public async Task GameService_GetAllGamesFilteredWithPaging_ReturnsGamesContainingString(string containsString, int expectedGamesCount)
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            mockUnitOfWork.Setup(m => m.GameRepository.GetAllAsQuertyable()).Returns(() => context.Games.AsQueryable());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            const int currentPageNumber = 1;
            const int numberOfItemsPerPage = 10;
            const int expectedNumberOfPages = 1;

            var firstGenre = UnitTestHelper.ExpectedGenres.FirstOrDefault(x => x.Name == UnitTestHelper.firstGenreName);
            var secondGenre = UnitTestHelper.ExpectedGenres.FirstOrDefault(x => x.Name == UnitTestHelper.secondGenreName);
            var thirdGenre = UnitTestHelper.ExpectedGenres.FirstOrDefault(x => x.Name == UnitTestHelper.thirdGenreName);

            // Act
            var result = await gameService.GetAllGamesFilteredWithPaging(currentPageNumber, numberOfItemsPerPage, containsString, new List<GenreDTO>());

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetAllAsQuertyable(), Times.Once);
            Assert.True(result.Games.Count() == expectedGamesCount);
            Assert.True(result.CurrentPageNumber == currentPageNumber);
            Assert.True(result.NumberOfPages == expectedNumberOfPages);
            Assert.True(result.Games.All(x => x.Name.Contains(containsString)));
        }

        [InlineData(UnitTestHelper.firstGenreName, 2)]
        [InlineData(UnitTestHelper.secondGenreName, 1)]
        [InlineData(UnitTestHelper.thirdGenreName, 0)]
        [Theory]
        public async Task GameService_GetAllGamesFilteredWithPaging_ReturnsGameContainingSelectedGenres(string genreName, int expectedGamesCount)
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            mockUnitOfWork.Setup(m => m.GameRepository.GetAllAsQuertyable()).Returns(() => context.Games.AsQueryable());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            const int currentPageNumber = 1;
            const int numberOfItemsPerPage = 10;
            const int expectedNumberOfPages = 1;

            var selectedGenres = new List<GenreDTO>()
            {
                UnitTestHelper.ExpectedGenresModels.First(x => x.Name == genreName)
            };

            // Act
            var result = await gameService.GetAllGamesFilteredWithPaging(currentPageNumber, numberOfItemsPerPage, string.Empty, selectedGenres);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetAllAsQuertyable(), Times.Once);
            Assert.True(result.Games.Count() == expectedGamesCount);
            Assert.True(result.CurrentPageNumber == currentPageNumber);
            Assert.True(result.NumberOfPages == expectedNumberOfPages);
            Assert.True(result.Games.All(x => x.Genres.Any(x => x.Name == genreName)));
        }

        [InlineData("gam")]
        [Theory]
        public async Task GameService_GetAllGamesFilteredWithPaging_ReturnsGameContainingFirstAndSecondGenreAndContainsString(string containsString)
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            mockUnitOfWork.Setup(m => m.GameRepository.GetAllAsQuertyable()).Returns(() => context.Games.AsQueryable());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            const int currentPageNumber = 1;
            const int numberOfItemsPerPage = 10;
            const int expectedNumberOfPages = 1;
            const int expectedGamesCount = 2;

            var selectedGenres = new List<GenreDTO>()
            {
                UnitTestHelper.ExpectedGenresModels.First(x => x.Name == UnitTestHelper.firstGenreName),
                UnitTestHelper.ExpectedGenresModels.First(x => x.Name == UnitTestHelper.secondGenreName)
            };

            // Act
            var result = await gameService.GetAllGamesFilteredWithPaging(currentPageNumber, numberOfItemsPerPage, containsString, selectedGenres);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetAllAsQuertyable(), Times.Once);
            Assert.True(result.Games.Count() == expectedGamesCount);
            Assert.True(result.CurrentPageNumber == currentPageNumber);
            Assert.True(result.NumberOfPages == expectedNumberOfPages);
            Assert.True(result.Games.All(x => selectedGenres.Select(sg => sg.Name).Intersect(x.Genres.Select(x => x.Name)).Count() > 0));
        }


        [InlineData("non-existing-string")]
        [Theory]
        public async Task GameService_GetAllGamesFilteredWithPaging_ReturnsEmptyList(string containsString)
        {
            // Arrange 
            var mockUnitOfWork = new Mock<IUnitOfWork>();

            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            mockUnitOfWork.Setup(m => m.GameRepository.GetAllAsQuertyable()).Returns(() => context.Games.AsQueryable());

            var gameService = new GameService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            const int currentPageNumber = 1;
            const int numberOfItemsPerPage = 10;
            const int expectedNumberOfPages = 1;
            const int expectedGamesCount = 0;

            var selectedGenres = new List<GenreDTO>()
            {
                UnitTestHelper.ExpectedGenresModels.First(x => x.Name == UnitTestHelper.firstGenreName),
                UnitTestHelper.ExpectedGenresModels.First(x => x.Name == UnitTestHelper.secondGenreName)
            };

            // Act
            var result = await gameService.GetAllGamesFilteredWithPaging(currentPageNumber, numberOfItemsPerPage, containsString, selectedGenres);

            // Assert
            mockUnitOfWork.Verify(m => m.GameRepository.GetAllAsQuertyable(), Times.Once);
            Assert.True(result.Games.Count() == expectedGamesCount);
            Assert.True(result.CurrentPageNumber == currentPageNumber);
            Assert.True(result.NumberOfPages == expectedNumberOfPages);
        }


        #endregion

        private ICollection<GameDTO> GameModels => new List<GameDTO>()
        {
            new GameDTO()
            {
                Id = Guid.NewGuid(),
                Name = "first-game",
                Description = "second-game",
                Price = 15m,
                DateOfAddition = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow.AddDays(2),
                Genres = new List<GenreDTO>()
                {
                    new GenreDTO()
                    {
                        Name = "first-genre"
                    },
                    new GenreDTO()
                    {
                        Name = "second-genre"
                    }
                }
            }
        };

    }

}
