﻿using FluentAssertions;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.BLL.Services;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Orders;
using GameStore.DAL.Entities.ShoppingCart;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests.BusinessTests
{
    public class OrderServiceTests
    {
        [InlineData(UnitTestHelper.firstUserIdStr, 2)]
        [InlineData(UnitTestHelper.secondUserIdStr, 3)]
        [InlineData(UnitTestHelper.thirdGenreIdStr, 0)]
        [Theory]
        public async Task OrderService_GetOrders_ReturnsOrders(string userIdStr, int expectedOrderCount)
        {

            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.OrderRepository.GetOrdersByCustomerId(It.IsAny<Guid>())).Returns((Guid id) => ExpectedOrders.Where(x => x.CustomerId == id));

            var claims =  new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, userIdStr) };

            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            var result = await orderService.GetMyOrders();

            // Assert
            mockUnitOfWork.Verify(m => m.OrderRepository.GetOrdersByCustomerId(It.IsAny<Guid>()), Times.Once());

            Assert.True(result.Count() == expectedOrderCount);
        }

        [InlineData(UnitTestHelper.firstUserIdStr)]
        [Theory]
        public async Task OrderService_PlaceOrder_(string userIdStr)
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => ExpectedCarts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.OrderRepository.AddAsync(It.IsAny<Order>()));
            mockUnitOfWork.Setup(m => m.SaveChangesAsync());

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, userIdStr) { }
            };

            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var orderDTO = new OrderDTO()
            {
                Id = Guid.NewGuid(),
                CustomerId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                FirstName = "First",
                LastName = "Last",
                Email = "email@email.com",
                OrderDetails = new List<OrderDetailDTO>()
                {
                    new OrderDetailDTO()
                    {
                        GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                        Price = 5,
                        Quantity = 5
                    },
                    new OrderDetailDTO()
                    {
                        GameId = Guid.Parse(UnitTestHelper.secondGameIdStr),
                        Price = 5,
                        Quantity = 10
                    },
                },
                Comments = "this-is-optional",
                OrderedAt = DateTime.Parse("1/1/2022"),
                PaymentType = "Card",
                Phone = "+995598120600"
            };

            // Act
            await orderService.PlaceOrder(orderDTO);

            // Assert
            mockUnitOfWork.Verify(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>()), Times.Once());
            
            mockUnitOfWork.Verify(m => m.OrderRepository.AddAsync(It.Is<Order>(x => 
                x.Id == orderDTO.Id &&
                x.CustomerId == Guid.Parse(userIdStr) &&
                x.OrderedAt == orderDTO.OrderedAt &&
                x.FirstName == orderDTO.FirstName && 
                x.LastName == orderDTO.LastName && 
                x.Email == orderDTO.Email &&
                x.PaymentType == orderDTO.PaymentType &&
                x.Phone == orderDTO.Phone &&
                x.Comments == orderDTO.Comments)), Times.Once);

            mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task OrderService_PlaceOrder_ThrowsWhenCartIsNull()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.OrderRepository.GetOrdersByCustomerId(It.IsAny<Guid>())).Returns((Guid id) => ExpectedOrders.Where(x => x.CustomerId == id));
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync(() => null);

            var claims = new List<Claim>() 
            { 
                new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString()) { }
            };

            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var orderDTO = new OrderDTO();

            // Act
            Func<Task> act = async () => await orderService.PlaceOrder(orderDTO);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task OrderService_PlaceOrder_ThrowsWhenCartIsEmpty()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.OrderRepository.GetOrdersByCustomerId(It.IsAny<Guid>())).Returns((Guid id) => ExpectedOrders.Where(x => x.CustomerId == id));
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync(() => new Cart());

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString()) { }
            };

            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var orderDTO = new OrderDTO();

            // Act
            Func<Task> act = async () => await orderService.PlaceOrder(orderDTO);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }


        private ICollection<Order> ExpectedOrders => new List<Order>()
        {
            new Order()
            {
                CustomerId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                OrderDetails = new List<OrderDetail>()
                {
                    new OrderDetail(),
                    new OrderDetail(),
                    new OrderDetail()
                }
            },
            new Order()
            {
                CustomerId = Guid.Parse(UnitTestHelper.secondUserIdStr)
            },
            new Order()
            {
                CustomerId = Guid.Parse(UnitTestHelper.secondUserIdStr)
            },
            new Order()
            {
                CustomerId = Guid.Parse(UnitTestHelper.firstUserIdStr)
            },
            new Order()
            {
                CustomerId = Guid.Parse(UnitTestHelper.secondUserIdStr)
            }
        };

        private ICollection<Cart> ExpectedCarts => new List<Cart>()
        {
            new Cart() {
                UserId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                Items = new List<CartItem>()
                {
                    new CartItem()
                    {
                        GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                        Game = new Game()
                        {
                            Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                            Price = 9.99m
                        },
                        Quantity = 5
                    },
                    new CartItem()
                    {
                        GameId = Guid.Parse(UnitTestHelper.secondGameIdStr),
                        Game = new Game()
                        {
                            Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                            Price = 8.99m
                        },
                        Quantity = 10
                    }
                }
            }
        };
    }
}
