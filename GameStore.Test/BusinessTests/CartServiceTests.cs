﻿using FluentAssertions;
using FluentValidation.Results;
using GameStore.BLL.DTOs;
using GameStore.BLL.Interfaces;
using GameStore.BLL.Services;
using GameStore.DAL.Data;
using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.ShoppingCart;
using GameStore.WebApi.Models.Game.Individual;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using System.Security.Claims;
using System.Text.Json.Serialization;

namespace GameStore.Tests.BusinessTests
{
    public class CartServiceTests
    {
        [InlineData(UnitTestHelper.firstUserIdStr, 1)]
        [InlineData(UnitTestHelper.secondUserIdStr, 0)]
        [Theory]
        public async Task CartService_GetCart_ReturnsCartWithItems(string userId, int expectedCartItemsCount)
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).Returns((Guid id) => sut.CartRepository.GetCartByUserId(id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, userId) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            var actual = await cartService.GetCart();

            // Assert
            mockUnitOfWork.Verify(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>()), Times.Once);
            Assert.NotNull(actual);
            Assert.True(actual.Items.Count() == expectedCartItemsCount);
        }


        [Fact]
        public async Task CartService_GetCart_ReturnsEmptyCart()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var newUser = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
                FirstName = "Test",
                LastName = "Test",
                Email = "testtest@email.com",
                UserName = "TestTest",
                PasswordHash = "Password@123"
            };

            await context.Users.AddAsync(newUser);
            await context.SaveChangesAsync();

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).Returns((Guid id) => sut.CartRepository.GetCartByUserId(id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, newUser.Id.ToString()) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            var actual = await cartService.GetCart();

            // Assert
            mockUnitOfWork.Verify(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>()), Times.Exactly(1));
            Assert.Null(actual);
        }

        [InlineData(UnitTestHelper.firstUserIdStr)]
        [InlineData(UnitTestHelper.secondUserIdStr)]
        [Theory]
        public async Task CartService_AddItemToCart_AddsItemToCart(string userIdStr)
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) =>  context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, userIdStr) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var userId = Guid.Parse(userIdStr);

            var cartBeforeAdding = await context.Carts.Include(x => x.Items).FirstOrDefaultAsync(x => x.UserId == userId);
            var cartItemsBeforeAdding = cartBeforeAdding is null ? 0 : cartBeforeAdding.Items.Count;

            // Act
            await cartService.AddItemToCart(Guid.Parse(UnitTestHelper.secondGameIdStr));

            // Assert
            mockUnitOfWork.Verify(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>()), Times.Once);
            mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);

            var cartAfterAdding = await context.Carts.Include(x => x.Items).FirstOrDefaultAsync(x => x.UserId == userId);
            var cartItemsAfterAdding = cartAfterAdding is null ? 0 : cartAfterAdding.Items.Count;

            Assert.True(cartItemsAfterAdding - cartItemsBeforeAdding == 1);
        }

        [Fact]
        public async Task CartService_AddItemToCart_IncreasesItemQuantityByOneWhenExists()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, UnitTestHelper.firstUserIdStr) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var userId = Guid.Parse(UnitTestHelper.firstUserIdStr);
            var gameId = Guid.Parse(UnitTestHelper.firstGameIdStr);

            var cartBeforeAdding = await context.Carts.Include(x => x.Items).FirstOrDefaultAsync(x => x.UserId == userId);
            var cartItemQuantityBeforeAdding = cartBeforeAdding is null ? 0 : cartBeforeAdding.Items.First(x => x.GameId == gameId).Quantity;

            // Act
            await cartService.AddItemToCart(Guid.Parse(UnitTestHelper.firstGameIdStr));

            // Assert
            mockUnitOfWork.Verify(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>()), Times.Once);
            mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);

            var cartAfterAdding = await context.Carts.Include(x => x.Items).FirstOrDefaultAsync(x => x.UserId == userId);
            var cartItemQuantityAfterAdding = cartBeforeAdding is null ? 0 : cartBeforeAdding.Items.First(x => x.GameId == gameId).Quantity;

            Assert.True(cartItemQuantityAfterAdding - cartItemQuantityBeforeAdding == 1);
        }

        [Fact]
        public async Task CartService_AddItemToCart_ThrowsWhenGameWithCurrentIdDoesNotExist()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, UnitTestHelper.firstUserIdStr) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            Func<Task> act = async () => await cartService.AddItemToCart(Guid.NewGuid());

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task CartServic_UpdateCartItemQuantity_UpdatesQuantity()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.CartItemRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.CartItems.Include(x => x.Cart).FirstOrDefault(x => x.Id == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, UnitTestHelper.firstUserIdStr) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var firstCartItemId = Guid.Parse(UnitTestHelper.firstCartItemIdStr);

            // Act
            await cartService.UpdateCartItemQuantity(firstCartItemId, 25);

            // Assert
            Assert.True(context.CartItems.First(x => x.Id == firstCartItemId).Quantity == 25);
        }

        [Fact]
        public async Task CartServic_UpdateCartItemQuantity_ThrowsExceptionOnNegativeValue()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.CartItemRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.CartItems.Include(x => x.Cart).FirstOrDefault(x => x.Id == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, UnitTestHelper.firstUserIdStr) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            var firstCartItemId = Guid.Parse(UnitTestHelper.firstCartItemIdStr);

            var cartItemBeforeUpdate = context.CartItems.FirstOrDefault(x => x.Id == firstCartItemId);

            // Act
            Func<Task> act = async () => await cartService.UpdateCartItemQuantity(firstCartItemId, -5);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task CartServic_UpdateCartItemQuantity_ThrowsWhenUnauthorized()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString() )};

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            Func<Task> act = async () => await cartService.UpdateCartItemQuantity(Guid.Parse(UnitTestHelper.firstCartItemIdStr), -5);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task CartServic_UpdateCartItemQuantity_ThrowsWhenCartItemWithIdDoesNotExist()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString()) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            Func<Task> act = async () => await cartService.UpdateCartItemQuantity(Guid.NewGuid(), -5);

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task CartServic_RemoveItemFromCart_ThrowsOnRemoveNonExisting()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartRepository.GetCartByUserId(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Carts.FirstOrDefault(x => x.UserId == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString()) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            Func<Task> act = async () => await cartService.RemoveItemFromCart(Guid.NewGuid());

            // Assert
            await act.Should().ThrowAsync<Exception>();
        }

        [Fact]
        public async Task CartServic_RemoveItemFromCart_RemovesCartItemWhenIsOwner()
        {
            // Arrange 
            var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var sut = new UnitOfWork(context, UnitTestHelper.CreateMapperProfile());

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.CartItemRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.CartItems.Include(x => x.Cart).FirstOrDefault(x => x.Id == id));
            mockUnitOfWork.Setup(m => m.GameRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Guid id) => context.Games.FirstOrDefault(x => x.Id == id));
            mockUnitOfWork.Setup(m => m.CartItemRepository.RemoveByIdAsync(It.IsAny<Guid>()));
            mockUnitOfWork.Setup(m => m.SaveChangesAsync());

            var claims = new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, UnitTestHelper.firstUserIdStr) };

            var cartService = new CartService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), UnitTestHelper.GetHttpContextAccessor(claims));

            // Act
            await cartService.RemoveItemFromCart(Guid.Parse(UnitTestHelper.firstCartItemIdStr));

            // Assert
            mockUnitOfWork.Verify(m => m.CartItemRepository.RemoveByIdAsync(It.IsAny<Guid>()), Times.Once);
            mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
        }


        private static ICollection<Cart> CartList => new List<Cart>()
        {
            new Cart() { UserId = Guid.Parse(UnitTestHelper.firstUserIdStr) },
            new Cart() { UserId = Guid.Parse(UnitTestHelper.secondUserIdStr) }
        };
    }
}
