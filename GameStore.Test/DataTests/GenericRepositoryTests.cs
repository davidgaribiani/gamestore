﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Repositories;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GameStore.Tests.DataTests
{
    public class GenericRepositoryTests<TEntity> where TEntity : class, IEntity
    {
        private Mock<GameStoreDbContext> mockContext;
        private Mock<DbSet<Game>> mockDbSet;
        private IRepository<Game> repository;
        private Game game;


        public GenericRepositoryTests()
        {
            mockContext = new Mock<GameStoreDbContext>();
            mockDbSet = new Mock<DbSet<Game>>();
            mockContext.Setup(m => m.Set<Game>()).Returns(mockDbSet.Object);
           // mockContext.Setup(m => m.AddAsync(It.IsAny<Game>()));
            repository = new GameRepository(mockContext.Object, Configuration.GetAutomapperConfiguration());
            game = new Game();
        }


        [Fact]
        public async Task AddAsync_ShouldCallProperContextMethods()
        {
            await repository.GetAllAsync();

            mockContext.Verify(m => m.Set<Game>(), Times.Once);
         //   mockDbSet.Verify(m => m.AddAsync(It.IsAny<Game>()), Times.Once);
        }

        [Fact]
        public async Task GetAllAsync_ShouldCallProperContextMethods()
        {
            await repository.GetAllAsync();

            mockContext.Verify(m => m.Set<Game>(), Times.Once);
            mockDbSet.Verify(m => m.ToListAsync(It.IsAny<CancellationToken>()), Times.Once);
            //      mockDbSet.Verify(m => m.Include(), Times.Once);
        }

        [Fact]
        public async Task GetAllWithDetailsAsync_ShouldCallProperContextMethods()
        {
            await repository.GetAllWithDetailsAsync();

            mockContext.Verify(m => m.Set<Game>(), Times.Once);
            mockDbSet.Verify(m => m.ToListAsync(It.IsAny<CancellationToken>()), Times.Once);
        }







    }
}
