﻿using AutoMapper;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace GameStore.Tests.DataTests
{
    public class GenreRepositoryTests
    {
        [Fact]
        public async Task GenreRepository_GetByName_ReturnsSingleValue()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context, Configuration.GetAutomapperConfiguration());

            // Act 
            var firstGenre = await genreRepository.GetByName(UnitTestHelper.firstGenreName);
            var secondGenre = await genreRepository.GetByName(UnitTestHelper.secondGenreName);
            var thirdGenre = await genreRepository.GetByName(UnitTestHelper.thirdGenreName);

            // Assert
            Assert.Equal(new List<Genre>() { firstGenre, secondGenre, thirdGenre }.OrderBy(x => x.Id), expectedGenres.OrderBy(x => x.Id), new GenreEqualityComparer());
        }

        [Fact]
        public async Task GenreRepository_GetByName_ReturnNull()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context, Configuration.GetAutomapperConfiguration());

            // Act 
            var result = await genreRepository.GetByName("non-existing-name");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task GenreRepository_GetAllWithDetailsAsync_ReturnAllValuesWithIncludedEntities()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context, Configuration.GetAutomapperConfiguration());

            // Act 
            var result = await genreRepository.GetAllWithDetailsAsync();

            // Assert
            Assert.Equal(result.OrderBy(x => x.Id), expectedGenres.OrderBy(x => x.Id), new GenreEqualityComparer());
            Assert.Equal(result.SelectMany(x => x.ChildGenres).OrderBy(x => x.Id), expectedGenres.SelectMany(x => x.ChildGenres).OrderBy(x => x.Id), new GenreEqualityComparer());
            Assert.Equal(result.OrderBy(x => x.Id).Select(x => x.ParentGenre), expectedGenres.OrderBy(x => x.Id).Select(x => x.ParentGenre), new GenreEqualityComparer());
            Assert.Equal(result.SelectMany(x => x.Games).OrderBy(x => x.Id), expectedGenres.SelectMany(x => x.Games).OrderBy(x => x.Id), new GameEqualityComparer());
        }

        [Fact]
        public async Task GenreRepository_GetAllAsync_ReturnAllValues()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context, Configuration.GetAutomapperConfiguration());

            // Act 
            var result = await genreRepository.GetAllAsync();

            // Assert
            Assert.Equal(result.OrderBy(x => x.Id), UnitTestHelper.ExpectedGenres.OrderBy(x => x.Id), new GenreEqualityComparer());
            Assert.True(result.Count() == 3);
        }

        private Genre[] expectedGenres = new Genre[]
        {
            new Genre() 
            {
                Id = Guid.Parse(UnitTestHelper.firstGenreIdStr),
                Name = UnitTestHelper.firstGenreName,
                Description = "description",
                ChildGenres = new List<Genre>()
                {
                    new Genre()
                    {
                        Id = Guid.Parse(UnitTestHelper.thirdGenreIdStr),
                        Name = UnitTestHelper.thirdGenreName,
                        Description = "description",
                        ParentGenreId = Guid.Parse(UnitTestHelper.firstGenreIdStr)
                    }
                },
                Games = new List<Game>()
                {
                    new Game()
                    {
                        Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                        Name = UnitTestHelper.firstGameName,
                        Description = "description",
                        DateOfAddition = DateTime.Parse("1/1/2022"),
                        Price = 1,
                    },
                    new Game()
                    {
                        Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                        Name = UnitTestHelper.secondGameName,
                        Description = "description",
                        DateOfAddition = DateTime.Parse("1/1/2022"),
                        Price = 1,
                        ImageId = Guid.Parse(UnitTestHelper.firstImageIdStr)
                    }
                }
            },
            new Genre() 
            { 
                Id = Guid.Parse(UnitTestHelper.secondGenreIdStr), 
                Name = UnitTestHelper.secondGenreName, 
                Description = "description",
                Games = new List<Game>()
                {
                    new Game()
                    {
                        Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                        Name = UnitTestHelper.secondGameName,
                        Description = "description",
                        DateOfAddition = DateTime.Parse("1/1/2022"),
                        Price = 1,
                        ImageId = Guid.Parse(UnitTestHelper.firstImageIdStr)
                    }
                }
            },
            new Genre() 
            { 
                Id = Guid.Parse(UnitTestHelper.thirdGenreIdStr), 
                Name = UnitTestHelper.thirdGenreName, 
                Description = "description", 
                ParentGenreId = Guid.Parse(UnitTestHelper.firstGenreIdStr), 
                ParentGenre = new Genre() 
                {
                    Id = Guid.Parse(UnitTestHelper.firstGenreIdStr), 
                    Name = UnitTestHelper.firstGenreName, Description = "description"
                },
            }
        };
    }
}
