﻿using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GameStore.Tests.DataTests
{
    public class CartRepositoryTests
    {
        [InlineData(UnitTestHelper.firstUserIdStr)]
        [InlineData(UnitTestHelper.secondUserIdStr)]
        [Theory]
        public async Task GameRepository_GetCartByUserId_ReturnsValue(string userIdStr)
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var cartRepository = new CartRepository(context, Configuration.GetAutomapperConfiguration());

            var userId = Guid.Parse(userIdStr);

            // Act 
            var result = await cartRepository.GetCartByUserId(userId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(result, UnitTestHelper.ExpectedCarts.First(x => x.UserId == userId), new CartEqualityComparer());
            Assert.Equal(result.Items, UnitTestHelper.ExpectedCarts.FirstOrDefault(x => x.UserId == userId).Items, new CartItemEqualityComparer());
            Assert.Equal(result.Items.Select(x => x.Game).OrderBy(x => x.Id), UnitTestHelper.ExpectedCarts.FirstOrDefault(x => x.UserId == userId).Items.Select(x => UnitTestHelper.ExpectedGames.FirstOrDefault(g => g.Id == x.GameId)).OrderBy(x => x.Id), new GameEqualityComparer());
        }
    }
}
