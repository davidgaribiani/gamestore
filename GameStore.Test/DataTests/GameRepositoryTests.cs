﻿using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json.Linq;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

#pragma warning disable CS8620 // Argument cannot be used for parameter due to differences in the nullability of reference types.

namespace GameStore.Tests.DataTests
{
    public class GameRepositoryTests
    {
        [Fact]
        public async Task GameRepository_AddAsync_AddsValueToDatabase()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());

            var gameToBeAdded = new Game()
            {
                Id = Guid.NewGuid(),
                Name = "name",
                Description = "description",
                Price = 9.99m,
            };

            // Act 
            await gameRepository.AddAsync(gameToBeAdded);
            await context.SaveChangesAsync();
            var addedGame = await context.Games.LastOrDefaultAsync();

            // Assert
            Assert.Equal(gameToBeAdded, addedGame, new GameEqualityComparer());
            Assert.True(context.Games.Count() == 3, "AddAsync method works incorrect.");

        }

        [Fact]
        public async Task GameRepository_RemoveByIdAsync_RemovesValueFromDatabase()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());

            var gameCountBeforeRemoval = context.Games.Count();

            // Act 
            await gameRepository.RemoveByIdAsync(context.Games.First().Id);
            await context.SaveChangesAsync();

            // Assert
            Assert.True(context.Games.Count() == 1 && gameCountBeforeRemoval == 2, "RemoveByIdAsync method works incorrect.");
        }

        [Fact]
        public async Task GameRepository_GetAllWithDetailsAsync_ReturnsValuesWithIncludedEntities()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());

            // Act 
            var result = await gameRepository.GetAllWithDetailsAsync();

            // Assert
            Assert.True(result.Count() == 2);
            
            Assert.Equal(result.OrderBy(x => x.Id), UnitTestHelper.ExpectedGames.OrderBy(x => x.Id), new GameEqualityComparer());
            
            Assert.Equal(result.SelectMany(x => x.Comments).OrderBy(x => x.Id), UnitTestHelper.ExpectedComments.OrderBy(x => x.Id), new CommentEqualityComparer());
            Assert.Equal(result.SelectMany(x => x.Comments).SelectMany(x => x.ChildComments).OrderBy(x => x.Id), UnitTestHelper.ExpectedComments.Where(x => x.ParentCommentId != null).OrderBy(x => x.Id), new CommentEqualityComparer());
            
            Assert.Equal(result.SelectMany(x => x.Genres).OrderBy(x => x.Id), UnitTestHelper.ExpectedGames.SelectMany(x => x.Genres).OrderBy(x => x.Id), new GenreEqualityComparer());
            Assert.Equal(result.SelectMany(x => x.Genres).SelectMany(x => x.ChildGenres).OrderBy(x => x.Id), UnitTestHelper.ExpectedGames.SelectMany(x => x.Genres).SelectMany(x => x.ChildGenres).OrderBy(x => x.Id), new GenreEqualityComparer());

       //   Assert.Equal(result.OrderBy(x => x.Id).Select(x => x.Image), UnitTestHelper.ExpectedGames.OrderBy(x => x.Id).Select(x => UnitTestHelper.ExpectedImages.FirstOrDefault(ei => ei.Id == x.ImageId)), new ImageEqualityComparer());
         // TODO something wrong in here   
        }

        [Fact]
        public async Task GameRepository_GetAllAsync_ReturnsValues()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());

            // Act 
            var result = await gameRepository.GetAllAsync();

            // Assert
            Assert.True(result.Count() == 2);
            Assert.Equal(result.OrderBy(x => x.Id), UnitTestHelper.ExpectedGames.OrderBy(x => x.Id), new GameEqualityComparer());
        }

        [InlineData(UnitTestHelper.firstGameIdStr)]
        [InlineData(UnitTestHelper.secondGameIdStr)]
        [Theory]
        public async Task GameRepository_GetByIdAsync_ReturnsEntity(string gameIdstr)
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());
            var gameId = Guid.Parse(gameIdstr);

            // Act 
            var result = await gameRepository.GetByIdWithDetailsAsync(gameId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(result, UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == gameId), new GameEqualityComparer());
        }

        [InlineData(UnitTestHelper.firstGameIdStr)]
        [InlineData(UnitTestHelper.secondGameIdStr)]
        [Theory]
        public async Task GameRepository_GetByIdWithDetailsAsync_ReturnsEntityWithIcnludedEntities(string gameIdStr)
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());
            var gameId = Guid.Parse(gameIdStr);

            // Act 
            var result = await gameRepository.GetByIdWithDetailsAsync(gameId);

            // Assert
            Assert.NotNull(result);

            Assert.Equal(result, UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == gameId), new GameEqualityComparer());

            Assert.Equal(result.Comments.OrderBy(x => x.Id), UnitTestHelper.ExpectedComments.Where(x => x.GameId == gameId).OrderBy(x => x.Id), new CommentEqualityComparer());
            Assert.Equal(result.Comments.SelectMany(x => x.ChildComments).OrderBy(x => x.Id), UnitTestHelper.ExpectedComments.Where(x => x.ParentCommentId != null && x.GameId == gameId).OrderBy(x => x.Id), new CommentEqualityComparer());

            Assert.Equal(result.Genres.OrderBy(x => x.Id), UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == gameId).Genres.OrderBy(x => x.Id), new GenreEqualityComparer());
            Assert.Equal(result.Genres.SelectMany(x => x.ChildGenres).OrderBy(x => x.Id), UnitTestHelper.ExpectedGames.FirstOrDefault(x => x.Id == gameId).Genres.SelectMany(x => x.ChildGenres).OrderBy(x => x.Id), new GenreEqualityComparer());

         //   Assert.Equal(result.Image, UnitTestHelper.ExpectedImages.FirstOrDefault(x => x.GameId == gameId), new ImageEqualityComparer());
        }

        [Fact]
        public async Task GameRepository_GetAllAsQuertyable_ReturnsValuesAsQueryable()
        {
            // Arrange
            using var context = new GameStoreDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var gameRepository = new GameRepository(context, UnitTestHelper.CreateMapperProfile());

            // Act 
            var queryable = gameRepository.GetAllAsQuertyable();

            // Assert
            Assert.IsType<Microsoft.EntityFrameworkCore.Internal.InternalDbSet<Game>>(queryable);
        }

    }
}
