﻿using AutoMapper;
using GameStore.BLL;
using GameStore.BLL.DTOs;
using GameStore.DAL;
using GameStore.DAL.Data;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.DAL.Entities.ShoppingCart;
using GameStore.PL;
using GameStore.WebApi.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Moq;
using System.Collections;
using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Reflection.Metadata.Ecma335;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;

#pragma warning disable CS8604 // Possible null reference argument.

namespace GameStore.Tests
{
    public class UnitTestHelper
    {
        public static DbContextOptions<GameStoreDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<GameStoreDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new GameStoreDbContext(options))
            {
                SeedData(context);
            }

            return options;
        }

        public static IMapper CreateMapperProfile()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutomapperProfileDAL>();
                cfg.AddProfile<AutomapperProfileBLL>();
                cfg.AddProfile<AutomapperProfilePL>();
            });

            IMapper mapper = mapperConfiguration.CreateMapper();

            return mapper;
        }

        public static IHttpContextAccessor GetHttpContextAccessor(ICollection<Claim> claims)
        {
            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(claims, "TestAuthType"));

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(m => m.User).Returns(() => claimsPrincipal);

            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            mockHttpContextAccessor.Setup(m => m.HttpContext).Returns(mockHttpContext.Object);

            return mockHttpContextAccessor.Object;
        }

        public static string GenerateTokenWithRolesForUser(ApplicationUser user, ICollection<string> userRoles)
        {
            const string secret = "this-is-just-a-strong-key";
            const string audience = "user";
            const string issuer = "http://localhost:18416";

            var authClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // Add User Role Claims

            foreach (var role in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }

            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret));

            var token = new JwtSecurityToken(
                    issuer: issuer,
                    audience: audience,
                    expires: DateTime.UtcNow.AddMinutes(5),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            return jwtToken;
        }
     
        public static FluentValidator CreateFluentValidator()
        {
            return new FluentValidator();
        }


        #region Games

        public const string firstGameName = "first-game";
        public const string secondGameName = "second-game";

        public const string firstGameIdStr = "63c15b77-59d8-4af4-b383-d31975b7c8dd";
        public const string secondGameIdStr = "cd506dc3-9a0c-444d-b14a-576dd5845222";

        #endregion

        #region Genres

        public const string firstGenreName = "first-genre";
        public const string secondGenreName = "second-genre";
        public const string thirdGenreName = "third-genre";

        public const string firstGenreIdStr = "69d91f48-5d58-4bcd-9549-75838032c223";
        public const string secondGenreIdStr = "eff9dfaa-43d2-42c8-94cc-1da48f31c2ed";
        public const string thirdGenreIdStr = "991d733b-6b88-42f1-a12d-e17d50c664c1";

        #endregion

        #region Users
        public const string firstUserIdStr = "66d1a480-5da8-4c59-81c2-8b1e067b7b5b";
        public const string secondUserIdStr = "7295fc16-2f03-48e6-83d7-04374ec47375";

        #endregion

        #region Comments

        public const string firstCommentIdStr = "2a9a3792-21a9-4599-a05a-db0a58c47062";
        public const string secondCommentIdStr = "6af904b7-4af3-4e29-a343-31717763e86d";
        public const string thirdCommentIdStr = "638fa1c9-beaa-4e19-9bc0-5d84097bd4a7";

        #endregion

        #region Images

        public const string firstImageIdStr = "2a9a3792-21a9-4599-a06a-db0a58c47062";

        #endregion

        #region Cart

        public const string firstCartIdStr = "2a9a3792-21a9-4599-a06a-db0a58c47053";
        public const string secondCartIdStr = "2a9a3792-21a9-4599-a06a-db0a58c47054";

        public const string firstCartItemIdStr = "2a9a3792-21a9-4599-a06a-db0a58c47055";
        public const string secondCartItemIdStr = "2a9a3792-21a9-4599-a06a-db0a58c47056";
        #endregion


        public static void SeedData(GameStoreDbContext context)
        {
            #region Seed Users To Db

            context.Users.AddRange(
                    new ApplicationUser 
                    { 
                        Id = Guid.Parse(UnitTestHelper.firstUserIdStr),
                        FirstName = "First",
                        LastName = "First",
                        BirthDate = DateTime.UtcNow,
                        Email = "first@first.email.com",
                        UserName = "FirstSecond",
                        PasswordHash = "Password@123"
                    },
                    new ApplicationUser 
                    { 
                        Id = Guid.Parse(UnitTestHelper.secondUserIdStr),
                        FirstName = "Second",
                        LastName = "Second",
                        BirthDate = DateTime.UtcNow,
                        Email = "second@second.email.com",
                        UserName = "SecondSecond",
                        PasswordHash = "Password@123"
                    }
                );

            context.SaveChangesAsync();

            #endregion

            #region Seed Genres To Db

            context.Genres.AddRange(
                new Genre() { Id = Guid.Parse(UnitTestHelper.firstGenreIdStr), Name = UnitTestHelper.firstGenreName, Description = "description" },
                new Genre() { Id = Guid.Parse(UnitTestHelper.secondGenreIdStr), Name = UnitTestHelper.secondGenreName, Description = "description" },
                new Genre() { Id = Guid.Parse(UnitTestHelper.thirdGenreIdStr), Name = UnitTestHelper.thirdGenreName, Description = "description", ParentGenreId = Guid.Parse(UnitTestHelper.firstGenreIdStr) });

            context.SaveChangesAsync();

            #endregion

            #region Seed Games To Db

            context.Games.AddRange(
                new Game()
                {
                    Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                    Name = UnitTestHelper.firstGameName,
                    Description = "description",
                    DateOfAddition = DateTime.Parse("1/1/2022"),
                    Genres = new List<Genre> { context.Genres.FirstOrDefault(pt => pt.Id == Guid.Parse(UnitTestHelper.firstGenreIdStr)) },
                    Price = 1,
                },
                new Game()
                {
                    Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                    Name = UnitTestHelper.secondGameName,
                    Description = "description",
                    DateOfAddition = DateTime.Parse("1/1/2022"),
                    Price = 1,
                    Genres = new List<Genre>
                    {
                        context.Genres.FirstOrDefault(pt => pt.Id == Guid.Parse(UnitTestHelper.firstGenreIdStr)),
                        context.Genres.FirstOrDefault(pt => pt.Id == Guid.Parse(UnitTestHelper.secondGenreIdStr))
                    },
                    Image = new Image()
                    {
                        Id = Guid.Parse(firstImageIdStr),
                        ImageTitle = "title.jpg",
                        ImageData = new byte[5] { 255, 255, 255, 255, 255 },
                        GameId = Guid.Parse(UnitTestHelper.secondGameIdStr)
                    }
                }
            ); 

            context.SaveChangesAsync();

            #endregion

            #region Seed Comments To Db

            context.Comments.AddRange(
                    new Comment()
                    {
                        Id = Guid.Parse(UnitTestHelper.firstCommentIdStr),
                        Body = "body",
                        CommentAuthorId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                        GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                        PublicationDate = DateTime.Parse("1/1/2022"),
                    },
                    new Comment()
                    {
                        Id = Guid.Parse(UnitTestHelper.secondCommentIdStr),
                        Body = "body",
                        CommentAuthorId = Guid.Parse(UnitTestHelper.secondUserIdStr),
                        GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                        PublicationDate = DateTime.Parse("1/1/2022"),
                        ParentCommentId = Guid.Parse(UnitTestHelper.firstCommentIdStr)
                    }
                ); 

            context.SaveChangesAsync();

            #endregion

            #region Seed Carts To Db

            context.AddRange(
                new Cart()
                {
                    Id = Guid.Parse(UnitTestHelper.firstCartIdStr),
                    UserId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                    Items = new List<CartItem>
                    {
                        new CartItem()
                        {
                            Id = Guid.Parse(firstCartItemIdStr),
                            CartId = Guid.Parse(firstCartIdStr),
                            GameId = Guid.Parse(firstGameIdStr),
                            Quantity = 5
                        }
                    }
                },
                new Cart()
                {
                    Id = Guid.Parse(UnitTestHelper.secondCartIdStr),
                    UserId = Guid.Parse(UnitTestHelper.secondUserIdStr),
                });

            context.SaveChanges();

            #endregion


        }

        public static ICollection<Genre> ExpectedGenres => new List<Genre>()
        {
            new Genre() { Id = Guid.Parse(UnitTestHelper.firstGenreIdStr), Name = UnitTestHelper.firstGenreName, Description = "description" },
            new Genre() { Id = Guid.Parse(UnitTestHelper.secondGenreIdStr), Name = UnitTestHelper.secondGenreName, Description = "description" },
            new Genre() { Id = Guid.Parse(UnitTestHelper.thirdGenreIdStr), Name = UnitTestHelper.thirdGenreName, Description = "description", ParentGenreId = Guid.Parse(UnitTestHelper.firstGenreIdStr) }
        };

        public static ICollection<GenreDTO> ExpectedGenresModels => new List<GenreDTO>()
        {
            new GenreDTO() { Id = Guid.Parse(UnitTestHelper.firstGenreIdStr), Name = UnitTestHelper.firstGenreName, Description = "description" },
            new GenreDTO() { Id = Guid.Parse(UnitTestHelper.secondGenreIdStr), Name = UnitTestHelper.secondGenreName, Description = "description" },
            new GenreDTO() { Id = Guid.Parse(UnitTestHelper.thirdGenreIdStr), Name = UnitTestHelper.thirdGenreName, Description = "description", ParentGenreId = Guid.Parse(UnitTestHelper.firstGenreIdStr) }
        };

        public static ICollection<Game> ExpectedGames => new List<Game>()
        {
            new Game()
            {
                Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                Name = UnitTestHelper.firstGameName,
                Description = "description",
                DateOfAddition = DateTime.Parse("1/1/2022"),
                Price = 1,
                Genres = new List<Genre>()
                {
                    ExpectedGenres.ElementAt(0)
                }
            },
            new Game()
            {
                Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                Name = UnitTestHelper.secondGameName,
                Description = "description",
                DateOfAddition = DateTime.Parse("1/1/2022"),
                Price = 1,
                ImageId = Guid.Parse(firstImageIdStr),
                Genres = new List<Genre>()
                {
                    ExpectedGenres.ElementAt(0),
                    ExpectedGenres.ElementAt(1)
                }
            }
        };

        public static ICollection<GameDTO> ExpectedGamesModels => new List<GameDTO>()
        {
            new GameDTO()
            {
                Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                Name = UnitTestHelper.firstGameName,
                Description = "description",
                DateOfAddition = DateTime.Parse("1/1/2022"),
                Price = 1,
                Genres = new List<GenreDTO>()
                {
                    ExpectedGenresModels.ElementAt(0)
                }
            },
            new GameDTO()
            {
                Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                Name = UnitTestHelper.secondGameName,
                Description = "description",
                DateOfAddition = DateTime.Parse("1/1/2022"),
                Price = 1,
                ImageId = Guid.Parse(firstImageIdStr),
                Genres = new List<GenreDTO>()
                {
                    ExpectedGenresModels.ElementAt(0),
                    ExpectedGenresModels.ElementAt(1)
                }
            }
        };

        public static ICollection<Image> ExpectedImages => new List<Image>()
        {
            new Image()
            {
                Id = Guid.Parse(firstImageIdStr),
                GameId = Guid.Parse(secondGameIdStr),
                ImageTitle = "title.jpg",
                ImageData = new byte[5] { 255, 255, 255, 255, 255 }
            }
        };

        public static ICollection<Comment> ExpectedComments => new List<Comment>()
        {
            new Comment()
            {
                Id = Guid.Parse(UnitTestHelper.firstCommentIdStr),
                Body = "body",
                CommentAuthorId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                PublicationDate = DateTime.Parse("1/1/2022"),
            },
            new Comment()
            {
                Id = Guid.Parse(UnitTestHelper.secondCommentIdStr),
                Body = "body",
                CommentAuthorId = Guid.Parse(UnitTestHelper.secondUserIdStr),
                GameId = Guid.Parse(UnitTestHelper.firstGameIdStr),
                PublicationDate = DateTime.Parse("1/1/2022"),
                ParentCommentId = Guid.Parse(UnitTestHelper.firstCommentIdStr)
            }
        };

        public static ICollection<Cart> ExpectedCarts => new List<Cart>()
        {
            new Cart()
            {
                Id = Guid.Parse(UnitTestHelper.firstCartIdStr),
                UserId = Guid.Parse(UnitTestHelper.firstUserIdStr),
                Items = new List<CartItem>()
                {
                    ExpectedCartItems.ElementAt(0)
                }
            },
            new Cart()
            {
                Id = Guid.Parse(UnitTestHelper.secondCartIdStr),
                UserId = Guid.Parse(UnitTestHelper.secondUserIdStr),
                Items = new List<CartItem>()
                {
                    ExpectedCartItems.ElementAt(1)
                }
            }
        };

        public static ICollection<CartItem> ExpectedCartItems => new List<CartItem>()
        {
            new CartItem()
            {
                Id =  Guid.Parse(firstCartItemIdStr),
                CartId = Guid.Parse(firstCartIdStr),
                GameId = Guid.Parse(firstGameIdStr),
                Quantity = 5
            },
            new CartItem()
            {
                Id =  Guid.Parse(secondCartItemIdStr),
                CartId = Guid.Parse(secondCartIdStr),
                GameId = Guid.Parse(firstGameIdStr),
                Quantity = 5
            }
        };

        public static ICollection<ApplicationUser> ExpectedUsers => new List<ApplicationUser>()
        {
            new ApplicationUser
            {
                Id = Guid.Parse(UnitTestHelper.firstUserIdStr),
                FirstName = "First",
                LastName = "First",
                BirthDate = DateTime.UtcNow,
                Email = "first@first.email.com",
                UserName = "FirstSecond",
                PasswordHash = "Password@123",
                Cart = ExpectedCarts.FirstOrDefault(x => x.Id == Guid.Parse(firstUserIdStr))
            },
            new ApplicationUser
            {
                Id = Guid.Parse(UnitTestHelper.secondUserIdStr),
                FirstName = "Second",
                LastName = "Second",
                BirthDate = DateTime.UtcNow,
                Email = "second@second.email.com",
                UserName = "SecondSecond",
                PasswordHash = "Password@123",
                Cart = ExpectedCarts.FirstOrDefault(x => x.Id == Guid.Parse(secondUserIdStr))
            }
        };

        public static IQueryable<Game> QuerybaleGames => 
            new Game[]
            {
                new Game()
                {
                    Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                    Name = UnitTestHelper.firstGameName,
                    Description = "description",
                    DateOfAddition = DateTime.Parse("1/1/2022"),
                    Price = 1,
                    Genres = new List<Genre>()
                    {
                        ExpectedGenres.FirstOrDefault(x => x.Name == UnitTestHelper.firstGenreName)
                    }
                },
                new Game()
                {
                    Id = Guid.Parse(UnitTestHelper.secondGameIdStr),
                    Name = UnitTestHelper.secondGameName,
                    Description = "description",
                    DateOfAddition = DateTime.Parse("1/1/2022"),
                    Price = 1,
                    ImageId = Guid.Parse(firstImageIdStr),
                    Genres = new List<Genre>()
                    {
                        ExpectedGenres.FirstOrDefault(x => x.Name == UnitTestHelper.firstGenreName),
                        ExpectedGenres.FirstOrDefault(x => x.Name == UnitTestHelper.secondGenreName)
                    }
                }
            }.AsQueryable();
    }
}
