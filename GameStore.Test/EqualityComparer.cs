﻿using GameStore.DAL.Entities;
using GameStore.DAL.Entities.ShoppingCart;
using System.Diagnostics.CodeAnalysis;

namespace GameStore.Tests
{
    internal class GenreEqualityComparer : IEqualityComparer<Genre>
    {
        public bool Equals([AllowNull] Genre x, [AllowNull] Genre y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.ParentGenreId == y.ParentGenreId
                && x.Description == y.Description
                && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Genre obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class GameEqualityComparer : IEqualityComparer<Game>
    {
        public bool Equals([AllowNull] Game x, [AllowNull] Game y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Description == y.Description
                && x.Name == y.Name
                && x.DateOfAddition == y.DateOfAddition
                && x.Price == y.Price
                && x.ImageId == y.ImageId
                && x.UpdatedAt == y.UpdatedAt;
        }

        public int GetHashCode([DisallowNull] Game obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class CommentEqualityComparer : IEqualityComparer<Comment>
    {
        public bool Equals([AllowNull] Comment x, [AllowNull] Comment y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.ParentCommentId == y.ParentCommentId
                && x.Body == y.Body
                && x.DeletedAt == y.DeletedAt
                && x.GameId == y.GameId
                && x.PublicationDate == y.PublicationDate;
        }

        public int GetHashCode([DisallowNull] Comment obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class ImageEqualityComparer : IEqualityComparer<Image>
    {
        public bool Equals([AllowNull] Image x, [AllowNull] Image y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.ImageTitle == y.ImageTitle
                && x.ImageData == y.ImageData
                && x.GameId == y.GameId;
        }

        public int GetHashCode([DisallowNull] Image obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class CartEqualityComparer : IEqualityComparer<Cart>
    {
        public bool Equals([AllowNull] Cart x, [AllowNull] Cart y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Cart obj)
        {
            return obj.GetHashCode();
        }
    }

    internal class CartItemEqualityComparer : IEqualityComparer<CartItem>
    {
        public bool Equals([AllowNull] CartItem x, [AllowNull] CartItem y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Quantity == y.Quantity
                && x.CartId == y.CartId
                && x.GameId == y.GameId;
        }

        public int GetHashCode([DisallowNull] CartItem obj)
        {
            return obj.GetHashCode();
        }
    }

}
