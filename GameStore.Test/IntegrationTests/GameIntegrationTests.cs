﻿using FluentAssertions;
using GameStore.DAL.Data.Helpers;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Identity;
using GameStore.WebApi.Models.Game;
using GameStore.WebApi.Models.Game.Add;
using GameStore.WebApi.Models.Game.Edit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace GameStore.Tests.IntegrationTests
{
    public class GameIntegrationTests
    {
        private CustomWebApplicationFactory _factory;
        private HttpClient _client;
        private const string RequestUri = "api/game";

        public GameIntegrationTests()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [InlineData(UserRoles.Customer, HttpStatusCode.Forbidden)]
        [InlineData(UserRoles.Manager, HttpStatusCode.OK)]
        [InlineData(UserRoles.Administrator, HttpStatusCode.OK)]
        [Theory]
        public async Task GameController_AddGame_(string role, HttpStatusCode expectedStatusCode)
        {
            // Arrange
            var roles = new List<string>() { role };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(ValidApplicationUser, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(ValidAddGameSubmitVM), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.PostAsync(RequestUri, content);

            // Assert
            httpResponse.StatusCode.Should().Be(expectedStatusCode);
        }

        [Fact]
        public async Task GameController_AddGame_ReturnsBadRequestOnSumbitWithValidationErrorsOnInvalidModel()
        {
            // Arrange
            var game = new EditGameSubmitVM()
            {
                Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                Description = "..."
            };

            var expectedModelValidationErrors = new Dictionary<string, string>();

            expectedModelValidationErrors.Add("name", "Name is required.");
            expectedModelValidationErrors.Add("description", "Description must contain between 5 and 600 characters.");

            var user = new ApplicationUser()
            {
                UserName = "userName",
                Id = Guid.Parse(UnitTestHelper.firstUserIdStr)
            };

            var roles = new List<string>() { UserRoles.Administrator };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(user, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(game), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.PutAsync($"{RequestUri}", content);

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var modelInResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(stringResponse);
            modelInResponse.OrderBy(x => x.Key).Should().BeEquivalentTo(expectedModelValidationErrors.OrderBy(x => x.Key));
        }



        #region AddGame_GET_AddGameModel

        [Fact]
        public async Task GameController_AddGame_ReturnsAddGameModel()
        {
            // Arrange
            var game = new AddGameSubmitVM
            {
                Name = "test",
                Description = "test",
                Price = 1.5m,
            };

            var expected = new AddGameGetVM()
            {
                Genres = new List<GenreCheckBoxVM>()
                {
                    new GenreCheckBoxVM()
                    {
                        Name = UnitTestHelper.firstGenreName,
                        ChildGenres = new List<GenreCheckBoxVM>()
                        {
                            new GenreCheckBoxVM() { Name = UnitTestHelper.thirdGenreName }
                        }
                    },
                    new GenreCheckBoxVM()
                    {
                        Name = UnitTestHelper.secondGenreName,
                    }
                }
            };

            var user = new ApplicationUser()
            {
                UserName = "userName",
                Id = Guid.Parse(UnitTestHelper.firstUserIdStr)
            };

            var roles = new List<string>() { UserRoles.Administrator };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(user, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(game), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model");

            // Assert
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var modelInResponse = JsonConvert.DeserializeObject<AddGameGetVM>(stringResponse);
            modelInResponse.Should().BeEquivalentTo(expected);
        }

        [InlineData(UserRoles.Customer, HttpStatusCode.Forbidden)]
        [InlineData(UserRoles.Manager, HttpStatusCode.OK)]
        [InlineData(UserRoles.Administrator, HttpStatusCode.OK)]
        [Theory]
        public async Task GameController_AddGame_ReturnsStatusCodesOnAuthorizationOnGetAddGameModel(string role, HttpStatusCode expectedStatusCode)
        {
            // Arrange
            var roles = new List<string>() { role };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(ValidApplicationUser, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model");

            // Assert
            httpResponse.StatusCode.Should().Be(expectedStatusCode);
        }

        [Fact]
        public async Task GameController_AddGame_ReturnsUnauthorizedWhenJWTTokenIsNotAppliedOnGetAddGameModel()
        {
            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model");

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        #endregion

        #region EditGame_GET_EditGameModel

        [InlineData(UnitTestHelper.firstGameIdStr, true, false)]
        [InlineData(UnitTestHelper.secondGameIdStr, true, true)]
        [Theory]
        public async Task GameController_EditGame_ReturnsEditGameModelOnGetEditModel(string gameIdStr, bool firstGenreIsChecked, bool secondGenreIsChecked)
        {
            // Arrange
            var game = UnitTestHelper.ExpectedGames.First(x => x.Id == Guid.Parse(gameIdStr));

            var expected = new EditGameVM()
            {
                Id = game.Id,
                Name = game.Name,
                Description = game.Description,
                Price = game.Price,
                Genres = new List<GenreCheckBoxVM>()
                {
                    new GenreCheckBoxVM()
                    {
                        Name = UnitTestHelper.firstGenreName,
                        ChildGenres = new List<GenreCheckBoxVM>()
                        {
                            new GenreCheckBoxVM() { Name = UnitTestHelper.thirdGenreName }
                        },
                        IsChecked = firstGenreIsChecked
                        
                    },
                    new GenreCheckBoxVM()
                    {
                        Name = UnitTestHelper.secondGenreName,
                         IsChecked = secondGenreIsChecked
                    }
                }
            };

            var roles = new List<string>() { UserRoles.Administrator };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(ValidApplicationUser, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model/{gameIdStr}");

            // Assert
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var modelInResponse = JsonConvert.DeserializeObject<EditGameVM>(stringResponse);
            modelInResponse.Should().BeEquivalentTo(expected);
            
        }

        [InlineData(UserRoles.Customer, HttpStatusCode.Forbidden)]
        [InlineData(UserRoles.Manager, HttpStatusCode.OK)]
        [InlineData(UserRoles.Administrator, HttpStatusCode.OK)]
        [Theory]
        public async Task GameController_EditGame_ReturnsCodesOnAuthorizationOnGetEditModel(string role, HttpStatusCode expectedStatusCode)
        {
            // Arrange
            var roles = new List<string>() { role };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(ValidApplicationUser, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(ValidEditGameSubmitVM), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model/{UnitTestHelper.firstGameIdStr}");

            // Assert
            httpResponse.StatusCode.Should().Be(expectedStatusCode);
        }

        [Fact]
        public async Task GameController_EditGame_ReturnsUnauthorizedWhenValidJWTTokenIsNotAppliedOnGetEditModel()
        {
            // Arrange
            var content = new StringContent(JsonConvert.SerializeObject(ValidEditGameSubmitVM), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model/{UnitTestHelper.firstGameIdStr}");

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task GameController_EditGame_ReturnsInternalErrorWhenGameWithIdDoesNotExistOnGetEditModel()
        {
            // Arrange
            var roles = new List<string>() { UserRoles.Administrator };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(ValidApplicationUser, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            // Act
            var httpResponse = await _client.GetAsync($"{RequestUri}/model/{Guid.NewGuid()}");

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        #endregion

        #region EditGame_POST_EditGameSubmitModel

        [Fact]
        public async Task GameController_EditGame_ReturnsInternalErrorWhenGameWithIdDoesNotExistOnModelSubmit()
        {
            // Arrange

            var game = new EditGameSubmitVM()
            {
                Id = Guid.NewGuid(),
                Description = ".....",
                Name = ".....",
            };
            
            var user = new ApplicationUser()
            {
                UserName = "userName",
                Id = Guid.Parse(UnitTestHelper.firstUserIdStr)
            };

            var roles = new List<string>() { UserRoles.Administrator };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(user, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(game), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.PutAsync($"{RequestUri}", content);

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        [Fact]
        public async Task GameController_EditGame_ReturnsBadRequestWithValidationErrorsOnInvalidModelSubmit()
        {
            // Arrange
            var game = new EditGameSubmitVM() 
            {
                Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
                Description = "..." 
            }; 

            var expectedModelValidationErrors = new Dictionary<string, string>();

            expectedModelValidationErrors.Add("name", "Name is required.");
            expectedModelValidationErrors.Add("description", "Description must contain between 5 and 600 characters.");

            var user = new ApplicationUser()
            {
                UserName = "userName",
                Id = Guid.Parse(UnitTestHelper.firstUserIdStr)
            };

            var roles = new List<string>() { UserRoles.Administrator };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(user, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(game), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.PutAsync($"{RequestUri}", content);

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var modelInResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(stringResponse);
            modelInResponse.OrderBy(x => x.Key).Should().BeEquivalentTo(expectedModelValidationErrors.OrderBy(x => x.Key));  
        }

        [InlineData(UserRoles.Customer, HttpStatusCode.Forbidden)]
        [InlineData(UserRoles.Manager, HttpStatusCode.OK)]
        [InlineData(UserRoles.Administrator, HttpStatusCode.OK)]
        [Theory]
        public async Task GameController_EditGame_ReturnsCodesOnAuthorizationOnSubmitModel(string role, HttpStatusCode expectedStatusCode)
        {
            // Arrange
            var roles = new List<string>() { role };

            var jwtToken = UnitTestHelper.GenerateTokenWithRolesForUser(ValidApplicationUser, roles);

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", jwtToken);

            var content = new StringContent(JsonConvert.SerializeObject(ValidEditGameSubmitVM), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.PutAsync($"{RequestUri}", content);

            // Assert
            httpResponse.StatusCode.Should().Be(expectedStatusCode);
        }

        [Fact]
        public async Task GameController_EditGame_ReturnsUnauthorizedWhenValidJWTTokenIsNotAppliedOnModelSubmit()
        {
            // Arrange

            var content = new StringContent(JsonConvert.SerializeObject(ValidEditGameSubmitVM), Encoding.UTF8, "application/json");

            // Act
            var httpResponse = await _client.PutAsync($"{RequestUri}", content);

            // Assert
            httpResponse.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        #endregion

        #region Reusable Test Data

        private AddGameSubmitVM ValidAddGameSubmitVM = new AddGameSubmitVM()
        {
            Name = "test",
            Description = "test",
            Price = 1.5m,
        };

        private EditGameSubmitVM ValidEditGameSubmitVM = new EditGameSubmitVM()
        {
            Id = Guid.Parse(UnitTestHelper.firstGameIdStr),
            Description = "..........",
            Name = ".................."
        };

        private ApplicationUser ValidApplicationUser = new ApplicationUser()
        {
            UserName = "userName",
            Id = Guid.Parse(UnitTestHelper.firstUserIdStr)
        };

        #endregion
    }
}
