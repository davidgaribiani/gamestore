﻿using Castle.Core.Logging;
using GameStore.BLL.Services;
using GameStore.Web.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Tests.ControlleTests
{
    public class GameControllerTests
    {
        private readonly Mock<GameService> _mockGameService;
        private readonly Mock<GenreService> _mockGenreService;
        private readonly ILogger<GameController> _mockLogger;

        private readonly GameController _gameController;

        public GameControllerTests()
        {
            _mockGameService = new Mock<GameService>();
            _mockGenreService = new Mock<GenreService>();
           // _mockLogger = new Mock<ILogger<GameController>>();


          //  _gameController = new GameController(_mockGameService.Object, _mockGenreService.Object, _mockLogger.Object, UnitTestHelper.CreateFluentValidator());
        }

        [Fact]
        public async Task GameController_AddGame_WorksCorrectly()
        {
            
        }

    }
}
