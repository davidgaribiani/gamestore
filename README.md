# GameStore

## Project Description
Game Store - online game store (eCommerce Web Application) with authentication and role-based authorization. Non-authenticated users can only access public resources. To perform certain CRUD operations such as managing games, shopping carts, orders, user comments, and user roles, users must be authorized to.

## Technologies: 
•	.NET 6, ASP.NET Core (Web API), LINQ, Dependency Injection <br />
•	Entity Framework Core (Code First), Data Annotations, FluentAPI  <br />
•	Identity, JWT Token-Based Authorization <br />
•	AutoMapper, Swagger, Serilog <br />
•	HTML, CSS, JavaScript/TypeScript, Angular, Boostrap 5 <br />
•	FluentValidation XUnit, FluentAssertion, Moq, InMemoryDatabaseProvider <br />
•	Azure App Service, Azure SQL, DevOps / AWS Lambda, API Gateway, S3 Bucket, RDS <br />

## Tools: 
•	Visual Studio 2022 Enterprise Edition, Visual Studio Code, Git, Postman

## API Endpoints (not a full list)

#### GET
/api/Cart <br />
/api/Comment/{id}<br />
/api/Comment/game/{id}<br />
/api/Game/model<br />
/api/Game/model/{id}<br />
/api/Game/individual/{id}<br />
/api/Game/small/{id}<br />
/api/Genres<br />
/api/Roles/user-manager<br />
/api/Orders<br />


#### POST
/api/Authentication/login<br />
/api/Authentication/register<br />
/api/Authentication/refresh-token<br />
/api/Cart/game/{id}<br />
/api/Comment<br />
/api/Game<br />
/api/Game/page/{pageNumber}<br />
/api/Genres<br />
/api/Image/game/{id}<br />
/api/Orders<br />

#### PUT
/api/Cart/item/{id}/{quantity}<br />
/api/Comment/delete/{id}<br />
/api/Comment/recover/{id}<br />

#### DELETE
/api/Cart/item/{id}<br />
/api/Game/{id}<br />

