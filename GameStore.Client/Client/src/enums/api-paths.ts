export enum ApiPaths {
    

    // Contorller Path
    AuthenticationContorller = 'authentication',


    // AuthenticationController
    Login = '/auth/login',
    Register = '/auth/register',
    RefreshToken = '/auth/refresh',
    
    
    AddGame = '/game/games',
    EditGame = '/game/games'

}