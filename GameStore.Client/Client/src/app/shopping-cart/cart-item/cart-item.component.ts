import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { DialogService } from 'src/app/services/dialog.service';
import { CartItemModel } from 'src/app/_interfaces/shopping-cart/shopping-cart-item.model';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {


  @Input() item: CartItemModel;
  @Output() delete: any = new EventEmitter();
  @Output() valueChanged: any = new EventEmitter();

  constructor(private router: Router, private cartService: CartService, private dialogService: DialogService, private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.valueChanged.emit();
  }

  onMinus() {
    var newQuantity = this.item.quantity - 1;

    if (newQuantity >= 1) {
      this.cartService.updateItemQuantity(this.item.id, newQuantity).subscribe(
        {
          next: (response: any) => {
            this.item.quantity = newQuantity;
            this.valueChanged.emit();
          },
          error: (err: HttpErrorResponse) => console.log(err)
        }
      );
    }
  }
  onPlus() {
    var newQuantity = this.item.quantity + 1;

    this.cartService.updateItemQuantity(this.item.id, newQuantity).subscribe(
      {
        next: (response: any) => {
          this.item.quantity = newQuantity;
          this.valueChanged.emit();
        },
        error: (err: HttpErrorResponse) => console.log(err)
      }
    );
  }

  onRemoveFromCart(event: any) {
    this.dialogService.openConfirmationDialog("Confiramtion Dialog", "Are you sure you can to remove item from the shopping cart?")
      .result.then((userResponse: boolean) => {
        if (userResponse == true) {
          this.cartService.removeItemFromCart(this.item.id).subscribe(
            {
              next: (response: any) => {
                this.delete.emit(this.item);
                this.valueChanged.emit();
              },
              error: (err: HttpErrorResponse) => console.log(err)
            }
          );
        }
      });
  }



  redirectToGameIndividualPage() {
    const url = `/game/individual/${this.item.game.id}`
    this.router.navigate([url]);
  }

}
