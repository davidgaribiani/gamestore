import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { isEmpty } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';
import { CartModel } from 'src/app/_interfaces/shopping-cart/shopping-cart.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: CartModel;
  isLoaded: boolean = false;
  isEmpty: boolean = false;
  total: number = 0;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.cartService.getCart().subscribe({
      next: (response: any) => {
        this.cart = response;
        this.isLoaded = true;
        this.valueChanged();
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      }
    });
  }

  async deleteItem(item: any) {
    this.cart.items = await this.cart.items.filter(x => x !== item);
    this.valueChanged();

    if (this.cart.items.length == 0) {
      location.reload();
    }
  }

  async valueChanged() {
    this.total = 0;

    if (this.cart == null || this.cart.items.length == 0) {
      this.isEmpty = true;
    } else {
      this.cart.items.forEach(i => {
        this.total = this.total + (i.quantity * i.game.price);
      });
    }

  }

}
