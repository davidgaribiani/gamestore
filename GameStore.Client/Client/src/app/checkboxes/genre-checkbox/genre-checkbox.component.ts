import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { GenreModel } from 'src/app/_interfaces/game/genre-checkbox.model';

@Component({
  selector: 'app-genre-checkbox',
  templateUrl: './genre-checkbox.component.html',
  styleUrls: ['./genre-checkbox.component.css']
})
export class GenreCheckboxComponent implements OnInit {

  @Input() genre: GenreModel;
  @Output() valueChanged: any = new EventEmitter();
  @Output() notifyParent: any = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  notifyParentOnChange() {
    this.valueChanged.emit();
    this.notifyParent.emit();
  }
}
