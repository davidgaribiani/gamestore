import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(private activeModal : NgbActiveModal) { }

  message : any = "Are you sure you want to ?"
  confirmationBoxTitle : any;
  confirmationMessage : any;


  ngOnInit(): void {
  }


  close(state : boolean) {
    this.activeModal.close(state);
  }


}
