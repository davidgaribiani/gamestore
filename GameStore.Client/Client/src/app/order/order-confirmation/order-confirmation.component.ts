import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { OrderConfirmationModel } from 'src/app/_interfaces/game/order-confirmation.model';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.css']
})
export class OrderConfirmationComponent implements OnInit {

  orderConfirmationModel: OrderConfirmationModel = { firstName: '', lastName: '', email: '', phone: '', paymentType: '', comments: '' }
  validationErrors : any;

  constructor(private router : Router, private orderService: OrdersService) { }

  ngOnInit(): void {
  }

  placeOrder() {
    this.orderService.placeOrder(this.orderConfirmationModel).subscribe({
      next: (response: any) => {
        alert('order placed successfully');
        this.router.navigate(["/"]);
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
        this.validationErrors = err.error;
      }
    });
  }

}
