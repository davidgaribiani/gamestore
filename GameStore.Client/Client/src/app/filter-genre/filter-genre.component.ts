import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GenreModel } from '../_interfaces/game/genre-checkbox.model';

@Component({
  selector: 'app-filter-genre',
  templateUrl: './filter-genre.component.html',
  styleUrls: ['./filter-genre.component.css']
})
export class FilterGenreComponent implements OnInit {

  @Input() genre : GenreModel;
  @Output() removed : any = new EventEmitter();
  @Output() notifyParent : any = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onXClick() {
    this.genre.isChecked = false;
    this.removed.emit();
    this.notifyParent.emit();
  }

}
