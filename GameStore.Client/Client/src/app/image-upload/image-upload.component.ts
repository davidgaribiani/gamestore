import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment as env } from 'src/environments/environment';
import { AuthService } from '../services/auth.service';
import { GameImageService } from '../services/game-image.service';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
  selectedImage: File;

  constructor(private router: Router, private httpClient: HttpClient, private authService: AuthService, private gameImageService : GameImageService) { } // TODO move httpClient implementation into separate service

  @Input() gameId: any;

  ngOnInit(): void {
  }

  onFileSelected(event: any) {
    console.log(event.target.files); // event.target.files
    this.selectedImage = <File>event.target.files[0];
  }

  onUpload() {
    const fileData = new FormData();
    fileData.append('image', this.selectedImage, this.selectedImage.name);

    this.gameImageService.addAndAssignImageToTheGame(this.gameId, fileData).subscribe({
      next: (response: any) => {
        location.reload();
      },
      error: (err: HttpErrorResponse) => {

        if (err.status > 200 || err.status < 400) {
          location.reload();
        }
        console.log(err);
      }
    });
  }
  
}
