import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { AuthService } from '../services/auth.service';
import { OrdersService } from '../services/orders.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  users: any;
  orders : any;
  ordersAreLoaded : boolean = true;
  usersListIsLoaded: boolean = false;

  constructor(private authService: AuthService, private adminService: AdminService, private ordersService : OrdersService) { }

  ngOnInit(): void {
    if (this.authService.isAdmin()) {
      this.adminService.getUserManagerModels().subscribe({
        next: (response: any) => {
          this.users = response;
          this.usersListIsLoaded = true;
        },
        error: (err: HttpErrorResponse) => {
          console.log(err);
        }
      });
    }

    this.ordersService.getOrders().subscribe({
      next: (response: any) => {
        this.orders = response;
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      }
    });

  }

  isAdmin = (): any => {
    return this.authService.isAdmin();
  }



  addToRoleManager(userName: any) {
    this.adminService.addUserToRole(userName, "Manager").subscribe({
      next: (response: any) => {
        this.updateUserManagerTable()
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      }
    });
  }

  removeFromRoleManager(userName: any) {
    this.adminService.removeUserFromRole(userName, "Manager").subscribe({
      next: (response: any) => {
        this.updateUserManagerTable();
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      }
    });
  }

  updateUserManagerTable() {
    this.adminService.getUserManagerModels().subscribe({
      next: (response: any) => {
        this.users = response;
      },
      error: (err: HttpErrorResponse) => {
        console.log(err);
      }
    });
  }

}
