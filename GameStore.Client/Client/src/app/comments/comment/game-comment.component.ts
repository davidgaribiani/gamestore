import { style } from '@angular/animations';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { take } from 'rxjs';
import { CommentModel } from 'src/app/_interfaces/game/game-comment.model';
import { AuthService } from 'src/app/services/auth.service';
import { ContentRef } from '@ng-bootstrap/ng-bootstrap/util/popup';
import { CommentService } from 'src/app/services/game-comment.service';
import { EditCommentModel } from 'src/app/_interfaces/game/edit-comment.model';
import { HttpErrorResponse } from '@angular/common/http';
import { AddCommentModel } from 'src/app/_interfaces/game/add-comment.model';
import { AddGameComponent } from 'src/app/games/add-game/add-game.component';



@Component({
  selector: 'app-game-comment',
  templateUrl: './game-comment.component.html',
  styleUrls: ['./game-comment.component.css']
})
export class GameCommentComponent implements OnInit {

  @Input() comment: CommentModel;
  @ViewChild('body', { read: ElementRef }) elemnentRef: ElementRef;
  @Output() reloadRequired: any = new EventEmitter();
  @Output() notifyParentCommentOnReply: any = new EventEmitter();
  isContentEditable: boolean = false;
  contentEditableValue: any;
  replyBody: any;
  replyBodyIsEnabled: boolean = false;

  constructor(private _ngZone: NgZone, private authService: AuthService, private renderer: Renderer2, private cdref: ChangeDetectorRef, private commentService: CommentService) {
  }

  ngOnInit(): void {
    this.contentEditableValue = this.comment.body;
  }

  onBodyChange(body: any) {
    this.contentEditableValue = body;
  }

  makeContentEditable() {
    this.isContentEditable = true;
  }

  saveCommentEdit() {
    var model: EditCommentModel = { id: this.comment.id, body: this.contentEditableValue }

    this.commentService.editComment(model).subscribe({
      next: (response: any) => {
        this.isContentEditable = false;
        this.comment.body = this.contentEditableValue;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    })
  }

  deleteComment() {
    this.commentService.deleteCommentSoft(this.comment.id).subscribe({
      next: (response: any) => {
        this.comment.deletedAt = Date.now();
      },
      error: (err: HttpErrorResponse) => console.log(err)
    })
  }

  recoverComment() {
    this.commentService.recoverCommentSoft(this.comment.id).subscribe({
      next: (response: any) => {
        this.comment.deletedAt = null;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

  cancelCommentEdit() {
    this.isContentEditable = false;
    this.reloadRequired();
  }

  isManagerOrAdmin = (): any => {
    return this.authService.isManagerOrAdmin()
  }

  getUserName = (): any => {
    return this.authService.getClaims().userName;
  }

  onReplyTo() {
    if (this.comment.parentCommentId == null) {
      this.replyBodyIsEnabled = true;
      console.log("parentReceivedAt:" + this.comment.parentCommentId)
    }
    else {
      this.notifyParentCommentOnReply.emit();
      console.log("notSentFromComment:" + this.comment.parentCommentId);
    }
  }

  saveReply() {
    var model: AddCommentModel = { parentCommentId: this.comment.id, gameId: null, body: this.replyBody }
    this.commentService.addComment(model).subscribe({
      next: (response: any) => {
        this.onReloadRequired();
        this.replyBody = '';
        this.replyBodyIsEnabled = false;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

  cancelReply() {
    this.replyBody = '';
    this.replyBodyIsEnabled = false;
  }

  onReloadRequired() {
    this.reloadRequired.emit();
  }
}
