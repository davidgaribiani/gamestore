import { AuthGuard } from './guards/auth.guard';
import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import { AddGameComponent } from './games/add-game/add-game.component';
import { EditGameComponent } from './games/edit-game/edit-game.component';
import { GameIndividualComponent } from './games/game-individual/game-individual.component';
import { CartComponent } from './shopping-cart/cart/cart.component';
import { OrderConfirmationComponent } from './order/order-confirmation/order-confirmation.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ReconstructionWarningPageComponent } from './reconstruction-warning-page/reconstruction-warning-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent, title: "Home" },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent, title: "Register" },
  { path: 'game/add', component: AddGameComponent, title: "Add Game" },
  { path: 'game/edit/:id', component: EditGameComponent, title: "Edit Game" },
  { path: 'game/individual/:id', component: GameIndividualComponent },
  { path: 'profile', component: UserProfileComponent, title: "Profile" },
  { path: 'cart', component: CartComponent, title: "Cart" },
  { path: 'order-confirmation', component: OrderConfirmationComponent, title: "Order Confirmation" },
  { path: 'community', component: ReconstructionWarningPageComponent, title: "Community" },
  { path: 'about', component: ReconstructionWarningPageComponent, title: "About" },
  { path: 'support', component: ReconstructionWarningPageComponent, title: "Support" },
  { path: '**', pathMatch: 'full', component: PageNotFoundComponent, title : '404 Not Found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }