import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { RegisterModel } from '../../_interfaces/auth/register.model';
import { environment } from '../../../environments/environment';
import { ApiPaths } from '../../../enums/api-paths';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';
import { AuthService } from 'src/app/services/auth.service';
import { NgTemplateOutlet } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationFailed: boolean = false;
  credentials: RegisterModel = { firstName: '', lastName: '', email: '', userName: '', password: '', confirmPassword: '', acceptTerms: false };
  validationErrors : any;

  constructor(private router: Router, private modalService: NgbModal, private authService: AuthService) {
  }

  ngOnInit(): void {

  }

  register = (form: NgForm) => {
    if (form.valid) {
      this.authService.register(this.credentials)
        .subscribe({
          next: (response: any) => {
            alert("User registered successfully");
            this.router.navigate(["/"]);
          },
          error: (err: HttpErrorResponse) => {
            if (err.status >= 200 && err.status < 400) {
              alert("User registered successfully");
              this.router.navigate(["/"]);
            } else if (err.status == 400) {
              this.validationErrors = err.error;
            }

            this.registrationFailed = true
          }
        })
    }
  }

  openLoginModal() {
    const loginModalRef = this.modalService.open(LoginComponent);
  }

}