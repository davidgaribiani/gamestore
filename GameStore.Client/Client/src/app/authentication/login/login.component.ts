import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/auth.service';
import { AuthenticatedResponseModel } from 'src/app/_interfaces/auth/authenticated-response.model';
import { LoginModel } from 'src/app/_interfaces/auth/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean;
  credentials: LoginModel = { userName: '', password: '' };
  validationErrors : any;
  rememberMe: boolean = true;

  constructor(private router: Router, private http: HttpClient, private activeModal: NgbActiveModal, private authService: AuthService) {
    this.invalidLogin = false;
  }

  ngOnInit(): void {
  }

  login = (form: NgForm) => {
    if (form.valid) {
      this.authService.login(this.credentials)
        .subscribe({
          next: (response: AuthenticatedResponseModel) => {
            const token = response.token;
            if (response == null) {
              alert('Something went wrong, try again later. Sorry for inconvenience.');
            }
            localStorage.setItem("jwt", token);
            localStorage.setItem('rememberMe', String(this.rememberMe));
            this.invalidLogin = false;
            this.activeModal.close();
            this.router.navigate(["/"]);
          },
          error: (err: HttpErrorResponse) => {
            if (err.status == 400) {
              this.validationErrors = err.error;
              this.invalidLogin = false;
            }
            else if (err.status == 401) {
              this.invalidLogin = true;
              this.validationErrors = null;
            }
          }
        })
    }
  }

  redirectToSignUp() {
    this.activeModal.close();
    this.router.navigate(["register"]);
  }

  closeActiveModal() {
    this.activeModal.close();
  }


}