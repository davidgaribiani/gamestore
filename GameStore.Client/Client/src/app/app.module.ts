import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { JwtModule } from "@auth0/angular-jwt";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './authentication/login/login.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './authentication/register/register.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AddGameComponent } from './games/add-game/add-game.component';
import { GenreCheckboxComponent } from './checkboxes/genre-checkbox/genre-checkbox.component';
import { EditGameComponent } from './games/edit-game/edit-game.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { GameIndividualComponent } from './games/game-individual/game-individual.component';
import { GameCommentComponent } from './comments/comment/game-comment.component';
import { CartComponent } from './shopping-cart/cart/cart.component';
import { GameCardComponent } from './games/game-card/game-card.component';
import { CartItemComponent } from './shopping-cart/cart-item/cart-item.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { FilterGenreComponent } from './filter-genre/filter-genre.component';
import { OrderConfirmationComponent } from './order/order-confirmation/order-confirmation.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FooterComponent } from './footer/footer.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ReconstructionWarningPageComponent } from './reconstruction-warning-page/reconstruction-warning-page.component';

export function tokenGetter() { 
  return localStorage.getItem("jwt"); 
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AddGameComponent,
    EditGameComponent,
    NavbarComponent,
    GenreCheckboxComponent,
    GameIndividualComponent,
    GameCommentComponent,
    CartComponent,
    GameCardComponent,
    CartItemComponent,
    ImageUploadComponent,
    ConfirmationDialogComponent,
    FilterGenreComponent,
    OrderConfirmationComponent,
    FooterComponent,
    UserProfileComponent,
    PageNotFoundComponent,
    ReconstructionWarningPageComponent,
  ],
  imports: [
    BrowserModule,
    NgSelectModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TextFieldModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:44326"],
        disallowedRoutes: []
      }
    }),
    FontAwesomeModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }