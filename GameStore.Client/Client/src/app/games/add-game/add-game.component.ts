import { Router } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm, NonNullableFormBuilder } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AuthenticatedResponseModel } from '../../_interfaces/auth/authenticated-response.model';
import { environment } from '../../../environments/environment';
import { ApiPaths } from '../../../enums/api-paths';
import { GameService } from 'src/app/services/game.service';
import { AuthService } from 'src/app/services/auth.service';
import { GenreModel } from 'src/app/_interfaces/game/genre-checkbox.model';
import { GameModel } from 'src/app/_interfaces/game.model';
import { DatePipe } from '@angular/common';
import { NgSelectConfig } from '@ng-select/ng-select'
import { AddGameModel } from 'src/app/_interfaces/game/add-game.model';


// rxjs/Rx';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css'],
  providers: [
    DatePipe
  ]
})
export class AddGameComponent implements OnInit {
  model: AddGameModel = { name: '', description: '', dateOfAddition: '', price: 0, genres: [] };
  validationErrors: any;


  constructor(private router: Router, private datePipe: DatePipe, private httpClient: HttpClient, private gameService: GameService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.gameService.getAddGameModel()
      .subscribe({
        next: (response: GameModel) => {
          this.model = response;
        },
        error: (err: HttpErrorResponse) => console.log(err)
      })
  }

  addGame = (form: NgForm) => {
    if (form.valid) {

      if (isNaN(this.model.price) || this.model.price < 0) {
        this.model.price = 0;
      }     

      this.model.dateOfAddition = new Date().toISOString();

      this.gameService.addGame(this.model)
        .subscribe({
          next: (response: any) => {
            alert('game added successfully');
            this.router.navigate(["/"]); 
          },
          error: (err: HttpErrorResponse) => {
            if (err.status >= 200 && err.status < 400) {
              alert('game added successfully');
              this.router.navigate(["/"]);
            } else if (err.status == 400) {
              this.validationErrors = err.error;
            }

          }
        })
    }
  }
}

