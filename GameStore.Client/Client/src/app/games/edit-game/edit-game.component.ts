import { DatePipe } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { GameService } from 'src/app/services/game.service';
import { GameModel } from 'src/app/_interfaces/game.model';
import { GenreModel } from 'src/app/_interfaces/game/genre-checkbox.model';
import { environment } from 'src/environments/environment';
import { EditGameModel } from 'src/app/_interfaces/game/edt-game.model';


@Component({
  selector: 'app-edit-game',
  templateUrl: './edit-game.component.html',
  styleUrls: ['./edit-game.component.css']
})
export class EditGameComponent implements OnInit {
  @ViewChild('selectedPublisherDetails', { static: false }) divPublisher: ElementRef;
  model: EditGameModel = { id: '', name: '', description: '', price: 0, genres: [] };
  validationErrors: any;
  id: any;

  constructor(private route: ActivatedRoute, private router: Router, private httpClient: HttpClient, private gameService: GameService, private authService: AuthService) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => this.id = params['id']);

    this.gameService.getEditGameModel(this.id)
      .subscribe({
        next: (response: GameModel) => {
          this.model = response;
          this.model.id = this.id;
        },
        error: (err: HttpErrorResponse) => console.log(err)
      });
  }

  editGame = (form: NgForm) => {
    if (form.valid) {

      if (!this.model.price || isNaN(this.model.price) || this.model.price < 0) {
        this.model.price = 0;
      }     

      this.gameService.editGame(this.model)
        .subscribe({
          next: (response: any) => {
            this.router.navigate(["/"]);
          },
          error: (err: HttpErrorResponse) => {
            if (err.status >= 200 && err.status < 400) {
              alert('game details updated successfully');
              this.router.navigate(["/"]);
            } else if (err.status == 400) {
              this.validationErrors = err.error;
            }
          }
        })
    }
  }

  onSelectChange() {
    this.divPublisher.nativeElement.innerHTML = "";
  }
}
