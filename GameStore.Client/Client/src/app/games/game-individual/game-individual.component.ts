import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { GameIndividualModel } from 'src/app/_interfaces/game/game-individual.model';
import { CommentModel } from 'src/app/_interfaces/game/game-comment.model';
import { ImageUploadComponent } from 'src/app/image-upload/image-upload.component';
import { GameImageModel } from 'src/app/_interfaces/game/game-image.model';
import { GameImageService } from 'src/app/services/game-image.service';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from 'src/app/authentication/login/login.component';
import { CommentService } from 'src/app/services/game-comment.service';
import { AddCommentModel } from 'src/app/_interfaces/game/add-comment.model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-game-individual',
  templateUrl: './game-individual.component.html',
  styleUrls: ['./game-individual.component.css']
})
export class GameIndividualComponent implements OnInit {

  model: GameIndividualModel;
  gameId: any;
  isLoaded: boolean = false;
  addCommentTextAreaValue: any = '';
  addCommentSectionIsEnabled : boolean = false;

  constructor(private titleService : Title, private modalService: NgbModal, private route: ActivatedRoute, private router: Router, private gameService: GameService, private gameImageService: GameImageService, private authService: AuthService, private cartService: CartService, private commentService: CommentService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.gameId = params['id']);

    this.gameService.getIndividualGameModel(this.gameId).subscribe({
      next: (response: any) => {
        this.model = response;
        this.titleService.setTitle(response.name);
        this.isLoaded = true;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });

    // this.commentService.getAllComments(this.gameId).subscribe({
    //   next: (response: any) => {
    //     console.log(response);
    //   },
    //   error: (err: HttpErrorResponse) => console.log(err)
    // });
  }

  onChangeImage() {
    var containerRef = document.getElementById("changeImageContainer");

    if (containerRef) {
      containerRef.style.visibility = "visible";
    }
  }

  isManagerOrAdmin = () => this.authService.isManagerOrAdmin();

  onBuy() {
    if (this.authService.isUserAuthenticated()) {
      this.cartService.addItemToCart(this.gameId).subscribe({
        next: (response: any) => {
          this.router.navigate(["/cart"]);
        },
        error: (err: HttpErrorResponse) => console.log(err)
      });
    }
    else {
      const loginModalRef = this.modalService.open(LoginComponent);
    }
  }




  addComment() {
    var newModel: AddCommentModel = { gameId: this.gameId, body: this.addCommentTextAreaValue, parentCommentId: null };
    this.commentService.addComment(newModel).subscribe({
      next: (response: any) => {
        this.commentService.getAllComments(this.gameId).subscribe({
          next: (response: any) => {
            this.model.comments = response;
            this.deactivateAddCommentSection();
          },
          error: (err: HttpErrorResponse) => console.log(err)
        });
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

  async reloadComments() {
    this.commentService.getAllComments(this.gameId).subscribe({
      next: async (response: any) => {
        this.model.comments = await response;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

  scroll(elementId: any) {
    var elementRef = document.getElementById(elementId);
    elementRef?.scrollIntoView();
  }

  activateAddCommentSection() {
    this.addCommentSectionIsEnabled = true;
  }

  deactivateAddCommentSection() {
    this.addCommentSectionIsEnabled = false;
    this.addCommentTextAreaValue = '';
  }
}
