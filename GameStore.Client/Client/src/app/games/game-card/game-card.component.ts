import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from 'src/app/authentication/login/login.component';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { DialogService } from 'src/app/services/dialog.service';
import { GameService } from 'src/app/services/game.service';
import { GameCardModel } from 'src/app/_interfaces/game/game-card.model';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.css']
})
export class GameCardComponent implements OnInit {

  @Input() game: any;
  @Output() delete: any = new EventEmitter();

  constructor(private dialogService: DialogService, private router: Router, private gameService: GameService, private authService: AuthService, private cartService: CartService, private ngbModal: NgbModal) { }

  ngOnInit(): void {
  }

  isManagerOrAdmin = () => this.authService.isManagerOrAdmin();

  onBuy() {
    if (this.authService.isUserAuthenticated()) {
      this.cartService.addItemToCart(this.game.id).subscribe({
        next: (response: any) => {
          this.router.navigate(["/cart"]);
        },
        error: (err: HttpErrorResponse) => console.log(err)
      });
    }
    else {
      const loginModalRef = this.ngbModal.open(LoginComponent);
    }
  }


  onGameDelete() {
    this.dialogService.openConfirmationDialog("Confiramtion Dialog", "Are you sure you can to remove the game ?")
      .result.then((userResponse: boolean) => {
        if (userResponse == true) {
          this.gameService.deleteGame(this.game.id).subscribe({
            next: (response: any) => {
              this.delete.emit(this.game);
            },
            error: (err: HttpErrorResponse) => console.log(err)
          });
        }
      });
  }


}
