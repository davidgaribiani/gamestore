import { HttpErrorResponse } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { LoginComponent } from '../authentication/login/login.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userName: any;

  constructor(private authService: AuthService, private modalService: NgbModal) {
  }

  async ngOnInit(): Promise<void> {
    if (this.isUserAuthenticated()) {
      this.userName = await this.authService.getClaims().userName;
    }
  }

  isUserAuthenticated = (): boolean => {
    if (this.authService.isUserAuthenticated()) {
      this.userName = this.authService.getClaims().userName;
      return true;
    }

    return false;
  }

  async getUsername() {
    if (this.isUserAuthenticated()) {
      return await this.authService.getClaims().userName;
    }
  }

  logOut = () => this.authService.logOut();

  openLoginModal() {
    const loginModalRef = this.modalService.open(LoginComponent);
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander() {
    if (localStorage.getItem('rememberMe') == 'false') {
      localStorage.clear();
    }
  }
}
