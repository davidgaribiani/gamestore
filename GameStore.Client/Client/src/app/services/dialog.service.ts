import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private ngbModal: NgbModal) { }

  openConfirmationDialog = (confirmationBoxTitle : any, confirmationMessage : any): any => {
    const modalRef = this.ngbModal.open(ConfirmationDialogComponent);
    modalRef.componentInstance.confirmationBoxTitle = confirmationBoxTitle;
    modalRef.componentInstance.confirmationMessage = confirmationMessage;

    return modalRef;
  }
}
