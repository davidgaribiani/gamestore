import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GameImageService {

  constructor(private router: Router, private httpClient: HttpClient, private authService: AuthService) { }

  addAndAssignImageToTheGame = (gameId: any, fileData : FormData ) : any => {
    return this.httpClient.post<any>(`${env.baseUrl}/image/game/${gameId}`, fileData, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    })
  }

  retreiveImageAssignedToTheGame = (gameId : any) : any => {
    return this.httpClient.get<any>(`${env.baseUrl}/image/game/${gameId}`);
  }
}
