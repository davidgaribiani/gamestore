import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httpClient: HttpClient, private authService: AuthService) { }


  getCart = (): any => {
    return this.httpClient.get<any>(`${env.baseUrl}/cart`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }


  addItemToCart = (gameId: any): any => {
    return this.httpClient.post<any>(`${env.baseUrl}/cart/game/${gameId}`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }

  updateItemQuantity = (itemId: any, quantity : any): any => {
    return this.httpClient.put<any>(`${env.baseUrl}/cart/item/${itemId}/${quantity}`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }

  removeItemFromCart = (itemId: any): any => {
    return this.httpClient.delete<any>(`${env.baseUrl}/cart/item/${itemId}`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }
  
  

}
