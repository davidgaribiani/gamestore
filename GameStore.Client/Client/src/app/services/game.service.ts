import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { GenreModel } from '../_interfaces/game/genre-checkbox.model';
import { Observable } from 'rxjs';
import { GameModel } from '../_interfaces/game.model';
import { AddGameModel } from '../_interfaces/game/add-game.model';
import { EditGameModel } from '../_interfaces/game/edt-game.model';
import { FilterModel } from '../_interfaces/game/game-filter.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private url = `${environment.baseUrl}/auth/username}`;


  constructor(private router: Router, private httpClient: HttpClient, private authService: AuthService) { }

  addGame(gameModel: AddGameModel) {
    return this.httpClient.post<any>(`${environment.baseUrl}/game`, gameModel, {
      headers: new HttpHeaders({ "Content-Type": "application/json", 'Authorization': `Bearer ${this.authService.getToken()}` })
    })
  }

  editGame(gameModel: EditGameModel) {
    return this.httpClient.put<any>(`${environment.baseUrl}/game`, gameModel, {
      headers: new HttpHeaders({ "Content-Type": "application/json", 'Authorization': `Bearer ${this.authService.getToken()}` })
    })
  }

  deleteGame(gameId: any) {
    return this.httpClient.delete<any>(`${environment.baseUrl}/game/${gameId}`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    })
  }

  getAddGameModel = (): any => {
    return this.httpClient.get<any>(`${environment.baseUrl}/game/model`,
      { headers: new HttpHeaders({ "Content-Type": "application/json", 'Authorization': `Bearer ${this.authService.getToken()}` }) });
  }

  getEditGameModel = (id: any): any => {
    return this.httpClient.get<any>(`${environment.baseUrl}/game/model/${id}`,
      { headers: new HttpHeaders({ "Content-Type": "application/json", 'Authorization': `Bearer ${this.authService.getToken()}` }) });
  }

  getIndividualGameModel = (id: any): any => {
    return this.httpClient.get<any>(`${environment.baseUrl}/game/individual/${id}`);
  }

  getSmallGameModel = (id: any): any => {
    return this.httpClient.get<any>(`${environment.baseUrl}/game/small/${id}`);
  }

  getAllGames() {
    return this.httpClient.get<any>(`${environment.baseUrl}/games`);
  }

  getGamesFiltered(filter: FilterModel, currentPageNumber: number) {
    return this.httpClient.post<any>(`${environment.baseUrl}/game/page/${currentPageNumber}`, filter)
  }

}

export enum ApiEndpoints {
  AddGame = 'game',  // Http Post
  EditGame = 'game', // Http Put
}



