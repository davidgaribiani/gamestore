import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { environment as env } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    constructor(private httpClient: HttpClient, private authService: AuthService) { }

    getUserManagerModels() {
        return this.httpClient.get<any>(`${env.baseUrl}/roles/user-manager`, {
            headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
        });
    }

    addUserToRole(username: any, role: any) {
        return this.httpClient.put<any>(`${env.baseUrl}/roles/user/${username}/add/${role}`, {
            headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
        });
    }

    removeUserFromRole(username: any, role: any) {
        return this.httpClient.put<any>(`${env.baseUrl}/roles/user/${username}/remove/${role}`, {
            headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
        });
    }
}
