import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { environment as env, environment } from 'src/environments/environment';
import { OrderConfirmationComponent } from '../order/order-confirmation/order-confirmation.component';
import { OrderConfirmationModel } from '../_interfaces/game/order-confirmation.model';

@Injectable({
    providedIn: 'root'
})
export class GenresService {

    constructor(private httpClient: HttpClient, private authService: AuthService) { }


    getGenres () {
        return this.httpClient.get<any>(`${environment.baseUrl}/genres`);
    }
}
