import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { environment as env } from 'src/environments/environment';
import { OrderConfirmationComponent } from '../order/order-confirmation/order-confirmation.component';
import { OrderConfirmationModel } from '../_interfaces/game/order-confirmation.model';

@Injectable({
    providedIn: 'root'
})
export class OrdersService {

    constructor(private httpClient: HttpClient, private authService: AuthService) { }

    placeOrder(model: OrderConfirmationModel) {
        return this.httpClient.post<any>(`${env.baseUrl}/orders`, model, {
            headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
        });
    }

    getOrders() {
        return this.httpClient.get<any>(`${env.baseUrl}/orders`, {
            headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
        });
    }
}
