import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { LoginModel } from '../_interfaces/auth/login.model';
import { RegisterModel } from '../_interfaces/auth/register.model';
import { UserClaimsModel } from '../_interfaces/user-claims.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userName: string = '';

  constructor(private router: Router, private httpClient: HttpClient, private jwtHelper: JwtHelperService) {
  }

  login = (loginModel: LoginModel): any => {
    return this.httpClient.post<AuthenticatorResponse>(`${environment.baseUrl}/authentication/login`, loginModel, {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    });
  }

  register = (registerModel: RegisterModel): any => {
    return this.httpClient.post<any>(`${environment.baseUrl}/authentication/register`, registerModel, {
      headers: new HttpHeaders({ "Content-Type": "application/json" })
    });
  }

  getUsername = (): any => {
    return this.httpClient.get<any>(`${environment.baseUrl}/authentication/username`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.getToken()}`
      })
    });
  }

  isUserAuthenticated = (): boolean => {

    const token = localStorage.getItem("jwt");

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }

    return false;
  }

  getToken = (): any => {
    const token = localStorage.getItem("jwt");
    return token;
  }

  logOut = () => {
    localStorage.removeItem("jwt");
    this.router.navigate(["/"]);
  }


  getClaims = (): any => {

    let jwt = localStorage.getItem('jwt');

    const userNameClaim = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
    const rolesClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

    if (jwt != null) {
      let decodedJWT = JSON.parse(window.atob(jwt.split('.')[1]));
      var claims: UserClaimsModel = { userName: '', roles: '' };
      claims.roles = decodedJWT[rolesClaim];
      claims.userName = decodedJWT[userNameClaim];
      return claims;
    }
  }



  isManager = (): any => {
    if (this.isUserAuthenticated()) {
      let jwt = localStorage.getItem('jwt');
      const rolesClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

      if (jwt != null) {
        let decodedJWT = JSON.parse(window.atob(jwt.split('.')[1]));
        var roles = decodedJWT[rolesClaim];

        return roles.includes("Manager");
      }
    }

    return false;
  }

  isAdmin = (): any => {
    if (this.isUserAuthenticated()) {
      let jwt = localStorage.getItem('jwt');
      const rolesClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

      if (jwt != null) {
        let decodedJWT = JSON.parse(window.atob(jwt.split('.')[1]));
        var roles = decodedJWT[rolesClaim];
        return roles.includes("Administrator");
      }
    }

    return false;
  }

  isManagerOrAdmin = (): any => {
    if (this.isUserAuthenticated()) {
      let jwt = localStorage.getItem('jwt');
      const rolesClaim = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

      if (jwt != null) {
        let decodedJWT = JSON.parse(window.atob(jwt.split('.')[1]));
        var roles = decodedJWT[rolesClaim];

        if (roles.includes("Manager") || roles.includes("Administrator")) {
          return true;
        }
      }
    }
    
    return false;
  }
}

