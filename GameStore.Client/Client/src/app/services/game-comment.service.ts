import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { EditCommentModel } from '../_interfaces/game/edit-comment.model';
import { CommentModel } from '../_interfaces/game/game-comment.model';
import { AddCommentModel } from '../_interfaces/game/add-comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private router: Router, private httpClient: HttpClient, private authService: AuthService) { }

  addComment = (model : AddCommentModel) : any => {
    return this.httpClient.post<any>(`${env.baseUrl}/comment`, model, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }

  editComment = (model : EditCommentModel ) : any => {
    return this.httpClient.put<any>(`${env.baseUrl}/comment`, model, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }

  deleteCommentSoft = (id : any) : any => {
    return this.httpClient.put<any>(`${env.baseUrl}/comment/delete/${id}`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }

  recoverCommentSoft = (id : any ) : any => {
    return this.httpClient.put<any>(`${env.baseUrl}/comment/recover/${id}`, {
      headers: new HttpHeaders({ 'Authorization': `Bearer ${this.authService.getToken()}` })
    });
  }

  getAllComments = (gameId : any ) : any => {
    return this.httpClient.get<any>(`${env.baseUrl}/comment/game/${gameId}`);
  }
}
