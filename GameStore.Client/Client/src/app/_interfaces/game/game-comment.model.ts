import { UserModel } from '../user.model';

export interface CommentModel {
    id: any;
    parentCommentId: any;
    timeSinceAdded : any;
    deletedAt: any;
    body: any;
    commentAuthor: UserModel;
    childComments: CommentModel[];
}