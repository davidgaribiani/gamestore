import { CommentModel } from './game-comment.model';
import { GameImageModel } from './game-image.model';
import { GenreModel } from './genre-checkbox.model';

export interface GameIndividualModel {
    id: any;
    name: string;
    description: string;
    dateOfAddition: any;
    price: number;
    genres: GenreModel[];
    comments: CommentModel[];
    image: GameImageModel;
}