import { GenreModel as GenreModel } from './genre-checkbox.model'

export interface EditGameModel {
    id: any;
    name: string;
    description: string;
    price: number;
    genres: GenreModel[];
}