export interface GenreModel {
    name: string;
    isChecked: boolean;
    childGenres: GenreModel[];
}