import { GenreModel } from "./genre-checkbox.model";

export interface FilterModel {
    containsString: any;
    genres: GenreModel[];
}