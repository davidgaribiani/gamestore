import { GameModel } from "../game.model";

export interface GameImageModel {
    imageTitle: any;
    imageDataUrl: any;
}