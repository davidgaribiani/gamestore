import { UserModel } from '../user.model';

export interface AddCommentModel {
    parentCommentId : any;
    gameId : any;
    body : any;
}