export interface OrderConfirmationModel {
    firstName: any;
    lastName: any;
    email: any;
    phone: any;
    paymentType: any;
    comments: any;
}