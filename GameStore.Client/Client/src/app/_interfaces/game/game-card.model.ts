import { GameImageModel } from './game-image.model';
import { GenreModel } from './genre-checkbox.model';

export interface GameCardModel {
    id: any;
    name: string;
    price: number;
    genres: GenreModel[];
    image: GameImageModel;
}