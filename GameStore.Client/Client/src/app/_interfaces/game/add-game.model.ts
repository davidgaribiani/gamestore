import { GenreModel } from './genre-checkbox.model'

export interface AddGameModel {
    name: string;
    description: string;
    dateOfAddition: any;
    price: number;
    genres: GenreModel[];
}