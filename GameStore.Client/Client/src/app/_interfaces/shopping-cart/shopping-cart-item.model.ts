import { GameCardModel } from "../game/game-card.model";

export interface CartItemModel {
    id: any;
    game: GameCardModel;
    quantity: number;
}