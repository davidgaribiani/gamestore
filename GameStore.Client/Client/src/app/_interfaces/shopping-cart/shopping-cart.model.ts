import { UserModel } from "../user.model";
import { CartItemModel } from "./shopping-cart-item.model";

export interface CartModel {
    items: CartItemModel[];
    user: UserModel;
}