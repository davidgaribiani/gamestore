import { GenreModel } from './game/genre-checkbox.model'

export interface GameModel {
    id: any;
    name: string;
    description: string;
    releaseDate: any;
    dateOfAddition: any;
    price: number;
    genres: GenreModel[];
}