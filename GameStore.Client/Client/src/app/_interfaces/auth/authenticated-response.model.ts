export interface AuthenticatedResponseModel {
        token: string;
        refreshToken : string;
        expiresAt : string; 
}
