import { JwtHelperService } from '@auth0/angular-jwt';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewContainerRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { GameService } from '../services/game.service';
import { AuthService } from '../services/auth.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { FilterModel } from '../_interfaces/game/game-filter.model';
import { GenreModel } from '../_interfaces/game/genre-checkbox.model';
import { LoginComponent } from '../authentication/login/login.component';
import { GameCardModel } from '../_interfaces/game/game-card.model';
import { environment } from 'src/environments/environment';
import { AddGameModel } from '../_interfaces/game/add-game.model';
import { delay } from 'rxjs';
import { NgTemplateOutlet } from '@angular/common';
import { GenresService } from '../services/genres.servis';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  games: GameCardModel[];
  genres: GenreModel[];
  containsString: any = '';
  filter: FilterModel = { containsString: '', genres: [] }
  isLoaded: boolean = false;

  subscription : any;

  currentPageNumber: number = 1;
  numberOfPages: number = 1;

  constructor(private httpClient: HttpClient, private jwtHelper: JwtHelperService, private authService: AuthService, private gameService: GameService, private viewContainerRef: ViewContainerRef, private genreService: GenresService) {
  }

  ngOnInit(): void {
    this.subscription = this.gameService.getGamesFiltered(this.filter, this.currentPageNumber).subscribe({
      next: async (response: any) => {
        this.games = response.games;
        this.currentPageNumber = response.currentPageNumber;
        this.numberOfPages = response.numberOfPages;
        this.isLoaded = true;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });

    this.genreService.getGenres().subscribe({
      next: (response: any) => {
        this.genres = response;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

  isUserAuthenticated = () => this.authService.isUserAuthenticated();
  isManagerOrAdmin = () => this.authService.isManagerOrAdmin();

  async onGameDelete(game: any) {
    this.games = await this.games.filter(x => x !== game);

    if (this.games.length == 0) {
      location.reload();
    }
  }

  async onGenreCheckBoxChanged() {

    this.filter.containsString = await this.containsString;
    this.filter.genres = await this.genres;

    if (this.filter.containsString.length < 3)
      this.filter.containsString = '';

    await this.getGamesFiltered(1);
  }

  async onXClick() {
    await this.onGenreCheckBoxChanged();
  }

  async onSearchBarChanged() {
    this.filter.genres = await this.genres;
    this.filter.containsString = await this.containsString;

    if (this.filter.containsString.length < 3)
      this.filter.containsString = '';

    await this.getGamesFiltered(1);
  }

  async moveToPage(numberOfPage: any) {

    this.currentPageNumber = numberOfPage;

    await this.getGamesFiltered(numberOfPage);

  }

  async getGamesFiltered(numberOfPage : any) {
    if (this.subscription)
      this.subscription.unsubscribe();

    this.subscription = this.gameService.getGamesFiltered(this.filter, numberOfPage).subscribe({
      next: (response: any) => {
        this.games = response.games;
        this.currentPageNumber = response.currentPageNumber;
        this.numberOfPages = response.numberOfPages;
      },
      error: (err: HttpErrorResponse) => console.log(err)
    });
  }

}
